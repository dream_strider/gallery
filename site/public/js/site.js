function ajaxPull()
{
	req = $.map($(".ajax"), function(o)
	{
		var data = {};
		$.each(o.attributes, function(i, attr) 
		{
			if (attr.name.indexOf('data-ajax-') === 0)
				data[attr.name.substring(10)] = attr.value;
		});
		
		ret =
		{
			id: o.id,
			data: data
		};
		return ret;
	});
	$.post( "ajax", { requests: req }).done(function( data )
	{
		$.each(data, function(i, val) 
		{
    		//console.log(val.id);
			$("#" + val.id).replaceWith(val.data);
			bindEvents($("#" + val.id));	
    	});
	});
}

function ajaxAucPull()
{
	$.post( "ajax",
	{
		requests: 
		[
			{
				id: 'bids',
				data:
				{
					auc_id: $('input[name="auc_id"]').val()
				}
			},
			{
				id: 'auction_params',
				data:
				{
					auc_id: $('input[name="auc_id"]').val()
				}
			}
		]
	}).done(function( data ) 
	{
		$.each(data, function(i, val) 
		{
			//console.log(val.id);
			$("#" + val.id).replaceWith(val.data);
		});
		$(".quip-count").html($("li.bid").length);

        if($(".auction_started").length > 0)
            $("#post-bid").show();
        else
            $("#post-bid").hide();

		//setTimeout(function(){$(".fdataitem-props").mCustomScrollbar("scrollTo","top");},200);		
	});	
}

function showModal(cls, reuse)
{
    if(!cls)
        cls = "fmodal";

    $fm = $("."+cls);
    if(!reuse || $fm.length == 0)
    {
        $fm.remove();

        $("body").css("overflow", "hidden");
        $("body").append('<div class="' + cls + '">' +
            '    <div class="focused">' +
            '        <div class="row fcontent" style="overflow: hidden;">' +
            '            <div class="fdataitem-cont col-sm-7 col-md-8 col-lg-9" style="height:100%; padding:0;">' +
            '                <a href="javascript:void(0);" class="glyphicon glyphicon-remove fclose"></a>' +
            '                <div class="row fheader shownotempty">' +
            '                    <div class="inset">' +
            '                    </div>' +
            '                </div>' +
            '                <div class="row" style="height:100%">' +
            '                    <div class="fdataitem-main col-sm-12">' +
            '                        <div class="centered" style="margin-top: 50px;">' +
            '                            <img src="img/ajax-loader.gif">' +
            '                            </div>' +
            '                        </div>' +
            '                    </div>' +
            '                </div>' +
            '                <div class="fdataitem-props scrollable col-sm-5 col-md-4 col-lg-3">' +
            '                </div>' +
            '            </div>' +
            '        </div>' +
            '    </div>');

        $fm = $('.'+cls);
        $(".glyphicon-remove.fclose", $fm).click(function()
        {
            if(window.aucPuller)
            {
                window.clearInterval(window.aucPuller);
                delete window.aucPuller;
            }

            var State = History.getState();
            if(State.data.p)
                window.History.back();
            else
                window.location.search = "";

            return false;
        });
    }
    $fm.show();
    if($("#phonediv").is(':hidden'))
        $(".fcontent", $fm).height($(window).height());
    return $fm;
}

function updateSshowPreview()
{
	if(window.jssor_slider1)
		delete window.jssor_slider1;
	$("#demo_slider").empty();
	$("#demo_slider").append($("div[u='slides']").clone().show());

	if(window.selectedSshowItem)
	{
		_SlideTransitions = [SlideTransitionEffects[window.selectedSshowItem.image_effect]];
		_CaptionTransitions = [CaptionTransitionEffects[window.selectedSshowItem.caption_effect]];
	}
	else if(typeof(__sshowImages) != 'undefined' && __sshowImages.length > 0)
	{
		_SlideTransitions = [SlideTransitionEffects[__sshowImages[0].image_effect]];
		_CaptionTransitions = [CaptionTransitionEffects[__sshowImages[0].caption_effect]];
	}
	else		
	{
		_SlideTransitions = [SlideTransitionEffects['fadetwins']];
		_CaptionTransitions = [CaptionTransitionEffects['movel']];
	}

	window.jssor_slider1 = new $JssorSlider$("demo_slider", 
	{
		$AutoPlay: true,
		$AutoPlayInterval: 1000,
		$PauseOnHover: 0,
		$CaptionSliderOptions: 
		{
			$Class: $JssorCaptionSlider$,
			$CaptionTransitions: _CaptionTransitions,
			$PlayInMode: 1,
			$PlayOutMode: 3
		},
		$SlideshowOptions: 
		{
			$Class: $JssorSlideshowRunner$,
			$Transitions: _SlideTransitions,
			$TransitionsOrder: 1,
			$ShowLink: false
		}
	});
}

function selectSshowImage(id)
{
	$("#sshow_images li").removeClass("selected");
	$img_div = $("#sshow_images li div[data-itemid='" + id + "']");
	if($img_div.length == 1)
	{
		$node = $($img_div[0]).parent();
		$node.addClass("selected");
		var offset = $node.position();
		$("#trash").show();
		$("#trash").css({ top: (offset.top - 3) + "px", left: (offset.left + 126) + "px"});
	}
	delete window.selectedSshowItem;
	if(typeof(__sshowImages) != 'undefined')
	{
		var res = $.grep(__sshowImages, function(e){ return e.artwork_id == id; });
		if(res.length==1)
		{
			window.selectedSshowItem = res[0];
			$("select[name='slide']").selectpicker('val', window.selectedSshowItem.image_effect);
			$("select[name='caption']").selectpicker('val', window.selectedSshowItem.caption_effect);
			$("#timing").val(window.selectedSshowItem.delay);
			$("input[name='title']").prop('checked', window.selectedSshowItem.showtitle == 1); 
		}
	}
	updateSshowPreview();
}

function sshowAddItemFilter()
{
	if(typeof(__sshowImages) == 'undefined')
		return;
		
	$('#sshow_select_img ul li').show();
	for(i=0; i<__sshowImages.length;i++)
	{
		var id = __sshowImages[i].artwork_id;
		$("#sshow_select_img ul li[data-itemid='" + id + "']").hide();
	}
	var col = $("input[name='collection_ref']").val();
	if(col > 0)
		$("#sshow_select_img ul li[data-col!='" + col + "']").hide();
}

function updateSshow(id, successFunc)
{
	if(window.sshowupdrequest)
		clearTimeout(window.sshowupdrequest);
	window.sshowupdrequest = null;
		
	var title = $("input[name='rename']").val();
	var rnd = $("input[name='random']").is(":checked") ? 1 : 0;
	var update = 
	{
		id: id,
		title: title,
		order: rnd,
		images: typeof(__sshowImages) != 'undefined' ? __sshowImages : []
	};
	
	window.sshowupdrequest = setTimeout(function()
	{
		window.sshowupdrequest = null;
		$.post( "ajax",
		{
			requests: 
			[
				{
					id: 'updshow',
					data: update
				}
			]
		}).done(function( data ) 
		{
			$('#success').hide();
			if(successFunc)
				successFunc();
		});		
	}, 500);
	
	updateSshowPreview();
}

_newestMessage = 0;

function fetchNewMessages()
{
    $.post( "ajax", { requests:
        [
            {
                id: 'usermessages',
                data:
                {
                    newer: _newestMessage
                }
            }
        ]}).done(function( data )
    {
        $.each(data, function(i, val)
        {
            $("#" + val.id).children().remove();
            $(val.data).appendTo($("#" + val.id));
        });
        bindEvents($("#usermessages"));

        var $ul = $(".msglist");
        $("#usermessages").children().each(function(i, el)
        {
            var $el = $(el);
            var tm = parseInt($el.attr("data-time"));
            if(tm > _newestMessage)
                _newestMessage = tm;
            $el.prependTo( $ul );
        });

        $ul = $ul.children();
        $ul.last().addClass("end");
        $ul.first().removeClass("end");
        $ul.first().addClass("top");
    });
}

function fetchMessages()
{
    var _this = this;
    if(!this.msgTime)
        this.msgTime = 99999999999;
    $.post( "ajax", { requests:
        [
            {
                id: 'usermessages',
                data:
                {
                    older: this.msgTime
                }
            }
        ]}).done(function( data )
    {
        $.each(data, function(i, val)
        {
            $("#" + val.id).children().remove();
            $(val.data).appendTo($("#" + val.id));
        });
        bindEvents($("#usermessages"));

        if($("#usermessages").children().length == 50)
            $("#showmore").show();
        else
            $("#showmore").hide();

        var $ul = $(".msglist");
        $("#usermessages").children().each(function(i, el)
        {
            var $el = $(el);
            var tm = parseInt($el.attr("data-time"));
            if(tm < _this.msgTime)
                _this.msgTime = tm;
            if(tm > _newestMessage)
                _newestMessage = tm;
            $el.appendTo( $ul );
        });

        $ul = $ul.children();
        $ul.last().addClass("end");
        $ul.first().removeClass("end");
        $ul.first().addClass("top");
    });
}

function readMessage(mid)
{
    $.post( "ajax",
        {
            requests:
                [
                    {
                        id: 'readmessage',
                        data:
                        {
                            mid: mid
                        }
                    }
                ]
        }).done(function( data )
        {
            $("li[data-mid=\"" + mid + "\"]").removeClass('unread');
        });
}

function deleteMessage(mid, delfn)
{
    $.post( "ajax",
        {
            requests:
                [
                    {
                        id: delfn,
                        data:
                        {
                            mid: mid
                        }
                    }
                ]
        }).done(function( data )
        {
            $li = $("li[data-mid=\"" + mid + "\"]");
            $ul = $li.parent();

            $li.remove();
            $ul = $("li", $ul);
            $ul.last().addClass("end");
            $ul.first().removeClass("end");
            $ul.first().addClass("top");
        });
}

function updateComments(actionUrl, mode)
{
	$('#confirmModal').modal('hide');
	if(actionUrl) {
		$.post(actionUrl).done(function (data) {
			updateComments(null);
		});
		if(mode == 1)
		{
			$('#enable_comments_btn').show();
			$('#disable_comments_btn').hide();
		}
		if(mode == 2)
		{
			$('#disable_comments_btn').show();
			$('#enable_comments_btn').hide();
		}
	}
	else
	{
		$.post( "ajax",
		{
			requests:
				[
					{
						id: 'comments',
						data:
						{
							thread: $('input[name="thread"]').val()
						}
					}
				]
		}).done(function( data )
		{
			$.each(data, function(i, val)
			{
				//console.log(val.id);
				$("#" + val.id).replaceWith(val.data);
			});
			$(".quip-count").html($("li.quip-comment").length);
			//setTimeout(function(){$(".fdataitem-props").mCustomScrollbar("scrollTo","top");},200);
		});
	}
}

function bindEvents(root)
{
	_this = this;
	$('form.noautocomplete input', root).attr('autocomplete', 'off');

	// add click handler for all thumbnails
	$(".select_thumb", root).not(".bound").click(function()
	{
		var rowid = $(this).attr('data-rowid');
		var itemid = $(this).attr('data-itemid');
		
		if(!rowid || !itemid)
			return false;
			
		window.History.pushState({p: 'focused', rowid:rowid, itemid: itemid}, null, window.location.origin + window.location.pathname + "?p=focused&rowid=" + rowid + "&itemid=" + itemid);

		return false;
	});

    $(".select_thumb", root).addClass('bound');
	
	// add click handler for all sorts
	$(".dosort", root).click(function()
	{
		var rowid = $(this).attr('data-rowid');
		var sort = $(this).attr('data-sort');
		var my = $(this).attr('data-my');
		$.post( "ajax", { requests:
		[
			{
				id: 'sort_thumbs',
				data:
				{
					rowid: rowid,
					sort: sort,
					my: my
				}
			}
		]}).done(function( data ) 
		{
			// select container list
			var row = $(".preview_list:has(a[data-rowid='" + rowid + "'])");
			if(!row)
				return false;
			row.replaceWith(data[0].data);
			var row = $(".preview_list:has(a[data-rowid='" + rowid + "'])");
//			row.hide().fadeIn(200);
			bindEvents(row);
		});			
		return false;
	});
	
	// add upload form logic	
	$(".btn-file", root).click(function()
	{ 
		$("input:file").click();
	});
	
	$('.crop[src=""]').hide();
	
	$('.crop[src!=""]').Jcrop(
	{
		aspectRatio: 1,
		minSize: [100, 100],
		onSelect: function(c)
		{
			var dim = this.getBounds();
			var coords = c.x/dim[0]+","+c.y/dim[1]+","+c.x2/dim[0]+","+c.y2/dim[1];
			$("input[name='photocrop']").val(coords);
		}
	},function()
	{
		document.jcrop_api = this;
		var dim = this.getBounds();
		var existing_crop = $("input[name='photocrop']").val();
		if(existing_crop)
		  existing_crop = existing_crop.split(",");
		  
		this.setSelect(existing_crop ? 
		[
			Math.round(existing_crop[0] * dim[0]),
			Math.round(existing_crop[1] * dim[1]),
			Math.round(existing_crop[2] * dim[0]),
			Math.round(existing_crop[3] * dim[1])
		]
		:
		[
			Math.round(0.33 * dim[0]),
			Math.round(0.33 * dim[1]),
			Math.round(0.66 * dim[0]),
			Math.round(0.66 * dim[1])
		]);					
	});
	
	var form = $("input:file", root).parent('form');
	$(form).ajaxForm(
	{ 
		beforeSend: function() 
		{
			$(form).hide();
			$(".fileprogress").show();
			$(".upload-success").hide();		
			$(".upload-error").hide();		
		},
		uploadProgress: function(event, position, total, percentComplete) 
		{		 
			$(".progress-bar").attr("aria-valuenow", percentComplete);
			$(".progress-bar").width(percentComplete+"%");
			$(".progress-text").text(percentComplete+"% Complete");
		},
		complete: function(response) 
		{
			$(".fileprogress").hide();
			$(".progress-bar").attr("aria-valuenow", 0);
			$(".progress-bar").width(0+"%");
			$(".progress-text").text(0+"% Complete");
			$(form).show();
		},
		success: function(resp) 
		{	
			if(resp.indexOf("Error") >= 0)			
				$(".upload-error").show();		
			else
			{
				if(document.jcrop_api)
				{
					document.jcrop_api.destroy();
					document.jcrop_api = null;
				}
				
				$("#cancel_crop").show();
				$("#save_photo").show();
			
				$(".file-preview").attr('src', resp);		
				$(".file-preview").show();		
				$(".upload-success").show();	
				$(".btn-success").removeClass("disabled");
								
				$('.crop').Jcrop(
				{
					aspectRatio: 1,
					minSize: [100, 100],
					onSelect: function(c)
					{
					    var dim = this.getBounds();
						var coords = c.x/dim[0]+","+c.y/dim[1]+","+c.x2/dim[0]+","+c.y2/dim[1];
						$("input[name='photocrop']").val(coords);
					}
				},function()
				{
					document.jcrop_api = this;
					var dim = this.getBounds();
					this.setSelect(
					[
						Math.round(0.33 * dim[0]),
						Math.round(0.33 * dim[1]),
						Math.round(0.66 * dim[0]),
						Math.round(0.66 * dim[1])
					]);					
				});
			}
		},
		error: function()
		{
			$(".upload-error").show();					
		}
	});	
	$(form).change(function()
	{
		$(form).trigger('submit');
	});
	
	$("#cancel_crop", root).click(function()
	{
		if(document.jcrop_api)
		{
			document.jcrop_api.destroy();
			document.jcrop_api = null;
		}
		$(".file-preview").hide();	
		$(".upload-success").hide();	
		$(".upload-error").hide();	
		$("input[name='avatar']").val(null);
		$("input[name='photocrop']").val("");
		$("#cancel_crop").hide();
		$("#save_photo").hide();
		return false;
	});
	
	$(".scrollable", root).mCustomScrollbar({scrollInertia:150, advanced:{
        updateOnContentResize: true
    }});
	
	
	if($("#phonediv").is(':hidden'))
		$('.selectpicker', root).selectpicker();
	else
		$('.selectpicker', root).selectpicker('mobile');
	
	$("ul[role='form-menu'] li[data-option]", root).click(function()
	{
		var parent = $(this).parent();
		$("li[data-option]", parent).removeClass("active");
		$(this).addClass("active");
		var scr_id = $(this).attr("data-option");
		$("input[name='opt']").val(scr_id);
		$(".form-screen").hide();
		$("#"+scr_id).show();
		$("#"+scr_id+".scrollable").mCustomScrollbar("update");
		if(!$("#phonediv").is(':hidden'))
			$('body').scrollTo( $("#"+scr_id), {offsetTop: $('body').scrollTop(), duration: 800} );
	});
	
	$("ul[role='single-select']", root).each(function()
	{
		var target = $(this).attr("data-select");
		var value = $("#"+target).val();
		$("li[data-option='" + value + "']", $(this)).addClass("active");
	});

	$("ul[role='single-select'] li[data-option]", root).click(function()
	{
		var parent = $(this).parent();
		var target = $("#"+parent.attr("data-select"));
		var value = $(this).attr("data-option");
		$("li[data-option]", parent).removeClass("active");
		$(this).addClass("active");
		target.val(value);
	});
	
	$("*[type='button'][data-form]", root).click(function()
	{
        var frm = "#" + $(this).attr("data-form");
        $(frm + " input.ios-switch").each(function()
        {
           if(!$(this).is(':checked'))
               $(frm).append('<input type="hidden" name="' + $(this).attr('name') + '" value="'+$(this).attr('off_value')+'" />')
        });
		var submitName = $(this).attr("data-submit");
		var submitVal = $(this).attr("data-value");
		if(submitName)
			$("input[name='" + submitName + "']").val(submitVal ? submitVal : 'click');
		$("#" + $(this).attr("data-form")).submit();
		return false;
	});
	
	$('div.shownotempty:has(*)').show();
	$('div.validator ul:has(*)').parent().show();
	
	$('.datepicker', root).datepicker(
	{
	}).on('changeDate', function(ev) 
	{
		var date_name = $(this).attr("data-unix-date");
		if(date_name)
		{
			var unixtime = ev.date.getTime()/1000;
			$("input[name='"+date_name+"']").val(unixtime);
		}
		$(this).datepicker('hide');
	}).on('change', function()
	{
		var date_name = $(this).attr("data-unix-date");
		if(date_name)
		{
			var dateVal = Date.parse($(this).val());
			if(!isNaN(dateVal))
			{
				var unixtime = dateVal/1000;
				$("input[name='"+date_name+"']").val(unixtime);
			}
			else
				$("input[name='"+date_name+"']").val('');
		}
	});
	
	$('.timepicker', root).timepicker(
	{
		minuteStep: 30,
        showSeconds: false,
        showMeridian: false,
        defaultTime: '00:00 AM'
	});
	
	$('.colorpick', root).colorpicker();
	
	$(".comment-entry", root).focus(function()
	{
		$(".comment-actions").show();
		//setTimeout(function(){$(".fdataitem-props").mCustomScrollbar("scrollTo","bottom");},200);		
	});
	
//	$(".comment-actions button", root).click(function()
//	{
//		$(".comment-actions").hide();
//		$(".comment-entry").val("");
//	});

	$(".postcomment", root).ajaxForm(
	{ 
		success: function(resp) 
		{	
			$(".comment-actions").hide();
			$(".comment-entry").val("");

			updateComments();
		},
		error: function()
		{
		}
	});	
	
	$(".postbid", root).ajaxForm(
	{ 
		success: function(resp) 
		{	
    		$('#bid_error').hide();
			ajaxAucPull();
		},
		error: function(resp)
		{
            $('#bid_error').html(resp.responseJSON.error);
            $('#bid_error').show();
		}
	});

    setTimeout(function()
    {
        var currentcarousel = 0;
        var $preview = $( '#carousel_preview', root);
		var $previewt = $( '#carousel_preview_title', root);
		var $previewd = $( '#carousel_preview_desc', root);
        var $carouselEl = $( '#carousel', root);
        var $carouselItems = $carouselEl.children();
        var carousel = $carouselEl.elastislide(
        {
            current : currentcarousel,
            minItems : 12,
            onClick : function( el, pos, evt )
            {
                $preview.slideToggle({duration: 200, complete: function()
                {
                    $preview.attr( 'src', el.data( 'preview' ) );
					$previewt.html( el.data( 'title' ) );
					$previewd.html( el.data( 'desc' ) );
                    $preview.slideToggle({duration: 200});
                }});
                $carouselItems.removeClass( 'current-img' );
                el.addClass( 'current-img' );
                carousel.setCurrent( pos );
                evt.preventDefault();
            },
            onReady : function()
            {
                var el = $carouselItems.eq( currentcarousel );
                var pos = currentcarousel;
                $preview.attr( 'src', el.data( 'preview' ) );
				$previewt.html( el.data( 'title' ) );
				$previewd.html( el.data( 'desc' ) );
                $carouselItems.removeClass( 'current-img' );
                el.addClass( 'current-img' );
                carousel.setCurrent( pos );
            }
        });
    }, 500);


	$(".auc-start", root).click(function()
	{ 
		$(".auc-settings").height(500);			
		$(".auc-settings").slideToggle({duration: 300, complete: function()
		{
			$(".auc-settings").mCustomScrollbar("update");		
		}});
		return false;
	});
	
	$(".addwork", root).click(function()
	{
        $fm = showModal("smodal", true);
        if($("#sshow_select_img").length == 0)
        {
            $.post( "ajax",
            {
                requests:
                [
                    {
                        id: 'addslide'
                    }
                ]
            }).done(function( data )
            {
                $fm.append(data[0].data);
                if($("#phonediv").is(':hidden'))
                    $(".fcontent", $fm).height($(window).height());
                bindEvents($fm);
                sshowAddItemFilter();
                $(".glyphicon-remove.fclose", $fm).click(function()
                {
                    if($fm.length)
                        $fm.hide();
                    $("body").css("overflow", "auto");
                    return false;
                });
            });
        }
        else
            sshowAddItemFilter();

		return false;
	});
	
	$(".sortable_list", root).sortable({ distance: 5, opacity: 0.5, tolerance: "pointer" });
	
	$( "#sshow_images", root).sortable(
	{
		start: function( event, ui ) 
		{
			$("#trash").hide();
		},
		update: function( event, ui ) 
		{
			_seq = __sshowImages;
			__sshowImages = [];
			$("#sshow_images li").each(function()
			{
				var target = $(this).children('div');
				var id = $(target[0]).attr('data-itemid');
				if(__sshowImages)
				{
					var res = $.grep(_seq, function(e){ return e.artwork_id == id; });
					if(res.length==1)
						__sshowImages.push(res[0]);
				}
			});
			if(window.selectedSshowItem)
				selectSshowImage(window.selectedSshowItem.artwork_id);
		}
	});
	
	$sshow_image = $("#sshow_images li", root);
	$sshow_image.click(function()
	{
		var target = $(this).children('div');
		var id = $(target[0]).attr('data-itemid');
		selectSshowImage(id);
	});
	if($sshow_image.length > 0)
	{
		var target = $($sshow_image[0]).children('div');
		var id = $(target[0]).attr('data-itemid');
		selectSshowImage(id);
	}	

	$( "ul[data-select='collection_ref']", root).click(function()
	{
		sshowAddItemFilter();
	});

	$( "ul.addlist li", root).click(function()
	{
		if(typeof(__sshowImages) == 'undefined')
			__sshowImages = [];
		var id = $(this).attr('data-itemid');
		var max_mid=0;
		for(i=0; i<__sshowImages.length;i++)
			max_mid = Math.max(max_mid,__sshowImages[i].num);
		
		if(window.selectedSshowItem)
			tpl = window.selectedSshowItem;
		else if(__sshowImages.length > 0)
			tpl = __sshowImages[0];
		else tpl=
		{
			delay: 6,
			showtitle: 1,
			image_effect: 'fadetwins',
			caption_effect: 'movel'
		};
			
		__sshowImages.push(
		{
			num: max_mid + 1,
			artwork_id: id,
			delay: tpl.delay,
			showtitle: tpl.showtitle,
			image_effect: tpl.image_effect,
			caption_effect: tpl.caption_effect
		});
		window.selectedSshowItem = __sshowImages[__sshowImages.length-1];
		
		var $newli = $(
		'<li><div class="sssetup-item" data-rowid="art" data-itemid="'+id+'">'+
			'<div class="preview_pic"><img src="' + $('div img', this).attr('src') + '"></div>'+
			'</div>'+
		'</li>');
		
		$('#sshow_images').append($newli);
		$newli.click(function()
		{
			var target = $(this).children('div');
			var id = $(target[0]).attr('data-itemid');
			selectSshowImage(id);
		});
		
		$(".sortable_list", root).sortable({ distance: 5, opacity: 0.5, tolerance: "pointer" });
				
		$fm = $('.smodal');
		if($fm.length)
			$fm.hide();
		$("body").css("overflow", "auto");				
		selectSshowImage(id);		
		return false;
	});
	
	$( "#applytoall", root).click(function()
	{
		if(!window.selectedSshowItem)
			return false;
			
		for(i=0; i<__sshowImages.length;i++)
		{
			var cur = __sshowImages[i];
			cur.delay = window.selectedSshowItem.delay;
			cur.showtitle = window.selectedSshowItem.showtitle;
			cur.image_effect = window.selectedSshowItem.image_effect;
			cur.caption_effect = window.selectedSshowItem.caption_effect;
		}
		
		$('#applied').modal({show: true});
		
		return false;
	});
		
	$("#trash", root).click(function()
	{
		if(!window.selectedSshowItem)
			return false;

		for(i=0; i<__sshowImages.length;i++)
		{
			if(__sshowImages[i] == window.selectedSshowItem)
			{
				$target = $("#sshow_images li div[data-itemid='" + window.selectedSshowItem.artwork_id + "']", root);
				if($target.length > 0)
					$($target[0]).parent().remove();
					
				__sshowImages.splice(i,1);
				
				if(__sshowImages.length > 0)
				{
					if(i >= __sshowImages.length)
						window.selectedSshowItem = __sshowImages[__sshowImages.length - 1];
					else
						window.selectedSshowItem = __sshowImages[i];
				}	
				else
					delete window.selectedSshowItem;
					
				if(window.selectedSshowItem)
					selectSshowImage(window.selectedSshowItem.artwork_id);
				else 
					$("#trash").hide();
				break;
			}
		}

		return false;
	});
	
	$("select[name='slide']", root).change(function()
	{
		if(!window.selectedSshowItem)
			return;
		window.selectedSshowItem.image_effect = $(this).val();
		updateSshow();
	});
	$("select[name='caption']", root).change(function()
	{
		if(!window.selectedSshowItem)
			return;
		window.selectedSshowItem.caption_effect = $(this).val();
		updateSshow();
	});	
	$("#timing", root).change(function()
	{
		if(!window.selectedSshowItem)
			return;
		window.selectedSshowItem.delay = $(this).val();
		updateSshow();
	});
	$("input[name='title']", root).change(function()
	{
		if(!window.selectedSshowItem)
			return;
		window.selectedSshowItem.showtitle = $(this).is(":checked") ? 1 : 0;
		updateSshow();
	});
	$("input[name='random']", root).change(function()
	{
		updateSshow();
	});
	$("input[name='rename']", root).on('input',function()
	{
		updateSshow();
	});
	
	
	if($("#demo_slider").length > 0)
		updateSshowPreview();
		
	var total_list_count = parseInt($("#total", root).val());
	if(total_list_count > 0)
	{
		var current_count = $(".showlist", root).children().length;
		if(current_count < total_list_count)
		{		
			$("button[data-showmore]", root).show();
			$("button[data-showmore]", root).attr("data-from", current_count);
		}
		else
		{
			$("button[data-showmore]", root).hide();
		}
	}
	
	$("button[data-showmore]", root).click(function()
	{
		var q = $(this).attr("data-showmore");
		var from = $(this).attr("data-from");
        var search = $(this).attr("data-ajax-search");
        var ord = $(this).attr("data-ajax-order");

		$.post( "ajax",
		{
			requests: 
			[
				{
					id: 'showall',
					data:
					{
						query: q,
						offset: from,
                        search: search,
                        order: ord
					}
				}
			]
		}).done(function( data ) 
		{
			$list = $('.showlist');
			$list.append(data[0].data);
			bindEvents($list);
			
			var total_list_count = parseInt($("#total", root).val());
			if(total_list_count > 0)
			{
				var current_count = $(".showlist", root).children().length;
				if(current_count < total_list_count)
				{		
					$("button[data-showmore]", root).show();
					$("button[data-showmore]", root).attr("data-from", current_count);
				}
				else
				{
					$("button[data-showmore]", root).hide();
				}
			}

		});		
	});
	
	$("a.hdr", root).click(function()
	{
		$(this).next().toggle();
		if($(this).next().is(':hidden'))
			$(this).prev().removeClass('perm');
		else
		{
			$(this).prev().addClass('perm');

            var mid = $(this).parent().attr("data-mid");
			var tid = $(this).parent().attr("data-tid");
            setTimeout(function()
            {
                readMessage(mid);
            }, 1000);

			$.post( "ajax",
			{
				requests:
				[
					{
						id: 'message_thread',
						data:
						{
							mid: mid,
							tid: tid
						}
					}
				]
			}).done(function( data )
			{
				$("#thread_" + mid).replaceWith('<ul id="thread_' + mid + '">' + data[0].data + '</ul>');
				bindEvents($("#thread_" + mid));
			});
		}
	});
	
	$("a.del", root).click(function()
	{
        var mid = $(this).parent().attr("data-mid");
        deleteMessage(mid, 'deletemessage');
	});

	$("a.to-user", root).click(function()
	{
		$(this).hide();
		$(this).next().show();
		$(this).next().next().show();
		$(this).next().focus();
	});

	$("a.to-webmaster", root).click(function()
	{
		var tid = $(this).attr("data-mid");
		deleteMessage(tid, 'to-webmaster');
	});

	$("button.send-reply", root).click(function()
	{
		var mid = $(this).data('mid');
		var tid = $(this).data('tid');
		$.post( "ajax",
		{
			requests:
			[
				{
					id: 'reply',
					data:
					{
						mid: mid,
						tid: tid,
						text: $('textarea[data-reply-mid="' + mid + '"]').val()
					}
				}
			]
		}).done(function( data )
		{
			$("#thread_" + mid).replaceWith('<ul id="thread_' + mid + '">' + data[0].data + '</ul>');
			bindEvents($("#thread_" + mid));
		});

		$(this).prev().val("");
		$(this).hide();
		$(this).prev().hide();
		$(this).prev().prev().show();
	});

    if($("#usermessages", root).length > 0)
    {
        window.setInterval(fetchNewMessages, 10000);
        fetchMessages();
        $("#showmore").click(function()
        {
            fetchMessages();
        });
    }

	$(".close.greeting", root).click(function()
	{
		var btn = this;
		$(btn).parent().hide();
		
		$.post( "ajax",
		{
			requests: 
			[
				{
					id: 'greeting'
				}
			]
		});					
		return false;
	});
	
	$("#usersearch button", root).click(function()
	{
		text = $("#usersearch input").val();
		window.location = "user/list/search?q="+text;
	});
	
	$("#usersearch input", root).keyup(function(e)
	{
		if(e.keyCode == 13)
		{
			$("#usersearch button", root).trigger('click');
		}
	});

	$("#famesearch button", root).click(function()
	{
		text = $("#famesearch input").val();
		window.location.search = "?q=" + text;
	});
	
	$("#famesearch input", root).keyup(function(e)
	{
		if(e.keyCode == 13)
		{
			$("#famesearch button", root).trigger('click');
		}
	});

    $("#aucsearch button", root).click(function()
    {
        text = $("#aucsearch input").val();
        window.location.search = "?q=" + text;
    });

    $("#aucsearch input", root).keyup(function(e)
    {
        if(e.keyCode == 13)
        {
            $("#aucsearch button", root).trigger('click');
        }
    });
	
	$("#aucsort", root).change(function(e)
    {
		var crit = $('.showlist').attr('data-ajax-crit');
	    window.location = "auctions/" + crit + "/" + $(this).val();
    });

    $("#globalsearch button", root).click(function()
    {
        text = $("#globalsearch input").val();
        window.location = "search?q="+text;
    });

    $("#globalsearch input", root).keyup(function(e)
    {
        if(e.keyCode == 13)
        {
            $("#globalsearch button", root).trigger('click');
        }
    });

    $("#confirmModal", root).on("show.bs.modal", function (event)
    {
        var button = $(event.relatedTarget);
        var text = button.data('confirm');
        var href = button.data('action');
        var modal = $(this);
        modal.find('.confirmText').text(text);
        modal.find('.okbutton').attr("href", href);
    });

	$("#rent_cb", root).change(function()
	{
		$("#rent_period_div").toggle(this.checked);
		$("#offer_accept").hide();
		$("#offer_change").show();
	});

	$("#offer_price,#offer_rent_period", root).keyup(function()
	{
		var val = parseInt($("#offer_price").val()) * parseInt($("#offer_rent_period").val());
		$("#offer_total").html(parseInt(val) >= 0 ? parseInt(val) : 0);
		$("#offer_accept").hide();
		$("#offer_change").show();
	});

	$(".nav-tabs a").click(function (e)
	{
		e.preventDefault();
		$(this).parents('ul').children().removeClass('active');
		$(this).parent('li').addClass('active');
		$('.'+$(this).data('panel')).hide();
		$('#'+$(this).data('tab')).show();
		$('#'+$(this).data('input')).val($(this).data('tab'))
	});

	$(".table").DataTable(
		{
			"pageLength": 25,
			"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Все"] ],
			"language":
			{
				"decimal":        "",
				"emptyTable":     "Нет данных",
				"info":           "Показано _START_ - _END_ из _TOTAL_ записей",
				"infoEmpty":      "Показано 0 - 0 из 0 записей",
				"infoFiltered":   "(применён фильтр. всего - _MAX_ записей)",
				"infoPostFix":    "",
				"thousands":      ",",
				"lengthMenu":     "Показать _MENU_",
				"loadingRecords": "Загрузка...",
				"processing":     "Обработка...",
				"search":         "Поиск:",
				"zeroRecords":    "Ничего не найдено",
				"paginate": {
					"first":      "Первый",
					"last":       "Последний",
					"next":       "Следующий",
					"previous":   "Предыдущий"
				},
				"aria": {
					"sortAscending":  ": Нажмите для сортировки по возрастанию",
					"sortDescending": ": Нажмите для сортировки по убыванию"
				}
			}
		}
	);
}

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function checkParamActions()
{
	var p = getParameterByName("p");
	if(p == "focused")
	{
		var rowid = getParameterByName("rowid");
		var itemid = getParameterByName("itemid");
		if(!rowid || !itemid)
			return;

        $fm = showModal("fmodal");

		$.post( "ajax",
		{
			requests: 
			[
				{
					id: 'focused',
					data:
					{
						rowid: rowid,
						itemid: itemid
					}
				}
			]
		}).done(function( data ) 
		{
            if(!data[0].data)
                return;
            $fm.html(data[0].data);

            if(rowid == 'auc')
                window.aucPuller = window.setInterval(ajaxAucPull, 10000);

            $fm = $('.fmodal');
            $(".glyphicon-remove.fclose", $fm).click(function()
            {
                if(window.aucPuller)
                {
                    window.clearInterval(window.aucPuller);
                    delete window.aucPuller;
                }

                var State = History.getState();
                if(State.data.p)
                    window.History.back();
                else
                    window.location.search = "";

                return false;
            });
            if($("#phonediv").is(':hidden'))
                $(".fcontent", $fm).height($(window).height());
            bindEvents($fm);
		});			

	}
}

$( document ).ready(function() 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

/*
    $.ajax(
    {
        statusCode:
        {
            403: function()
            {
                window.location = window.location.origin + '/auth/login';
            }
        }
    });
*/

	//ajaxPull();
	window.setInterval(ajaxPull, 60000);

	$( window ).resize(function() 
	{
		//if($("#phonediv").is(':hidden'))
		//	$(".fcontent", $fm).height($(window).height());
	});
	
	
//	$(window).scrollTop($.cookie("gvc_scroll"));
//	$(window).scroll(function() {
//		console.log($(this).scrollTop());
//		$.cookie("gvc_setscroll", $(this).scrollTop());
//	});

	checkParamActions();
	
	window.History.Adapter.bind(window,'statechange',function()
	{
		var State = History.getState();
		if(State.data.p)
		{
			if(!window.popups)
				window.popups = [];
			window.popups.push(State.data);
			checkParamActions();
		}
		else
		{
			if(window.popups && window.popups.length > 0)
			{
				var st = window.popups.pop();
				if(st.p == 'focused')
				{
					if(st.rowid == 'auc')
						clearInterval(_this.aucPuller);

					$fm = $('.fmodal');
					if($fm.length)
						$fm.remove();
					$("body").css("overflow", "auto");				
				}
			}
		}
		
		//window.History.log(State.data, State.title, State.url);
	});


	if(typeof Dropzone !== 'undefined')
		Dropzone.options.dzform =
		{
			maxFiles: 1,
			init: function()
			{
				this.on("maxfilesexceeded", function(file)
				{
					this.removeAllFiles();
					this.addFile(file);
				});
				this.on("success", function(file)
				{
					$(".btn-success").removeClass("disabled");
					$(".upload-success").show();
				});
				this.on("error", function(file, response)
				{
					if(typeof response === 'object')
						$(file.previewElement).find('.dz-error-message').text("Неверный формат файла или файл слишком большой");
					else
						$(file.previewElement).find('.dz-error-message').text("Ошибка сервера");
					$(".btn-success").addClass("disabled");
				});
			}
		};

	bindEvents($("body"));	
});