$(function() { 

	$("#aclose").click(function()
	{
		var State = History.getState();
		if(State.data)
			window.History.back();
		return false;
	}); 

	$("#sclose").click(function()
	{
		var State = History.getState();
		if(State.data)
			window.History.back();
		return false;
	}); 
	
	var tmp_images = [];
	$("div[data-art]").each(function()
	{
		var id = $(this).attr('data-art');
		var res = $.grep(__sshowImages, function(e){ return e.artwork_id == id; });
		if(res.length==1)
			tmp_images.push(res[0]);
	});

	__sshowImages = tmp_images;

	$("#fmode").click(function()
	{
		var isFullscreen = $(document).fullScreen();
		$("#background").fullScreen(isFullscreen ? false : true);
		return false;
	});

	var preventWakeControls = false;
	function wakeControls()
	{
		if(preventWakeControls)
			return;
		if(window.fmodeFade)
			clearTimeout(window.fmodeFade);
		window.fmodeFade = null;
		$("#controls").fadeIn(200);
	    $('html').css({cursor: 'default'});
		window.fmodeFade = setTimeout(function()
		{
		  window.fmodeFade = null;
		  $("#controls").fadeOut(1000);
		  $('html').css({cursor: 'none'});
		}, 5000);
	}
	
	wakeControls();

	$("#background").bind("click", function()
	{
		wakeControls();
	});

	var moveCounter = 0;
	
	$("#background").bind("mousemove", function()
	{
		moveCounter++;
		if(moveCounter >= 10)		
		{
			moveCounter = 0;
			wakeControls();
		}
		else
		{
			setTimeout(function()
			{
			  moveCounter = 0;
			}, 1000);
		}
	});

	var showMode = false;
	if($(".slide").length > 1)
		showMode = true;
	else
		$("#nav").hide();
	
	document.addEventListener("keydown", function(e) 
	{
		if (e.keyCode == 13) 
		{
			var isFullscreen = $(document).fullScreen();
			$("#background").fullScreen(isFullscreen ? false : true);
		}
		if(e.keyCode == 32)
		{
			if(!showMode)
				return;
			if(jssor_slider1.$IsAutoPlaying())
			{
				jssor_slider1.$Pause();
				OnPlayPause(0);
			}
			else
			{
				jssor_slider1.$Play();
				OnPlayPause(1);
			}		
		}		
		wakeControls();
	}, false);


	$(document).bind("fullscreenchange", function(e) 
	{
		var isFullscreen = $(document).fullScreen();
		if(isFullscreen)
		{
		}
		else
		{
		}
    });  
		
	var SlideTransitions = [];
	var CaptionTransitions = [];
	
	for(i=0; i<__sshowImages.length;i++)
	{
		SlideTransitions.push(SlideTransitionEffects[__sshowImages[i].image_effect]);
		CaptionTransitions.push(CaptionTransitionEffects[__sshowImages[i].caption_effect]);
	}

	var _SlideTransitions = showMode ? [SlideTransitions[0]] : [SlideTransitionEffects['fadetwins']];
	var _CaptionTransitions = showMode ? [CaptionTransitions[0]] : [CaptionTransitionEffects['fade']];
  
	var viewportWidth = $(window).width();
	var viewportHeight = $(window).height();
  
	var options = 
	{
		$FillMode: 2,
		$AutoPlay: showMode,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
		$AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
		$AutoPlayInterval: __sshowImages.length > 0 ? __sshowImages[0].delay*1000 : 0,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
		$PauseOnHover: 0,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3

		$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
		$SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
		$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
		$SlideWidth: viewportWidth,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
		$SlideHeight: viewportHeight,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
		$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
		$DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
		$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
		$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
		$PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
		$DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

		$CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
			$Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
			$CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
			$PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
			$PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
		},

		$ArrowNavigatorOptions: {
			$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
			$ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
		},
		
		$SlideshowOptions: {
			$Class: $JssorSlideshowRunner$,
			$Transitions: _SlideTransitions,
			$TransitionsOrder: 1,
			$ShowLink: false
		}
	};

	$("#sshow").width(viewportWidth);
	$("#sshow").height(viewportHeight);
	$("div[u='slides']").width(viewportWidth);
	$("div[u='slides']").height(viewportHeight);		
	jssor_slider1 = new $JssorSlider$("sshow", options);

	function ScaleSlider() 
	{
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();			

		$("#sshow").width(viewportWidth);
		$("#sshow").height(viewportHeight);
		$("div[u='slides']").width(viewportWidth);
		$("div[u='slides']").height(viewportHeight);		
		jssor_slider1.$SetNewSize(viewportWidth, viewportHeight);		
	}
	
	ScaleSlider();

	$(window).bind('resize', ScaleSlider);  
	
	$("#prev_slide").click(function()
	{
		jssor_slider1.$Prev();
		return false;
	});

	$("#next_slide").click(function()
	{
		jssor_slider1.$Next();
		return false;
	});
	
	function OnPlayPause(pp)
	{
		if(pp)
			$("#pause_slide").toggleClass('glyphicon-play glyphicon-pause');
		else
			$("#pause_slide").toggleClass('glyphicon-pause glyphicon-play');
	}
	
	$("#pause_slide").click(function()
	{
		if(jssor_slider1.$IsAutoPlaying())
		{
			jssor_slider1.$Pause();
			OnPlayPause(0);
		}
		else
		{
			jssor_slider1.$Play();
			OnPlayPause(1);
		}
		return false;
	});
	
	jssor_slider1.$On($JssorSlider$.$EVT_DRAG_START,function(position, virtualPosition, event)
	{
	/*
		window.fmodePrevent = setTimeout(function()
		{
			preventWakeControls = true;
			if(window.fmodeFade)
				clearTimeout(window.fmodeFade);
			window.fmodeFade = null;
			$("#controls").fadeOut(10);
		}, 1000);*/
	});
	
	jssor_slider1.$On($JssorSlider$.$EVT_DRAG_END,function(position, startPosition, virtualPosition, virtualStartPosition, event)
	{
	/*
		if(window.fmodePrevent)
			clearTimeout(window.fmodePrevent);
		window.fmodePrevent = null;
		setTimeout(function()
		{
			preventWakeControls = false;
		}, 3000);*/
	});
	
	jssor_slider1.$On($JssorSlider$.$EVT_PARK,function(slideIndex, fromIndex)
	{
		_SlideTransitions = [SlideTransitions[slideIndex]];
		jssor_slider1.$SetSlideshowTransitions(_SlideTransitions);
	});
	
	jssor_slider1.$On($JssorSlider$.$EVT_STATE_CHANGE,function(slideIndex, progress, progressBegin, idleBegin, idleEnd, progressEnd)
	{
		if(progress == idleBegin)
		{
			_CaptionTransitions = [CaptionTransitions[(slideIndex + 1) % __sshowImages.length]];
			jssor_slider1.$SetCaptionTransitions(_CaptionTransitions);
			jssor_slider1.$SetNewInterval(__sshowImages[(slideIndex + 1) % __sshowImages.length].delay*1000);
		}
	});
});
