<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

    use SoftDeletes;

    protected $guarded = ['id', 'remember_token', 'active'];

	protected $hidden = ['password', 'remember_token', 'activation_code'];

    public function artworks()
    {
        return $this->hasMany('App\Artwork', 'owner_id');
    }

    public function collections()
    {
        return $this->hasMany('App\Collection', 'created_by');
    }

    public function artworkCountRelation()
    {
        $a = $this->artworks()->where('visibility','=',0);

        $authenticated = \Auth::check();
        if($authenticated)
            $a->orWhere('visibility','=',1)->orWhere(function($query)
            {
                $query->where('visibility', '=', 2)->where('owner_id', '=', \Auth::user()->id);
            });

        return $a->selectRaw($a->getForeignKey() . ', count(*) as count')->groupBy($a->getForeignKey());
    }

    public function getArtworkCountAttribute()
    {
        $ak = $this->artworkCountRelation->first();
        return $ak ? $ak->count : 0;
    }

    public function collectionCountRelation()
    {
        $a = $this->collections()->where('visibility','=',0);

        $authenticated = \Auth::check();
        if($authenticated)
            $a->orWhere('visibility','=',1)->orWhere(function($query)
            {
                $query->where('visibility', '=', 2)->where('created_by', '=', \Auth::user()->id);
            });

        return $a->selectRaw($a->getForeignKey() . ', count(*) as count')->groupBy($a->getForeignKey());
    }

    public function getCollectionCountAttribute()
    {
        $ak = $this->collectionCountRelation->first();
        return $ak ? $ak->count : 0;
    }

    public function getCountryTextAttribute()
    {
        return LangResource::get(ResourceType::Country, $this->country);
    }

    public function getRoleNameAttribute()
    {
        return trans('common.role.'.$this->role);
    }

    public function thumbPath($size)
    {
        return storage_path().'/app/content/thumbs/user/'.$this->id.'_'.$size.'.png';
    }

    public function thumbUrl($size)
    {
        return route('thumb', array('type' => 'user', 'id' => $this->id, 'size' => $size, 'hash' => md5($this->updated_at) ));
    }

    public function generateThumbs($path)
    {
        $img = \Image::make($path);
		Util::makeThumb($img, 250);
		$img->interlace();
        $img->save($this->thumbPath('mid'));
		Util::makeThumb($img, 58);
        $img->save($this->thumbPath('small'));
		$img->destroy();
    }
}
