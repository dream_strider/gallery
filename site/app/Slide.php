<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
	protected $guarded = ['id'];
	protected $hidden = [];

    public $timestamps = false;

    public function slideshow()
    {
        return $this->belongsTo('App\Slideshow');
    }

    public function artwork()
    {
        return $this->belongsTo('App\Artwork');
    }

}
