<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LangResource extends Model
{
    protected $guarded = [];

    public $timestamps = false;


    public static function flush()
    {
        Cache::tags('LangRes')->flush();
    }

    public static function get($res_type, $id = 0)
    {
        $locale = \App::getLocale();

        $set = \Cache::tags('LangRes', $locale)->rememberForever('LangRes_'.$locale.'_'.$res_type, function() use($locale, $res_type)
        {
            $ret = [];
            $col = \DB::table('lang_resources')->where('lang', $locale)->where('resource_type', $res_type)->select('resource_id','value')->get();
            foreach($col as $item)
                $ret[$item->resource_id] = $item->value;
            return $ret;
        });
        if(isset($set))
            return empty($id) ? $set : $set[$id];
    }
}
