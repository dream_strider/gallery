<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model 
{
    use SoftDeletes;
	
	protected $guarded  = ['id', 'view_count'];

	public function owner()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

	public function artworks()
    {
        return $this->hasMany('App\Artwork');
    }

    public function getKindAttribute()
    {
        return "col";
    }
	
	public function artworkCountRelation()
	{
		$a = $this->artworks();	
		return $a->selectRaw($a->getForeignKey() . ', count(*) as count')->groupBy($a->getForeignKey());
	}
	
	public function getArtworkCountAttribute()
	{
		$ak = $this->artworkCountRelation->first();
		return $ak ? $ak->count : 0;
	}



    public function thumbPath($size)
    {
        return storage_path().'/app/content/thumbs/col/'.$this->id.'_'.$size.'.png';
    }

    public function thumbUrl($size)
    {
        return route('thumb', array('type' => 'col', 'id' => isset($this->id) ? $this->id : 0, 'size' => $size, 'hash' => md5($this->updated_at) ));
    }

    public function generateThumbs()
    {
        if(file_exists($this->thumbPath('mid')))
		    unlink($this->thumbPath('mid'));
        if(file_exists($this->thumbPath('small')))
    		unlink($this->thumbPath('small'));

        $this->load('artworks');
		$cnt = count($this->artworks);
		if($cnt < 1)
			return;

		$img_mid = \Image::canvas(250, 250);
		$img_small = \Image::canvas(50, 50);

		for($i=0; $i<min($cnt, 3); $i++)
		{
			$art = $this->artworks[$i];
			$img = \Image::make($art->thumbPath('mid'));
			Util::makeThumb($img, 200);
			$img_mid->insert($img, 'bottom-right', 10 + $i*15, 10 + $i*15);
			Util::makeThumb($img, 30);
			$img_small->insert($img, 'bottom-right', $i*5, $i*5);
			$img->destroy();
		}
		
		$img_mid->interlace();
		$img_mid->save($this->thumbPath('mid'));
		$img_mid->destroy();
		$img_small->interlace();
		$img_small->save($this->thumbPath('small'));
		$img_small->destroy();
        $this->touch();
    }
}
