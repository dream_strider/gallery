<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model 
{
    public $timestamps = false;

    protected $guarded = array('id');

	public function sender()
    {
        return $this->belongsTo('App\User');
    }

	public function recipient()
    {
        return $this->belongsTo('App\User');
    }

    public static function sendAdminMessage(\App\User $recipient, $subject, $text)
    {
        \Mail::queue('emails.generic_message', ['text' => $text], function($message) use ($recipient, $subject, $text)
        {
            $msg = Message::create(
            [
                'subject' => $subject,
                'text' => $text,
                'recipient_id' => $recipient->id,
                'sender_id' => null,
                'date_sent' => \Carbon\Carbon::now()->toDateTimeString()
            ]);

            MessageMask::create(['user_id' => $recipient->id, 'message_id' => $msg->id]);

            $message->to($recipient->email, $recipient->real_name)->subject($subject);
        });
    }

    public static function sendServiceMessage(\App\User $sender, $subject, $text, $service)
    {
        $msg = Message::create(
        [
            'subject' => $subject,
            'text' => $text,
            'recipient_id' => null,
            'sender_id' => $sender->id,
            'service' => $service,
            'date_sent' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        return self::sendServiceMessageMsg($sender, $msg, $service);
    }

    public static function sendServiceMessageMsg(\App\User $sender, $msg, $service)
    {
        $msg->update(
        [
            'recipient_id' => null,
            'sender_id' => $sender->id,
            'service' => $service,
            'date_sent' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        $msg->save();

        $msg_id = $msg->id;
        MessageMask::create(['user_id' => $sender->id, 'message_id' => $msg_id]);

        $users = \App\User::where('role', $service)->where('active', 1)->select('id', 'email', 'real_name')->get();

        $text = $msg->text;
        $subject = $msg->subject;
        foreach($users as $user)
        {
            $user_id = $user->id;
            $email = $user->email;
            $real_name = $user->real_name;
            \Mail::queue('emails.generic_message', ['text' => $text], function ($message) use ($user_id, $email, $real_name, $subject, $text, $msg_id)
            {
                MessageMask::firstOrCreate(['user_id' => $user_id, 'message_id' => $msg_id]);
                $message->to($email, $real_name)->subject($subject);
            });
        }
        return $msg;
    }

    public static function sendNewMessage(\App\User $sender, \App\User $recipient, $subject, $text)
    {
        $msg = Message::create(
        [
            'subject' => $subject,
            'text' => $text,
            'recipient_id' => $recipient->id,
            'sender_id' => $sender->id,
            'date_sent' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        $msg_id = $msg->id;
        \Mail::queue('emails.generic_message', ['text' => $text], function($message) use ($sender, $recipient, $subject, $text, $msg_id)
        {
            MessageMask::create(['user_id' => $sender->id, 'message_id' => $msg_id]);
            MessageMask::create(['user_id' => $recipient->id, 'message_id' => $msg_id]);

            $message->to($recipient->email, $recipient->real_name)->subject('Сообщение от: '.$sender->real_name.': '.$subject);
        });
        return $msg;
    }

    public static function sendReply(\App\User $sender, Message $original, $text)
    {
        if(!$original->sender)
            return;

        $msg = Message::create(
        [
            'subject' => $original->subject,
            'text' => $text,
            'recipient_id' => $original->sender_id,
            'sender_id' => $sender->id,
            'service' => $original->service ? $original->service : null,
            'date_sent' => \Carbon\Carbon::now()->toDateTimeString(),
            'reply' => $original->reply ? $original->reply : $original->id
        ]);

        $msg_id = $msg->id;
        \Mail::queue('emails.generic_message', ['text' => $text], function($message) use ($sender, $original, $text, $msg_id)
        {

            MessageMask::create(['user_id' => $sender->id, 'message_id' => $msg_id]);
            MessageMask::create(['user_id' => $original->sender_id, 'message_id' => $msg_id]);

            $message->to($original->sender->email, $original->sender->real_name)->subject('Сообщение от: '.$sender->real_name.': '.$original->subject);
        });
        return $msg;
    }

    public function changeService($service)
    {
        $tid = $this->reply ? $this->reply : $this->id;
        $q = \App\Message::where(function($iq) use($tid)
        {
            $iq->where('reply', $tid)
                ->orWhere('id', $tid);
        });

        foreach ($q->get() as $item)
        {
            $item->service = $service;
            $item->save();
        }

        $msg_id = $this->id;
        $subject = $this->subject;
        $text = $this->text;

        $users = \App\User::where('role', $service)->where('active', 1)->select('id', 'email', 'real_name')->get();

        foreach($users as $user)
        {
            $user_id = $user->id;
            $email = $user->email;
            $real_name = $user->real_name;
            \Mail::queue('emails.generic_message', ['text' => $text], function ($message) use ($user_id, $email, $real_name, $subject, $text, $msg_id)
            {
                MessageMask::create(['user_id' => $user_id, 'message_id' => $msg_id]);
                $message->to($email, $real_name)->subject($subject);
            });
        }
    }
}
