<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;

class AddCopyright extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gallery:copyright';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Update users');
		foreach(\App\User::all() as $user)
		{
			$this->info('User: '.$user->real_name);

			$process = new Process('convert -gravity center -size 150x100 -fill blue -pointsize 12 label:"Virtual Gallery\nImage By\n\n' . $user->real_name . '\n'.$user->email.'" ' . storage_path().'/app/content/thumbs/user/'.$user->id.'_copyright.gif');
			$process->mustRun();
		}

		$this->info('Artworks');
		foreach(\App\Artwork::all() as $art)
		{
			$this->info('Artwork: '.$art->title);
			$user_id = $art->author->id;

			$cimage = storage_path().'/app/content/thumbs/user/'.$user_id.'_copyright.gif';

			$orig = $art->imagePath();
			$this->info('Fname: '.$orig);
			$art->filename = $art->id.'.png';

			try {
				$process = new Process('composite ' . $cimage . ' ' . $orig . ' -stegano 47 ' . $art->imagePath());
				$process->mustRun();
				$art->save();
			}catch(\Exception $ex)
			{
				$this->error($ex);
			}

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		];
	}

}
