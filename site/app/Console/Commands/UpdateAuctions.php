<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Auction;

class UpdateAuctions extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gallery:update_auctions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update auction statuses and send messages based on status change';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$this->updateAuctions();
	}

	protected function getArguments()
	{
		return [];
	}

	protected function getOptions()
	{
		return [];
	}

		
	protected function updateAuctions()
    {
		$this->comment(PHP_EOL.'Auction update...'.PHP_EOL); 

		try
		{
			Auction::with('bids', 'bids.bidder')->chunk(50, function($auctions)
			{
				foreach ($auctions as $auc)
				{
					if($auc->prev_status != $auc->currentStatus)
						$this->updateAuction($auc);
				}
			});			
		}catch(\Exception $e)
		{
			$this->error(PHP_EOL.'EXCEPTION: '.$e.PHP_EOL); 
		}
    }

    protected function updateAuction(Auction $auc)
    {
		try
		{
			$this->comment('Updating '.$auc->title.PHP_EOL);

			$stat = $auc->currentStatus;
			$prev_stat = $auc->prev_status;
			$auc->prev_status = $stat;
			$auc->save();


			$emls = array();
			foreach($auc->bids as $bid)
			{
				if($bid->bidder_id != $auc->winner_id)
					$emls[$bid->bidder_id] = $bid->bidder;
			}

			$emls = array_unique($emls);

			switch($stat)
			{
				case \App\AuctionStatus::Finished:
				{
					$msg = $view = \View::make('emails.auction_end_owner', ['item' => $auc])->render();
					\App\Message::sendAdminMessage($auc->owner, "Аукцион № " . $auc->id . " закончился", $msg);

					if (isset($auc->winner_id))
					{
						$msg = $view = \View::make('emails.auction_won', ['item' => $auc])->render();
						\App\Message::sendAdminMessage($auc->winner, "Вы выиграли аукцион № " . $auc->id, $msg);
					}

					$msg = $view = \View::make('emails.auction_end_participants', ['item' => $auc])->render();
					foreach (array_values($emls) as $eml)
						\App\Message::sendAdminMessage($eml, "Аукцион № " . $auc->id . " закончился", $msg);
					break;
				}
				case \App\AuctionStatus::Started:
				{
					$msg = $view = \View::make('emails.auction_started', ['item' => $auc])->render();
					\App\Message::sendAdminMessage($auc->owner, "Аукцион № " . $auc->id . " начался", $msg);
					break;
				}
			}

			$this->comment('done'.PHP_EOL);
		}catch(\Exception $e)
		{
			$this->error(PHP_EOL.'EXCEPTION: '.$e.PHP_EOL); 
		}
    }


	
}
