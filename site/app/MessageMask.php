<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageMask extends Model
{
    public $timestamps = false;

    protected $fillable = array('user_id', 'message_id');
}
