<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		$ctrs = array_keys(\App\LangResource::get(\App\ResourceType::Country));

		return Validator::make($data, [
			'name' => 'required|unique:users,name|max:255',
			'email' => 'required|email|unique:users|max:255',
			'real_name' => 'required',
			'role' => 'required|integer|in:1,2',
			'gender' => 'required|integer|in:1,2',
			'birthday' => 'required|date',
			'country' => 'required|integer|in:'.implode(',',$ctrs),
			'city' => 'required',
			'password' => 'required|confirmed|min:3',
			'recaptcha_response_field' => 'recaptcha',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
        $data['password'] = bcrypt($data['password']);
        $data['activation_code'] = str_random(32);
        $data['active'] = 0;
		$data['revealedPassword'] = '';


		$user = User::create(array_except($data, ['password_confirmation', 'recaptcha_challenge_field', 'recaptcha_response_field', 'revealedPassword']));
        if(!isset($user))
            return null;

        $data = array('name' => $user->name, 'real_name' => $user->real_name, 'code' => $data['activation_code'], 'revealedPassword' => $data['revealedPassword']);

        \Mail::queue('emails.activate_account', $data, function($message) use ($user)
        {
            $message->to($user->email, $user->name)->subject('Активация пользователя');
        });
        return $user;
	}

    public function activate($code)
    {
        $user = User::where('activation_code', '=', $code)->first();
        if(!isset($user))
            return null;
			
		$defcol = \App\Collection::create(
		[
			'title' => 'Коллекция по умолчанию',
			'description' => 'Коллекция по умолчанию. Приобритаемые работы будут добавлены сюда.',
			'visibility' => \App\Visibility::Owner,
			'allow_comments' => 0,
			'created_by' => $user->id,
			'edited_by' => $user->id,			
			'default' => 1,
			'allow_ad' => 0
		]);
			
        $user->active = 1;
        $user->activation_code = '';
        $user->save();
        return $user;
    }
}