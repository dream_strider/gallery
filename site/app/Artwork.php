<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\Process\Process;
use App\User;

class Artwork extends Model 
{
    use SoftDeletes;

    protected $guarded  = ['id', 'view_count'];
	public $canBeSold_Status = null;

	public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

	public function owner()
    {
        return $this->belongsTo('App\User');
    }

	public function auction()
    {
        return $this->hasOne('App\Auction');
    }

    public function collection()
    {
        return $this->belongsTo('App\Collection');
    }
	
	public function offers()
    {
        return $this->hasMany('App\Offer');
    }


    public function getKindAttribute()
    {
        return "art";
    }

    public function getGenreTextAttribute()
    {
        return LangResource::get(ResourceType::Genre, $this->genre);
    }

    public function getTechnicTextAttribute()
    {
        return LangResource::get(ResourceType::Technic, $this->art_technic);
    }

	public function imagePath()
	{
		return storage_path().'/app/content/artworks/'.$this->filename;
	}
	
	public function storeImage($path, $user_id, $user_email)
	{
		$cimage = storage_path().'/app/content/thumbs/user/'.$user_id.'_copyright.gif';
		$this->filename = $this->id.'.png';
		$this->save();

		$process = new Process('composite '.$cimage.' '.$path.' -stegano 47 '.$this->imagePath());
		$process->mustRun();
	}

    public function thumbPath($size)
    {
        return storage_path().'/app/content/thumbs/art/'.$this->id.'_'.$size.'.png';
    }

    public function thumbUrl($size)
    {
        return route('thumb', array('type' => 'art', 'id' => $this->id, 'size' => $size, 'hash' => md5($this->updated_at) ));
    }

    public function originalUrl()
    {
        return route('thumb', array('type' => 'art0', 'id' => $this->id, 'size' => '0', 'hash' => md5($this->updated_at) ));
    }

    public function uploadThumbUrl()
    {
        return route('thumb', array('type' => 'upl', 'id' => \Auth::user()->id, 'size' => '0', 'hash' => md5(date('r'))));
    }

    public function generateThumbs()
    {
        $img = \Image::make($this->imagePath());
		Util::makeThumb($img, 250, 250, $this->empty_fill);
		$img->interlace();
        $img->save($this->thumbPath('mid'));
		Util::makeThumb($img, 27, 27, $this->empty_fill);
        $img->save($this->thumbPath('small'));
		$img->destroy();
        $img = \Image::make($this->imagePath());
		Util::makeThumb($img, 1200, 700, $this->empty_fill);
		$img->interlace();
        $img->save($this->thumbPath('large'));
		$img->destroy();
    }

	public function onAuction()
	{
		if(!empty($this->auction))
		{
			$auc = $this->auction;
			if($auc->currentStatus < \App\AuctionStatus::Finished)
				return true;
			if($auc->currentStatus == \App\AuctionStatus::Finished  && ($auc->bids->count() > 0) && !$auc->payment_done)
				return true;
		}
		return false;
	}

	public function hasOffers()
	{
		return $this->offers()->count() > 0;
	}

	public function canBeSoldOnAuction()
	{
		if(!empty($this->rented_until))
		{
			$this->canBeSold_Status = 'app.sell_unavailable.rented';
			return false;
		}

		if($this->onAuction())
		{
			$this->canBeSold_Status = 'app.sell_unavailable.auction_pending';
			return false;
		}

		if($this->offers()->where('accepted_buyer', 1)->where('accepted_owner', 1)->count() > 0)
		{
			$this->canBeSold_Status = 'app.sell_unavailable.offer_pending';
			return false;
		}

		$this->canBeSold_Status = null;
		return true;
	}

	public function canBeSold()
	{
		if(!$this->buy_possible && !$this->rent_possible)
		{
			$this->canBeSold_Status = 'app.sell_unavailable.buy_disabled';
			return false;
		}

		if(!$this->canBeSoldOnAuction())
			return false;
				
		$this->canBeSold_Status = null;
		return true;
	}
	
	public function canBeSoldTo(User $user)
	{		
		if($user == null)
		{
			$this->canBeSold_Status = 'app.sell_unavailable.must_authenticate';
			return false;
		}
		
		if($user->id == $this->owner_id)
		{
			$this->canBeSold_Status = 'app.sell_unavailable.already_owner';
			return false;
		}
		
		if($user->role != \App\UserRole::Author && $user->role != \App\UserRole::Collector)
		{
			$this->canBeSold_Status = 'app.sell_unavailable.wrong_role';
			return false;
		}

		if(empty($user->pay_method))
		{
			$this->canBeSold_Status = 'app.sell_unavailable.no_pay_method';
			return false;
		}

		if(!$this->canBeSold())
			return false;

		$this->canBeSold_Status = null;
		return true;
	}
		
}
