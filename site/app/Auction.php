<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\AuctionStatus;

class Auction extends Model 
{
    use SoftDeletes;

    protected $guarded = ['id', 'view_count'];

	public function owner()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function winner()
    {
        return $this->belongsTo('App\User', 'winner_id');
    }

	public function artwork()
    {
        return $this->belongsTo('App\Artwork');
    }

    public function bids()
    {
        return $this->HasMany('App\Bid');
    }

	public function scopeWithMe($query)
    {
		if(!\Auth::check())
			return;
			
		$query->where(function($q)
		{
			$q->where('created_by', \Auth::user()->id)->orWhereExists(function($q)
			{
				$q->select(\DB::raw(1))
					  ->from('bids')
					  ->whereRaw('bids.auction_id = auctions.id and bids.bidder_id = '.\Auth::user()->id);
			});			
		});			
    }

	
    public function getKindAttribute()
    {
        return "auc";
    }

    public function numBidsRelation()
    {
        $a = $this->bids();
        return $a->selectRaw($a->getForeignKey() . ', count(*) as count')->groupBy($a->getForeignKey());
    }

    public function getNumBidsAttribute()
    {
        $ak = $this->numBidsRelation->first();
        return $ak ? $ak->count : 0;
    }

    public function getRemTimeAttribute()
    {
        $now = time();
        $dt1 = strtotime($this->start);
        $dt2 = strtotime($this->end);
        if($dt1 > $now)
            return $dt1 - $now;
        if($dt1 <= $now && $dt2 >= $now)
            return $dt2 - $now;
		return null;
    }

	public function getCurrentStatusAttribute()
	{
		if($this->payment_done)
			return AuctionStatus::Payed;
	
        $now = time();
        $dt1 = strtotime($this->start);
        $dt2 = strtotime($this->end);

        if($dt2 < $now)
            return AuctionStatus::Finished;

        if($dt1 <= $now && $dt2 >= $now)
            return AuctionStatus::Started;
        return AuctionStatus::NotStarted;
	}

	public function getCurrentStatusTextAttribute()
	{
		return trans('app.auction_status.'.$this->currentStatus);
	}

    public function getSellTypeTextAttribute()
    {
        return trans('app.sell_type.'.$this->sell_type);
    }

    public function getSellConditionsTextAttribute()
    {
        return trans('app.sell_conditions.'.$this->sell_conditions);
    }

    public function thumbPath($size)
    {
        return storage_path().'/app/content/thumbs/art/'.$this->artwork_id.'_'.$size.'.png';
    }

    public function thumbUrl($size)
    {
        return route('thumb', array('type' => 'art', 'id' => $this->artwork_id, 'size' => $size, 'hash' => $this->artwork ? md5($this->artwork->updated_at) : 0 ));
    }
}
