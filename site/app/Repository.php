<?php namespace App;

class Repository
{

	public static function query($type, $search=null, $ord=null)
	{
		$q = null;
		
		switch($type)
		{
			case 'art' :
            {
                $q = Artwork::where(function($query)
                {
                    self::privacyScope($query, 'owner_id');
                });


                if(!empty($search))
                    $q->where(function($query) use ($search)
                    {
                        $query->where('title','like','%'.$search.'%')
                            ->orWhere('description','like','%'.$search.'%')
                            ->orWhereExists(function($q) use ($search)
                            {
                                $q->select(\DB::raw(1))
                                    ->from('users')
                                    ->whereRaw("users.id = artworks.created_by and (users.name like '%".$search."%' or users.real_name like '%".$search."%')");
                            })
                            ->orWhereExists(function($q) use ($search)
                            {
                                $q->select(\DB::raw(1))
                                    ->from('users')
                                    ->whereRaw("users.id = artworks.owner_id and (users.name like '%".$search."%' or users.real_name like '%".$search."%')");
                            });
                    });
            } break;

            case 'col' :
				$q = Collection::where(function($query)
				{
					self::privacyScope($query);
				}); break;
			case 'auc' :
				$q = Auction::where(function($query)
				{
					self::privacyScope($query);
				}); break;
			case 'show' :
				$q = Slideshow::where('new', '=', 0)->where(function($query)
				{
					self::privacyScope($query);
				}); break;
			case 'offer' :
				$q = Offer::with('artwork')->where(function($query)
				{
					$query->where('buyer_id', \Auth::user()->id)->orWhereHas('artwork', function($query)
					{
						$query->where('owner_id', \Auth::user()->id);
					});
				}); break;
            case 'usr' :
                $q = User::where('active', 1)->whereIn('role', [\App\UserRole::Author, \App\UserRole::Collector]); break;
            case 'msg' :
                //$q = Message::where('recipient_id', '=', \Auth::user()->id); break;
				$q = Message::where(function($iq)
				{
					$iq->where('recipient_id', '=', \Auth::user()->id)
						->orWhere('sender_id', '=', \Auth::user()->id)
						->orWhere('service', '=', \Auth::user()->role);
				})->join(\DB::raw('(select (CASE WHEN reply IS null THEN id ELSE reply END) as thread, MAX(id) as latest_id from messages where recipient_id='.\Auth::user()->id.' or sender_id='.\Auth::user()->id.' or service='.\Auth::user()->role.' group by thread) msg'), 'messages.id', '=', 'msg.latest_id')
				  ->join('message_masks', function ($join)
				  {
					  $join->on('messages.id', '=', 'message_masks.message_id')->where('message_masks.user_id', '=', \Auth::user()->id);
				  })->select(\DB::raw('messages.*, message_masks.read as _read'));
				break;
            case 'hfartviews' :
            {
                $q = User::where('active', 1)->whereIn('role', [\App\UserRole::Author, \App\UserRole::Collector])
                        ->join(\DB::raw('(select owner_id, MAX(view_count) as count from artworks where deleted_at is null group by owner_id) aw'), 'users.id', '=', 'aw.owner_id')
                        ->orderBy('count', 'desc');
						
				if(!empty($search))
					$q->where(function($query) use ($search)
					{
						$query->where('name','like','%'.$search.'%')
							  ->orWhere('real_name','like','%'.$search.'%')
							  ->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									  ->from('artworks')
									  ->whereRaw("artworks.owner_id = users.id and artworks.title like '%".$search."%'");
							});
					});

            } break;
            case 'hfcolviews' :
            {
                $q = User::where('active', 1)->whereIn('role', [\App\UserRole::Author, \App\UserRole::Collector])
                    ->join(\DB::raw('(select created_by, MAX(view_count) as count from collections where deleted_at is null group by created_by) col'), 'users.id', '=', 'col.created_by')
                    ->orderBy('count', 'desc');

				if(!empty($search))
					$q->where(function($query) use ($search)
					{
						$query->where('name','like','%'.$search.'%')
							  ->orWhere('real_name','like','%'.$search.'%')
							  ->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									  ->from('artworks')
									  ->whereRaw("artworks.owner_id = users.id and artworks.title like '%".$search."%'");
							});
					});
			} break;
            case 'hfexpensive' :
            {
                $q = self::query('auc')->where('payment_done', 1)->withTrashed()->with('artwork')->whereHas('artwork', function($query) use($search)
                {
                    $query->whereNull('deleted_at');
                    if(!empty($search))
                        $query->where(function($iq) use ($search)
                        {
                            $iq->where('title','like','%'.$search.'%');
                        });
                });
            } break;
            case 'hfpurchase' :
            {
                $q = User::where('active', 1)->whereIn('role', [\App\UserRole::Author, \App\UserRole::Collector])->orderBy('purchase_count', 'desc');

				if(!empty($search))
					$q->where(function($query) use ($search)
					{
						$query->where('name','like','%'.$search.'%')
							  ->orWhere('real_name','like','%'.$search.'%')
							  ->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									  ->from('artworks')
									  ->whereRaw("artworks.owner_id = users.id and artworks.title like '%".$search."%'");
							});
					});
			} break;
            case 'hfsell' :
            {
                $q = User::where('active', 1)->whereIn('role', [\App\UserRole::Author, \App\UserRole::Collector])->orderBy('sell_count', 'desc');

				if(!empty($search))
					$q->where(function($query) use ($search)
					{
						$query->where('name','like','%'.$search.'%')
							  ->orWhere('real_name','like','%'.$search.'%')
							  ->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									  ->from('artworks')
									  ->whereRaw("artworks.owner_id = users.id and artworks.title like '%".$search."%'");
							});
					});
            } break;

            case 'auc_my' :
            {
				$q = Auction::where(function($query)
                {
                    self::privacyScope($query);
                })->withMe();

				if(!empty($search))
					$q->where(function($query) use ($search)
					{
						$query->where('title','like','%'.$search.'%')
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('artworks')
									->whereRaw("artworks.id = auctions.artwork_id and artworks.title like '%".$search."%'");
							})
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('users')
									->whereRaw("users.id = auctions.created_by and (users.name like '%".$search."%' or users.real_name like '%".$search."%')");
							});
					});
			
                $q->with('artwork')->with('owner');
				
				switch($ord)
				{
					case 'stake': $q->orderBy('final_stake', 'desc'); break;
					case 'bids': 
						$q->leftJoin('bids', 'auctions.id', '=', 'bids.auction_id');
						$q->groupBy('auctions.id');
						$q->addSelect('auctions.*');
						$q->addSelect(\DB::raw('count(bids.id) as num_bids'));				
						$q->orderBy('num_bids', 'desc'); break;
					default: $q->orderBy('created_at', 'desc'); break;
				}
            }break;
            case 'auc_active' :
            {
                $q = Auction::where(function($query)
                {
                    self::privacyScope($query);
                });
                $q->with('artwork')->with('owner');
                $q->where('start' ,'<=', new \DateTime('today'))->where('end' ,'>', new \DateTime('today'));

                if(!empty($search))
                    $q->where(function($query) use ($search)
                    {
                        $query->where('title','like','%'.$search.'%')
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('artworks')
									->whereRaw("artworks.id = auctions.artwork_id and artworks.title like '%".$search."%'");
							})
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('users')
									->whereRaw("users.id = auctions.created_by and (users.name like '%".$search."%' or users.real_name like '%".$search."%')");
							});
                    });
										
				switch($ord)
				{
					case 'stake': $q->orderBy('final_stake', 'desc'); break;
					case 'bids': 
						$q->leftJoin('bids', 'auctions.id', '=', 'bids.auction_id');
						$q->groupBy('auctions.id');
						$q->addSelect('auctions.*');
						$q->addSelect(\DB::raw('count(bids.id) as num_bids'));				
						$q->orderBy('num_bids', 'desc'); break;
					default: $q->orderBy('created_at', 'desc'); break;
				}					
            }break;
            case 'auc_past' :
            {
                $q = Auction::where(function($query)
                {
                    self::privacyScope($query);
                });
                $q->withTrashed()->where('end' ,'<', new \DateTime('today'))->with('artwork')->with('owner')->with('winner');

                if(!empty($search))
                    $q->where(function($query) use ($search)
                    {
                        $query->where('title','like','%'.$search.'%')
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('artworks')
									->whereRaw("artworks.id = auctions.artwork_id and artworks.title like '%".$search."%'");
							})
							->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									->from('users')
									->whereRaw("users.id = auctions.created_by and (users.name like '%".$search."%' or users.real_name like '%".$search."%')");
							});
                    });
					
				switch($ord)
				{
					case 'stake': $q->orderBy('final_stake', 'desc'); break;
					case 'bids': 
						$q->leftJoin('bids', 'auctions.id', '=', 'bids.auction_id');
						$q->groupBy('auctions.id');
						$q->addSelect('auctions.*');
						$q->addSelect(\DB::raw('count(bids.id) as num_bids'));				
						$q->orderBy('num_bids', 'desc'); break;
					default: $q->orderBy('created_at', 'desc'); break;
				}
            }break;
		}
		
		if(!isset($q))
		{
			\Log::warning('Wrong query type for Repository::query - '.$type);
			return;
		}
		
		return $q;
	}
	
	
	
	
	
	private static function privacyScope($query, $field = 'created_by')
	{
		if(\Auth::check() && \Auth::user()->role == UserRole::Moderator)
			return;

		$query->where('visibility','=',0);

		$authenticated = \Auth::check();
		if($authenticated)
			$query->orWhere('visibility','=',1)->orWhere(function($query) use ($field)
			{
				$query->where('visibility', '=', 2)
					  ->where($field, '=', \Auth::user()->id);
			});
	}
	
}
