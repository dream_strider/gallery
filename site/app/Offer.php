<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = array('buyer_id', 'artwork_id', 'price', 'rent_period', 'accepted_buyer', 'accepted_owner');

    public function buyer()
    {
        return $this->belongsTo('App\User');
    }

    public function artwork()
    {
        return $this->belongsTo('App\Artwork');
    }

}
