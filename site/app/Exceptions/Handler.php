<?php namespace App\Exceptions;

use Auth;
use Exception;

//use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Bugsnag;
use Bugsnag\BugsnagLaravel\BugsnagExceptionHandler as ExceptionHandler;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		$user = Auth::user();
		if(isset($user))
/*
            try {
                Bugsnag::setUser(array(
                    'name' => $user->name,
                    'email' => $user->email
                ));
            }catch(Exception $ex){}
*/
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
        if(\Config::get('app.debug'))
            return parent::render($request, $e);

        if ($this->isHttpException($e) || $e instanceof \Symfony\Component\Security\Core\Exception\AuthenticationException)
        {
            if($e->getStatusCode() == 403)
            {
                if ($request->ajax())
                    return response()->json(['error' => $e->getMessage()], 403);
                else
                {
                    $path = $request->fullUrl();
                    \Session::put('url.intended', $path);
                    return redirect('auth/login');
                }
            }
            if ($request->ajax())
                return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
            else
                return $this->renderHttpException($e);
        }
        else
        {
            if ($request->ajax())
                return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
            else
		        return parent::render($request, $e);
        }
	}
}
