<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/hall-of-fame/{crit?}', 'HomeController@fame');
Route::get('/legal', 'HomeController@legal');
Route::get('/mission', 'HomeController@mission');
Route::get('/search', 'HomeController@search');
Route::get('/contacts', 'HomeController@contacts');
Route::get('/checkcopy/{mid?}', 'HomeController@checkcopy');
Route::get('/imagetocheck/{type}/{mid}', 'HomeController@imagetocheck');
Route::post('/checkcopy/{mid?}', 'HomeController@post_checkcopy');
Route::post('/contacts/webmaster', 'HomeController@contacts_webmaster');
Route::get('/report/{what}/{id}', 'HomeController@report');
Route::post('/report', 'HomeController@send_report');

Route::post('/ajax', 'AjaxController@index');
Route::get('thumb/{type}/{id}/{size}/{hash}', ['as' => 'thumb', 'uses' => 'ThumbController@thumb']);

Route::get('/users', 'UserController@index');
Route::get('/user/profile/{user?}', 'UserController@profile');
Route::get('/user/edit/{user?}', 'UserController@edit');
Route::post('/user/save', 'UserController@save');
Route::post('/user/upload', 'UserController@upload');
Route::get('/user/list/{action}', 'UserController@userlist');
Route::get('/user/emulate/{user}', 'UserController@emulate');
Route::get('/user/delete/{user}', 'UserController@delete');

Route::get('/artwork/list', 'ArtworkController@index');
Route::get('/artwork/add', 'ArtworkController@add');
Route::get('/artwork/edit/{id?}', 'ArtworkController@edit');
Route::get('/artwork/delete/{id}', 'ArtworkController@delete');
Route::get('/artwork/fullscreen/{art}', 'FullscreenController@artwork');
Route::post('/artwork/save', 'ArtworkController@save');
Route::post('/artwork/upload', 'ArtworkController@upload');
Route::get('/artwork/buy/{id}', 'ArtworkController@make_offer');
Route::get('/artwork/offer/{id?}', ['as' => 'view_offer', 'uses' => 'ArtworkController@view_offer']);
Route::post('/artwork/offer', 'ArtworkController@update_offer');
Route::post('/artwork/submit', 'ArtworkController@submit_offer');
Route::get('/artwork/offer/{id}/pay', 'ArtworkController@pay_offer');
Route::get('/artwork/offer/{id}/dopay', 'ArtworkController@dopay_offer');
Route::get('/artwork/cantsell/{art_id}/{usr_id?}', ['as' => 'cant_sell', 'uses' => 'ArtworkController@cantSell']);


Route::get('/collection/list', 'CollectionController@index');
Route::get('/collection/edit/{id?}', 'CollectionController@edit');
Route::get('/collection/delete/{id}', 'CollectionController@delete');
Route::post('/collection/save', 'CollectionController@save');

Route::get('/auctions/{crit?}/{ord?}', 'AuctionController@index');
Route::get('/auction/start/{id}', 'AuctionController@start');
Route::post('/makebid', 'AuctionController@makebid');
Route::post('/auction/create', 'AuctionController@create');
Route::get('/auction/delete/{id}', 'AuctionController@delete');
Route::get('/auction/pay/{auc}', 'AuctionController@pay');
Route::get('/auction/dopay/{auc}', 'AuctionController@dopay');
Route::get('/auction/history/{art}', 'AuctionController@history');

Route::get('/slideshow/create', 'SlideshowController@create');
Route::get('/slideshow/edit/{show?}', 'SlideshowController@edit');
Route::get('/slideshow/delete/{id}', 'SlideshowController@delete');
Route::get('/slideshow/params/{show?}', 'SlideshowController@params');
Route::get('/slideshow/save', 'SlideshowController@save');
Route::get('/slideshow/fullscreen/{show?}', 'FullscreenController@slideshow');

Route::post('/comment', 'CommentController@add');
Route::post('/comment/delete_all/{thread}', 'CommentController@deleteAll');
Route::post('/comment/delete/{id}', 'CommentController@delete');
Route::post('/comment/disable/{thread}', 'CommentController@disable');
Route::post('/comment/enable/{thread}', 'CommentController@enable');

Route::get('/messages', 'MessageController@index');

Route::group(['prefix' => 'wm', 'as'=>'webmaster', 'middleware' => 'webmaster'], function()
{
	Route::get('messages', 'WebmasterController@messages');
	Route::get('users', 'WebmasterController@users');
	Route::get('createuser', 'WebmasterController@get_adduser');
	Route::post('add_user', 'WebmasterController@add_user');
	Route::get('genres', 'WebmasterController@genres');
	Route::get('technics', 'WebmasterController@technics');
	Route::get('devices', 'WebmasterController@devices');
	Route::get('logout', 'WebmasterController@logout');
	Route::get('catitem/{type}/{id?}', 'WebmasterController@catitem');
	Route::post('catitem/{type}/{id}', 'WebmasterController@post_catitem');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
