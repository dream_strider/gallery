<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Webmaster {

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || $this->auth->user()->role != \App\UserRole::Webmaster)
            return redirect()->guest('auth/login');

        return $next($request);
    }

}
