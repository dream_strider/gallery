<?php namespace App\Http\Middleware;

use Closure;

class ClientCache
{
	public function handle($request, Closure $next)
	{
		$response = $next($request);

        $response->header('Cache-Control', 'private, max-age=604800');

        return $response;
	}

}
