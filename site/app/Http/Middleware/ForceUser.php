<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class ForceUser
{
    public function handle($request, Closure $next)
    {
		if(Auth::check() && Auth::User()->role == \App\UserRole::Webmaster &&
			!(
				strpos($request->path(), 'wm/') === 0 ||
				strpos($request->path(), 'thumb/') === 0 ||
				(strpos($request->path(), 'user/') === 0 && strpos($request->path(), 'user/profile') !== 0) ||
				strpos($request->path(), 'ajax') === 0
			)
		)
			return redirect('wm/messages');

		if($request->path() != 'auth/login')
			\Session::forget('url.intended');
		$user_id = $request->input('fu');		
		if(isset($user_id))
		{
			if(!Auth::check() || Auth::user()->id != $user_id)
				abort(403);
		}
        return $next($request);
    }

}
