<?php namespace App\Http\Middleware;

use Closure;
use Cookie;
use App;

class Lang
{

    public function handle($request, Closure $next)
    {
        $locations = array('en' => 'en_US.UTF8', 'ru' => 'ru_RU.UTF8');

		$lang = 'ru';
		
        /*$lang = $request->cookie('lang');
        if(empty($lang))
        {
			$lang = 'ru';
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            if(empty($lang))
                $lang = 'en';
            Cookie::queue('lang', $lang);
        }*/
        App::setLocale($lang);
        setlocale(LC_ALL, $locations[$lang]);

        return $next($request);
    }

}
