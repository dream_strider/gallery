<?php namespace App\Http\Middleware;

use Closure;

class LastAction
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($request->isMethod('post'))
            \Session::put('last_action', $request->path());

        return $response;
    }

}
