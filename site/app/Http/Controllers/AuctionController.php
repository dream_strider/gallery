<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auction;
use App\Bid;

class AuctionController extends Controller {

    public function __construct()
    {
    }

    public function index(Request $request, $crit = 'all', $ord = 'new')
    {
        $search = $request->input('q');

        return view('auctions', ['crit' => $crit, 'ord' => $ord, 'search' => $search]);
    }

    public function makebid(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $id = $request->input('auc_id');
        $bid = (int)$request->input('bid');
        $cmt = $request->input('comment');

        $auc = Auction::find($id);
        if(!isset($auc))
            abort(404);

        if($auc->created_by == \Auth::user()->id)
            abort(403);

        if($auc->currentStatus != \App\AuctionStatus::Started)
            return response()->json(
            [
                'error' => 'Торги завершены',
            ], 500);

        $minStake = $auc->final_stake > 0 ? $auc->final_stake + $auc->stake_step : $auc->initial_stake;
        if($bid < $minStake)
            return response()->json(
            [
                'error' => 'Ставка должна быть выше минимальной',
            ], 422);

        Bid::create(
        [
            'auction_id' => $id,
            'stake' => $bid,
            'bidder_id' => \Auth::user()->id,
            'comment' => $cmt
        ]);

        $auc->winner_id = \Auth::user()->id;
        $auc->final_stake = $bid;
        $auc->final_stake_date = \Carbon\Carbon::now();
        $auc->save();

        $auc->winner = \Auth::user();

        $emls = array();
        foreach($auc->bids as $bid)
            if($bid->bidder_id != $auc->winner_id)
                $emls[$bid->bidder_id] = $bid->bidder;

        $emls = array_unique($emls);

        $msg = $view = \View::make('emails.auction_bid_owner',['item' => $auc])->render();
        \App\Message::sendAdminMessage($auc->owner,  "Новая ставка на Аукционе № ".$auc->id, $msg);

        $msg = $view = \View::make('emails.auction_bid_bidder',['item' => $auc])->render();
        \App\Message::sendAdminMessage($auc->winner,  "Новая ставка на Аукционе № ".$auc->id, $msg);

        $msg = $view = \View::make('emails.auction_bid_participants',['item' => $auc])->render();
        foreach(array_values($emls) as $eml)
            \App\Message::sendAdminMessage($eml, "Новая ставка на Аукционе № ".$auc->id, $msg);

        return response()->json('OK');
    }

    public function start($id)
    {
        if(!\Auth::check())
            abort(403);

        $art = \App\Artwork::find($id);
        if(empty($art))
            abort(404, 'Artwork not found');

		if(\Auth::user()->id != $art->owner_id)
			abort(403);
		
        if(!$art->canBeSoldOnAuction())
            return redirect()->route('cant_sell', ['art_id' => $id]);

        return view("add-auction",
        [
            'item' => $art,
        ]);
    }

    public function create(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $art = \App\Artwork::find($request->input('art'));
        if(empty($art))
            abort(404, 'Artwork not found');

		if(\Auth::user()->id != $art->owner_id)
			abort(403);
		
        $auc = $art->auction;

        if(!$art->canBeSoldOnAuction())
            return redirect()->route('cant_sell', ['art_id' => $art->id]);

        $rules = [
            'sell_type' => 'boolean',
            'rent_period' => 'required_if:sell_type,1|integer|min:1',
            'repeat_possible' => 'boolean',
            'sell_conditions' => 'required|integer|in:0,1',
            'initial_stake' => 'required|integer|min:50',
            'stake_step' => 'required|integer|min:50',
            'auction_start_date' => 'required|date|date_format:Y-m-d',
            'auction_end_date' => 'required|date|date_format:Y-m-d',
            'auction_start_time' => 'required',
            'auction_end_time' => 'required',
            'description' => 'string',
        ];

        $this->validate($request, $rules);

        unset($rules['auction_start_date']);
        unset($rules['auction_end_date']);
        unset($rules['auction_start_time']);
        unset($rules['auction_end_time']);

        $start = $request->input('auction_start_date').' '.$request->input('auction_start_time');
        $end = $request->input('auction_end_date').' '.$request->input('auction_end_time');

        $st = strtotime($start);
        $en = strtotime($end);

        $stmin = strtotime("+1 hour");

        if(strtotime($request->input('auction_start_date').' 00:00') < strtotime(date('Y-m-d')))
            return redirect()->back()->withInput()->withErrors(
                [
                    'error' => 'Дата начала аукциона должна быть позже, чем текущая дата',
                ]);

        if($st < $stmin)
            return redirect()->back()->withInput()->withErrors(
            [
                'error' => 'Аукцион не может начаться ранее, чем через час',
            ]);

        if($en - $st < 3600)
            return redirect()->back()->withInput()->withErrors(
            [
                'error' => 'Аукцион не может длиться менее 1 часа',
            ]);

        if(isset($auc))
            $art->auction->delete();

        $auc = new Auction;
        $auc->created_by = \Auth::user()->id;
        $auc->edited_by = \Auth::user()->id;
        $auc->artwork_id = $art->id;
        $auc->start = $start;
        $auc->end = $end;
        $auc->visibility = 0;

        $auc->fill($request->only(array_keys($rules)));

        if(!$auc->save())
        {
            return redirect()->back()->withInput()->withErrors(
            [
                'error' => 'Ошибка создания аукциона',
            ]);
        }

        $auc->title = 'Лот '.$auc->id;
        $auc->save();

        $msg = \View::make('emails.auction_created',['item' => $auc])->render();
        \App\Message::sendAdminMessage(\Auth::user(), "Аукцион № ".$auc->id." создан", $msg);

        return redirect('user/profile?p=focused&rowid=auc&itemid='.$auc->id);
    }

    public function delete($id)
    {
        if (!\Auth::check() || \Auth::user()->role != \App\UserRole::Moderator)
            abort(403);

        $auc = Auction::withTrashed()->find($id);
        if(!$auc)
            abort(404);

        foreach($auc->bids as $bid)
            $bid->forceDelete();
        $auc->forceDelete();
        $auc->forceDelete();
        return redirect('/');
    }

    public function pay(Auction $auc)
    {
        if(!($auc->currentStatus == \App\AuctionStatus::Finished && $auc->winner_id == \Auth::user()->id))
            abort(403);
        if($auc->payment_done)
            abort(403);

        return view('auction_pay', ['item' => $auc]);
    }

    public function dopay(Auction $auc)
    {
        if(!($auc->currentStatus == \App\AuctionStatus::Finished && $auc->winner_id == \Auth::user()->id))
            abort(403);
        if($auc->payment_done)
            abort(403);

        $art = $auc->artwork;
        $seller = $art->owner;
        $purchaser = $auc->winner;
        $price = $auc->final_stake;
        $art->prev_owner_id = $seller->id;
        $art->owner_id = $purchaser->id;
        $auc->payment_done = 1;

        if($auc->sell_type == \App\SellType::Rent)
            $art->rented_until = \Carbon\Carbon::now()->addDay($auc->rent_period)->toDateTimeString();
        else
            $art->rented_until = null;

        $defcol = \App\Collection::where('created_by', $purchaser->id)->where('default', 1)->first();
        $art->collection_id = $defcol->id;

        $seller->sell_count++;
        $purchaser->purchase_count++;

		\App\Slide::where('artwork_id', $art->id)->delete();
			
		
        $auc->save();
        $art->save();
        $seller->save();
        $purchaser->save();

        $msg = $view = \View::make('emails.auction_payed_owner',['item' => $auc])->render();
        \App\Message::sendAdminMessage($seller, "Аукцион № ".$auc->id." оплачен", $msg);

        $msg = $view = \View::make('emails.auction_payed_bidder',['item' => $auc])->render();
        \App\Message::sendAdminMessage($purchaser, "Аукцион № ".$auc->id." оплачен", $msg);

        return view('auction_payment_done', ['item' => $auc]);
    }

    public function history(\App\Artwork $art)
    {
        if(!$art->id)
            abort(404);
        if(!$art->auction_history)
            abort(403);

        $list = Auction::withTrashed()->where('artwork_id', $art->id)->where('end' ,'<', new \DateTime('today'))->with('artwork')->with('owner')->with('winner')->orderBy('created_at', 'desc')->get();
        return view('auction_history', ['art' => $art, 'list' => $list]);
    }
}
