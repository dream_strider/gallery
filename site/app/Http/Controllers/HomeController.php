<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;


class HomeController extends Controller {

	public function __construct()
	{
	}

	public function index()
	{
		return view('home');
	}

    public function fame(Request $request, $crit = 'all')
    {
		$search = $request->input('q');

        return view('hall_of_fame', ['crit' => $crit, 'search' => $search]);
    }

    public function legal()
    {
        return view('legal');
    }

    public function mission()
    {
        return view('mission');
    }

    public function search(Request $request)
    {
        $search = $request->input('q');
        return view('search', ['search' => $search]);
    }

    public function contacts()
    {
        return view('contacts',
        [
            'success' => session('success'),
        ]);
    }

    public function checkcopy($mid = null)
    {
        if(!\Auth::check())
            abort(403);

        if(!isset($mid))
            return view('checkcopy');

        $msg = \App\Message::find($mid);
        if(!isset($msg))
            abort(404);

        return view('getcopyright', ['msg' => $msg, 'is_moderator' => \Auth::user()->role == \App\UserRole::Moderator, 'success' => false]);
    }

    public function imagetocheck($type, $mid)
    {
        if(!\Auth::check())
            abort(403);

        $msg = \App\Message::find($mid);
        if(!isset($msg))
            abort(404);

        $is_moderator = \Auth::user()->role == \App\UserRole::Moderator;
        if($type == 'c' && !$is_moderator)
            abort(403);

        $path = $type =='c' ? storage_path().'/app/content/validation/'.$msg->id.'_result.gif' : storage_path().'/app/content/validation/'.$msg->id.'.png';

        $headers = [
            'Content-Type' => image_type_to_mime_type(exif_imagetype($path)),
            'Content-Length' => filesize($path)
        ];

        $file = file_get_contents($path);
        return \Response::make( $file, 200, $headers );
    }

    public function post_checkcopy(Request $request, $mid = null)
    {
        if(!\Auth::check())
            abort(403);

        $is_moderator = \Auth::user()->role == \App\UserRole::Moderator;
        if(!isset($mid))
        {
            $validator = \Validator::make($request->all(), [
                'file' => 'required|image',
            ]);

            if ($validator->fails())
            {
                $this->throwValidationException(
                    $request, $validator
                );
            }

            $image = $request->file('file');

            $msg = \App\Message::create(
            [
                'subject' => 'Проверка происхождения изображения',
                'text' => '',
                'sender_id' => \Auth::user()->id,
                'date_sent' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
            $mid = $msg->id;

            $view = \View::make('emails.checkcopy', ['mid' => $mid]);
            $msg->update(
            [
                'text' => $view->render()
            ]);
            $msg->save();

            \App\Message::sendServiceMessageMsg(\Auth::user(), $msg, \App\UserRole::Moderator);

            $process = new Process('convert -size 150x100+47 stegano:'.$image->getPathname().' '.storage_path().'/app/content/validation/'.$mid.'_result.gif');
            $process->mustRun();

            $img = \Image::make($image->getPathname());
            \App\Util::makeThumb($img, 250);
            $img->save(storage_path().'/app/content/user/upload_th'.\Auth::user()->id.'.png');
            $img->save(storage_path().'/app/content/validation/'.$mid.'.png');
            $img->destroy();

            return route('thumb', array('type' => 'upl', 'id' => \Auth::user()->id, 'size' => '0', 'hash' => md5(date('r'))));
        }

        if(!$is_moderator)
            abort(403);
        $msg = \App\Message::find($mid);
        if(!isset($msg))
            abort(404);

        $result = 0;
        if($request->input('submit') == 'YES')
            $result = 1;

        $text = $request->has('text') ? $request['text'] : '';
        $view = \View::make('emails.checkcopy_comment', ['mid' => $mid, 'result' => $result, 'comment' => $text]);

        $msg = \App\Message::sendReply(\Auth::user(), $msg, $view->render());
        $msg->_read = 1;
        $msgmask = \App\MessageMask::where('user_id', \Auth::user()->id)->where('message_id', $msg->id)->first();

        if($msgmask)
        {
            $msgmask->read = 1;
            $msgmask->save();
        }

        return redirect('/messages');
    }

    public function contacts_webmaster(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $validator = \Validator::make($request->all(), [
            'subject' => 'required|max:128',
            'text' => 'required|max:1024',
        ]);

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $subject = $request["subject"];
        $text = $request["text"];

        \App\Message::sendServiceMessage(\Auth::user(), 'to:Webmaster - '.$subject, $text, \App\UserRole::Moderator);

        return redirect()->back()->withInput()->with(
        [
            'success' => true,
        ]);
    }

    public function report($what, $id)
    {
        if(!\Auth::check())
            abort(403);

        switch($what)
        {
            case 'usr':
            case 'art':
            case 'col':
            case 'auc':
               $item = \App\Repository::query($what)->find($id); break;
            default:
                abort(404);
        }

        if(!$item)
            abort(404);

        return view('report',
        [
            'success' => session('success'),
            'what' => $what,
            'id' => $id,
            'item' => $item
        ]);
    }

    public function send_report(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $what = $request["what"];
        $id = $request["id"];
        switch($what)
        {
            case 'usr':
            case 'art':
            case 'col':
            case 'auc':
                $item = \App\Repository::query($what)->find($id); break;
            default:
                abort(404);
        }

        if(!$item)
            abort(404);

        $validator = \Validator::make($request->all(), [
            'text' => 'required|max:1024',
        ]);

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $text = $view = \View::make('emails.report',['what' => $what, 'item' => $item, 'text' => $request["text"]])->render();

        \App\Message::sendServiceMessage(\Auth::user(), 'Жалоба', $text, \App\UserRole::Moderator);

        return redirect()->back()->withInput()->with(
        [
            'success' => true,
        ]);
    }
}
