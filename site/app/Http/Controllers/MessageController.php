<?php namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Message;
use Carbon\Carbon;
	
class MessageController extends Controller
{
	public function index()
    {
		if(!\Auth::check())
			abort(403);
        return view('messages');
    }


}
