<?php namespace App\Http\Controllers;

use Carbon\Carbon;

class ThumbController extends Controller
{

    public function thumb($type, $id, $size, $hash)
    {
        $path = storage_path().'/app/content/thumbs/'.$type.'/'.$id.'_'.$size.'.png';
        if(!file_exists($path))
        {
            switch($type)
            {
                case 'user':
                    $path = storage_path().'/app/images/noava.jpg'; break;
                case 'upl':
                    $path = storage_path().'/app/content/user/upload_th'.$id.'.png'; $hash=0; break;
                case 'art0':
                    if(!\Auth::check())
                        abort(403);
                    $art = \App\Artwork::find($id);
                    if($art->owner_id != \Auth::user()->id)
                        abort(403);
                    $path = $art->imagePath(); break;
                default:
                    $path = storage_path().'/app/images/nophoto.jpg';
            }
        }

        return $this->getImage($path, $hash);
    }

    private function getImage($path, $hash)
    {
        $request = \Request::instance();

        $modhdr = $request->header('If-None-Match');

        if(isset($modhdr) && $hash == trim($modhdr,'"'))
            return \Response::make(null, 304);

        $headers = [
            'Content-Type' => image_type_to_mime_type(exif_imagetype($path)),
            'Content-Length' => filesize($path)
        ];

        $file = file_get_contents($path);
        $response = \Response::make( $file, 200, $headers );

        $dt = Carbon::now()->addDay(30);

        if($hash !== 0)
        {
            $response->setEtag($hash);
            $response->setExpires($dt);
            $response->setPublic();
        }

        return $response;
    }

}
