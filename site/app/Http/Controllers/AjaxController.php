<?php namespace App\Http\Controllers;

class AjaxController extends Controller {

    public function __construct()
    {
    }

    public function index()
    {
        if (!\Request::ajax() || !\Request::has('requests'))
            abort(500, 'wrong Ajax request');

        $req_list = \Request::input('requests');

        $output = array();
        foreach($req_list as $req)
        {
            if(gettype($req) == 'string')
                $id = $req;
            else
            {
                $data = isset($req['data']) ? $req['data'] : [];
                $id = $req['id'];
            }

            $result = '';
            switch($id)
            {
                case 'showall':
                    $result = $this->getShowAll($data); break;

                case 'messages_block':
                    $result = \View::make('sidebar.messages_block')->render(); break;
                case 'auctions_block':
                    $result = \View::make('sidebar.auctions_block')->render(); break;
                case 'collections_block':
                    $result = \View::make('sidebar.collections_block')->render(); break;
                case 'arts_block':
                    $result = \View::make('sidebar.arts_block')->render(); break;

                case 'auc_preview_block':
                case 'col_preview_block':
                case 'art_preview_block':
                case 'usr_new_block':
                case 'usr_authors_block':
                case 'usr_collects_block':
                case 'profile_art_block':
                case 'profile_col_block':
                case 'profile_auc_block':
                case 'profile_show_block':
                case 'profile_offer_block':
                case 'hall_total':
                case 'hall_artviews':
                case 'hall_colviews':
                case 'hall_expensive':
                case 'hall_purchase':
                case 'hall_sell':
                case 'auc_my':
                case 'auc_active':
                case 'auc_past':
					$result = \View::make('content.'.$id, $data)->render(); break;
					
                case 'focused':
                    $result = $this->getFocused($data); break;

                case 'comments':
                    if(starts_with($data['thread'], 'art'))
                        $item = \App\Artwork::find(substr($data['thread'], 3));
                    else if(starts_with($data['thread'], 'col'))
                        $item = \App\Collection::find(substr($data['thread'], 3));
                    else
                        abort(404);
                    $result = \View::make('content.comment_list')->with('thread', $data['thread'])->with('item', $item)->render(); break;

				case 'usermessages':
                    $result = $this->getMessages($data); break;
                case 'message_thread':
                    $result = $this->getThreadMessages($data); break;
                case 'readmessage':
                    $result = $this->readMessage($data); break;
                case 'deletemessage':
                    $result = $this->deleteMessage($data); break;
                case 'to-webmaster':
                    $result = $this->toWebmaster($data); break;
                case 'reply':
                    $result = $this->replyMessage($data); break;

                case 'auction_params':
                {
                    $item = \App\Repository::query('auc')->where('id', '=', $data['auc_id'])->firstOrFail();
                    $result = \View::make('content.auc_params', ['item' => $item])->render(); break;
                }; break;
                case 'bids':
                {
                    $item = \App\Repository::query('auc')->where('id', '=', $data['auc_id'])->firstOrFail();
                    $result = \View::make('content.bids', ['item' => $item])->render(); break;
                }; break;

                case 'greeting':
                {
                    if(\Auth::check())
                    {
                        \Auth::user()->greeting = 0;
                        \Auth::user()->save();
                    }
                    $result = "";
                } break;

                case 'addslide':
                {
                    $result = \View::make('content.add_slide')->render(); break;
                }; break;
                case 'updshow':
                {
					$result = $this->updateSlideshow($data); break;
                }; break;
            }
            $output[] = array('id' => $id, 'data' => $result);
        }
        return response()->json($output);
    }

    public function getShowAll($data)
    {
        $ofs = (int)$data['offset'];
        $search = isset($data['search']) ? $data['search'] : null;
        $ord = isset($data['order']) ? $data['order'] : null;
        $result = '';
        switch($data['query'])
        {
            case 'art':
                foreach (\App\Repository::query('art', $search, $ord)->with('author')->with('owner')->orderBy('created_at', 'desc')->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_artwork', ['item' => $item]);
                    $result .= $view->render();
                }
                break;
            case 'col':
                foreach (\App\Repository::query('col', $search, $ord)->with('owner')->with('artworkCountRelation')->orderBy('created_at', 'desc')->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_collection', ['item' => $item]);
                    $result .= $view->render();
                }
                break;
            case 'auc':
                foreach (\App\Repository::query('auc', $search, $ord)->with('artwork')->with('owner')->orderBy('created_at', 'desc')->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_auction', ['item' => $item]);
                    $result .= $view->render();
                }
                break;
            case 'hfartviews':
            case 'hfcolviews':
                foreach (\App\Repository::query($data['query'], $search, $ord)->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_fame', ['item' => $item, 'caption' => 'просмотров', 'value' => $item->count]);
                    $result .= $view->render();
                }
				break;
            case 'hfpurchase':
                foreach (\App\Repository::query($data['query'], $search, $ord)->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_fame', ['item' => $item, 'caption' => 'куплено работ', 'value' => $item->purchase_count]);
                    $result .= $view->render();
                }
				break;
            case 'hfsell':
                foreach (\App\Repository::query($data['query'], $search, $ord)->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_fame', ['item' => $item, 'caption' => 'продано работ', 'value' => $item->sell_count]);
                    $result .= $view->render();
                }
				break;
            case 'hfexpensive':
                foreach (\App\Repository::query($data['query'], $search, $ord)->skip($ofs)->take(config('gallery.query_page_size'))->get() as $item)
                {
                    $view = \View::make('content.thumb_artwork_fame', ['item' => $item->artwork, 'price' => $item->final_stake]);
                    $result .= $view->render();
                }
                break;
            default:
                abort(500, "Wrong showall query");
        }
        return $result;
    }

    public function getFocused($data)
    {
        $item = \App\Repository::query($data['rowid'])->withTrashed()->where('id', '=', $data['itemid'])->firstOrFail();
        switch($data['rowid'])
        {
            case 'art':
				$auc = $item->auction;
				\App\ViewCount::logVisit($data['rowid'], $item->id);
                $view = \View::make('content.focused_art', 
				[
					'item' => $item,
					'canEdit' => \Auth::check() && ($item->owner_id == \Auth::user()->id || \Auth::user()->role == \App\UserRole::Moderator),
					'my' => \Auth::check() && $item->owner_id == \Auth::user()->id,
					'onAuction' => !empty($auc) && $auc->currentStatus < \App\AuctionStatus::Finished,
					'auctionId' => !empty($auc) ? $auc->id : null
				]);
                break;
            case 'col':
                \App\ViewCount::logVisit($data['rowid'], $item->id);
                $view = \View::make('content.focused_col',
                [
                    'item' => $item,
                    'my' => \Auth::check() && $item->created_by == \Auth::user()->id,
                    'isModerator' => \Auth::check() && \Auth::user()->role == \App\UserRole::Moderator
                ]);
                break;
            case 'auc':
                \App\ViewCount::logVisit($data['rowid'], $item->id);
                $view = \View::make('content.focused_auc',
                [
                    'item' => $item,
                    'my' => \Auth::check() && $item->created_by == \Auth::user()->id,
                    'isModerator' => \Auth::check() && \Auth::user()->role == \App\UserRole::Moderator
                ]);
                break;
            default:
                abort(500, "Wrong focused query");
        }
        $result = $view->render();
        return $result;
    }
	
	public function getMessages($data)
	{
        $result = '';
		
		$q = \App\Repository::query('msg')->with('sender');

        if(!empty($data['older']))
			$q->where('date_sent','<', date("Y-m-d H:i:s", $data['older']));
        else if(!empty($data['newer']))
			$q->where('date_sent','>', date("Y-m-d H:i:s", $data['newer']));

		foreach ($q->orderBy('date_sent', 'desc')->take(50)->get() as $item)
		{
			$view = \View::make('content.message', ['item' => $item]);
			$result .= $view->render();
		}
		return $result;
	}

    public function getThreadMessages($data)
    {
        $result = '';

        $msg = \App\Message::find($data['tid']);
        if(!isset($msg))
            abort(404);
        if($msg->recipient_id != \Auth::user()->id && $msg->sender_id != \Auth::user()->id && $msg->service != \Auth::user()->role)
            abort(403);

        $q = \App\Message::where(function($iq) use($data)
        {
            $iq->where('reply', $data['tid'])
                ->orWhere('id', $data['tid']);
        })->where('id', '<', $data['mid'])->with('sender');

        foreach ($q->orderBy('date_sent', 'desc')->take(50)->get() as $item)
        {
            $view = \View::make('content.reply_message', ['item' => $item]);
            $result .= $view->render();
        }
        return $result;
    }

	public function readMessage($data)	
	{
		if(!\Auth::check())
			abort(403);
	
		$mid = $data['mid'];
		$msg = \App\Message::find($mid);
		
		if(!isset($msg))
			abort(404);
        if($msg->recipient_id != \Auth::user()->id && $msg->sender_id != \Auth::user()->id && $msg->service != \Auth::user()->role)
			abort(403);

        $msg->_read = 1;
        $msgmask = \App\MessageMask::where('user_id', \Auth::user()->id)->where('message_id', $msg->id)->first();

        if($msgmask)
        {
            $msgmask->read = 1;
            $msgmask->save();
        }

        return "";
	}	

	public function deleteMessage($data)
	{
		if(!\Auth::check())
			abort(403);
	
		$mid = $data['mid'];
		$msg = \App\Message::find($mid);
		
		if(!isset($msg))
			abort(404);
        if($msg->recipient_id != \Auth::user()->id && $msg->sender_id != \Auth::user()->id && $msg->service != \Auth::user()->role)
            abort(403);

        $msgmask = \App\MessageMask::where('user_id', \Auth::user()->id)->where('message_id', $msg->id)->first();

        if($msgmask)
		    $msgmask->delete();
        return "";
	}

    public function toWebmaster($data)
    {
        if(!\Auth::check())
            abort(403);

        $mid = $data['mid'];
        $msg = \App\Message::find($mid);

        if(!isset($msg))
            abort(404);
        if($msg->recipient_id != \Auth::user()->id && $msg->sender_id != \Auth::user()->id && $msg->service != \Auth::user()->role)
            abort(403);

        $msg->changeService(\App\UserRole::Webmaster);

        $msgmask = \App\MessageMask::where('user_id', \Auth::user()->id)->where('message_id', $msg->id)->first();

        if($msgmask)
            $msgmask->delete();
        return "";
    }

    public function replyMessage($data)
    {
        if(!\Auth::check())
            abort(403);

        $mid = $data['mid'];
        $msg = \App\Message::find($mid);

        if(!isset($msg))
            abort(404);
        if($msg->recipient_id != \Auth::user()->id && $msg->service != \Auth::user()->role)
            abort(403);

        $data['text'] = filter_var($data['text'], FILTER_SANITIZE_STRING);

        \App\Message::sendReply(\Auth::user(), $msg, $data['text']);
        return $this->getThreadMessages($data);
    }

    public function updateSlideshow($data)
	{
		if(!\Auth::check())
			abort(403);
			
		$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
		if(empty($show))
			abort(404);

		$show->title = $data['title'];
		$show->randomize = $data['order'];
		
		if(isset($data['images']))
		{
			$slides = [];		
			$i = 0;
			foreach($data['images'] as $s)
			{
				$slide = new \App\Slide($s);
				$slide->num = $i++;
				$slides[] = $slide;
			}
			
			$show->slides()->delete();
			$show->slides()->saveMany($slides);
		}
		
		$show->save();
		return "";
	}
}









