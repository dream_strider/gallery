<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collection;

class CollectionController extends Controller {

    public function __construct()
    {
    }

    public function index()
    {
        return view('collections');
    }


    public function edit(Request $request, $id = 0)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

        $col = null;
        if($id == 0)
        {
            $col = new Collection;
            $col->created_by = \Auth::user()->id;
            $col->allow_comments = 1;
            $col->allow_ad = 1;
        }
        else
        {
            $col = Collection::find($id);
            if(empty($col))
                abort(404, 'Collection not found');
            if($col->created_by != \Auth::user()->id && !$isModerator)
                abort(403);
        }

        return view('edit-collection',
        [
            'item' => $col,
            'success' => session('success'),
            'isNew' => $id == 0,
            'opt' => \Input::old('opt'),
        ]);
    }

    public function save(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

        $id = $request->input("id");
        $col = null;
        if($id == 0)
        {
            $col = new Collection;
            $col->created_by = \Auth::user()->id;
        }
        else
        {
            $col = Collection::find($id);
            if(empty($col))
                abort(404, 'Collection not found');
            if($col->created_by != \Auth::user()->id && !$isModerator)
                abort(403);
        }

        $rules =
        [
            'title' => 'required|string|max:255',
            'description' => 'string',
            'visibility' => 'integer|in:0,1,2',
            'allow_comments' => 'boolean',
            'allow_ad' => 'boolean',
        ];

        $this->validate($request, $rules);

        $col->fill($request->only(array_keys($rules)));

        $col->edited_by = \Auth::user()->id;

        if(!$col->save())
        {
            return redirect()->back()->withInput()->withErrors(
            [
                'error' => 'Ошибка сохранения коллекции',
            ]);
        }

        $col->generateThumbs();

        return redirect('collection/edit/'.$col->id)->withInput()->with(
        [
            'success' => true,
        ]);
    }

    public function delete($id)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

        $col = Collection::find($id);
        if(empty($col))
            abort(404, 'Collection not found');
        if($col->created_by != \Auth::user()->id && !$isModerator)
            abort(403);

        if($col->default)
            abort(403, 'Can not delete default collection');

        $defcol = Collection::where('created_by', $col->created_by)->where('default', 1)->first();
        foreach($col->artworks as $art)
        {
            $art->collection_id = $defcol->id;
            $art->save();
        }

        $col->delete();
        $defcol->generateThumbs();

        return redirect($isModerator ? '/' : 'user/profile');
    }
}
