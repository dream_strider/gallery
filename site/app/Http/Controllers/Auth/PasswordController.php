<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

    protected $redirectTo = '/password/changed';
    protected $subject = 'Ссылка на сброс пароля';

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;
        $this->passwords->validator(function($credentials)
        {
            list($password, $confirm) = [
                $credentials['password'], $credentials['password_confirmation'],
            ];

            return $password === $confirm && mb_strlen($password) >= 3;
        });
	}

    public function getChanged()
    {
        return view('auth.password-changed');
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = $this->passwords->getUser($request->only('email'));
        if (is_null($user))
            return redirect()->back()->withErrors(['email' => trans(PasswordBroker::INVALID_USER)]);

        if(!$user->active)
        {
            $data = array('name' => $user->name, 'real_name' => $user->real_name, 'code' => $user->activation_code);
            \Mail::queue('emails.activate_account', $data, function($message) use ($user)
            {
                $message->to($user->email, $user->name)->subject('Активация пользователя');
            });
            return redirect()->back()->with('status', trans('passwords.activation_sent'));
        }

        $response = $this->passwords->sendResetLink($request->only('email'), function($m)
        {
            $m->subject($this->getEmailSubject());
        });

        switch ($response)
        {
            case PasswordBroker::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans($response));

            case PasswordBroker::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

}
