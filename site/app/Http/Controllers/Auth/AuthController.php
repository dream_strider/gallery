<?php namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	protected $redirectTo = '/user/profile';
	
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
	}
	
	protected function getFailedLoginMessage()
	{
		return 'Пользователь с данным именем и паролем не найден.';
	}

    public function getLogin()
    {
        return view('auth.login');
    }

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'name' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('name', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember'), false))
		{
            $user = $this->auth->getLastAttempted();
            if($user->active)
            {
                $this->auth->login($user, $request->has('remember'));
                return redirect()->intended($this->redirectPath());
            }
            else
                return redirect()->back()
                    ->withInput($request->only('name', 'remember'))
                    ->withErrors([
                        'name' => 'Пользователь не активирован',
                    ]);
		}

		return redirect()->back()
					->withInput($request->only('name', 'remember'))
					->withErrors([
						'name' => $this->getFailedLoginMessage(),
					]);
	}

    public function postRegister(Request $request)
    {	
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->registrar->create($request->only('name', 'real_name', 'role', 'password', 'email', 'birthday', 'gender', 'country', 'city', 'address'));
        if(isset($user))
			return redirect('/auth/waitingactivation');

        return redirect()->back()->withInput()->withErrors([
            'name' => 'Ошибка при регистрации. Пожалуйста, попробуйте заново',
        ]);
    }

    public function getActivate($code)
    {
        $user = $this->registrar->activate($code);
        if(isset($user))
        {
            $this->auth->login($user);
            return redirect('/auth/activated');
        }

        return redirect('/auth/activateerror');
    }

    public function getWaitingactivation()
    {
        return view('auth.waiting-activation');
    }

    public function getActivateerror()
    {
        return view('auth.activate-error');
    }

    public function getActivated()
    {
        return view('auth.activated');
    }
}
