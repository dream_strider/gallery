<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use App\User;

class UserController extends Controller 
{
    public function index()
    {
        return view('users');
    }

    public function userlist(Request $request, $action)
    {		
		$search = '';
		switch($action)
		{
			case 'new' : $cat = 'Новички'; $q = \App\Repository::query('usr')->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc'); break;
			case 'authors' : $cat = 'Авторы'; $q = \App\Repository::query('usr')->where('role', \App\UserRole::Author)->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc'); break;
			case 'collectors' : $cat = 'Коллекционеры'; $q = \App\Repository::query('usr')->where('role', \App\UserRole::Collector)->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc'); break;
			case 'search' : 
				$cat = 'Результаты поиска';
				$q = \App\Repository::query('usr')->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc'); 
				$search = $request->input('q');
				if(!empty($q))
				{
					$q->where(function($query) use ($search)
					{
						$query->where('name','like','%'.$search.'%')
							  ->orWhere('real_name','like','%'.$search.'%')
							  ->orWhereExists(function($q) use ($search)
							{
								$q->select(\DB::raw(1))
									  ->from('artworks')
									  ->whereRaw("artworks.owner_id = users.id and artworks.title like '".$search."'");
							});
					});
				}
				break;
		}
		
		if(!isset($q))
			abort(404);
		
		$count = $q->count();
        return view('user_list', ['list' => $q, 'cat' => $cat, 'cid' => $action, 'count' => $count, 'search' => $search]);
    }

	private static function clearUpload($id)
    {
        foreach(glob(storage_path().'/app/content/user/upload'.$id.'.*') as $file)
            unlink($file);
        foreach(glob(storage_path().'/app/content/user/upload_th'.$id.'.png') as $file)
            unlink($file);
    }

    public static function getUpload($id)
    {
        $ufs = glob(storage_path().'/app/content/user/upload'.$id.'.*');
        if(count($ufs) > 0)
            return $ufs[0];
        return null;
    }


    public function profile(Request $request, \App\User $user)
	{
		if(!isset($user->id))
			$user = \Auth::user();

		$isModerator = \Auth::check() && \Auth::user()->role == \App\UserRole::Moderator;

        if(!isset($user) || !$user->active)
            abort(403);

        $me = \Auth::check() && $user->id == \Auth::user()->id;

		if($me && $user->role == \App\UserRole::Moderator)
			return redirect('/user/edit/'.$user->id);

		$tab = $request->input('tab');
		
		return view('profile', ['user' => $user, 'tab' => $tab, 'me' => $me, 'isModerator' => $isModerator]);
	}

    public function edit(Request $request, \App\User $user)
    {
		if(!\Auth::check())
			abort(403);

		$isModerator = \Auth::user()->role == \App\UserRole::Moderator;
		$isWebMaster = \Auth::user()->role == \App\UserRole::Webmaster;

        if(!isset($user->id))
            $user = \Auth::user();

        if(\Auth::user()->id != $user->id && !$isModerator && !$isWebMaster)
            abort(403);

        self::clearUpload($request->input('id'));

		$devs = [];
		if(!empty($user->devices))
			$devs = json_decode($user->devices, true);
		$user->dev1 = count($devs) > 0 ? $devs[0] : '';
		$user->dev2 = count($devs) > 1 ? $devs[1] : '';
		$user->dev3 = count($devs) > 2 ? $devs[2] : '';
		$user->dev4 = count($devs) > 3 ? $devs[3] : '';
		$user->dev5 = count($devs) > 4 ? $devs[4] : '';
		
		$exs = [];
		if(!empty($user->exhibitions))
			$exs = json_decode($user->exhibitions, true);
		$user->exname1 = count($exs) > 0 ? $exs[0] : '';
		$user->exname2 = count($exs) > 1 ? $exs[1] : '';
		$user->exname3 = count($exs) > 2 ? $exs[2] : '';
		$user->exname4 = count($exs) > 3 ? $exs[3] : '';
		$user->exname5 = count($exs) > 4 ? $exs[4] : '';

        return view('edit-user',
        [
            'user' => $user,
			'isAuthor' => $user->role == \App\UserRole::Author,
			'isModerator' => $isModerator || $isWebMaster,
            'success' => session('success'),
			'extends' => $isWebMaster ? 'webmaster.app' : 'app',
            'opt' => \Input::old('opt'),
        ]);
    }

	public function delete(\App\User $user)
	{
		if (!\Auth::check())
			abort(403);

		if( !(\Auth::user()->role == \App\UserRole::Moderator || \Auth::user()->role == \App\UserRole::Webmaster))
			abort(403);

		if (!isset($user->id))
			abort(404);

		$user->delete();
		$rented = \App\Artwork::where('owner_id', $user->id)->whereNotNull('rented_until')->whereNotNull('prev_owner_id')->get();
		foreach($rented as $art)
		{
			$art->rented_until = null;
			$art->owner_id = $art->prev_owner_id;
			$art->prev_owner_id = null;
			$art->save();
		}

		$arts = \App\Artwork::where('owner_id', $user->id)->get();
		foreach($arts as $art)
		{
			\App\Auction::where('artwork_id', $art->id)->delete();
			$art->delete();
		}
		\App\Collection::where('created_by', $user->id)->delete();
		\App\Slideshow::where('created_by', $user->id)->delete();

		return redirect('/');
	}

    public function upload(Request $request)
    {
        if(!\Auth::check())
            abort(403);

		$isModerator = \Auth::user()->role == \App\UserRole::Moderator;
		$isWebMaster = \Auth::user()->role == \App\UserRole::Webmaster;

		if(\Auth::user()->id != $request->input('id') && !$isModerator && !$isWebMaster)
			abort(403);

        $this->validate($request,
        [
            'file' => 'required|image',
        ]);

        self::clearUpload($request->input('id'));
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();

        $img = \Image::make($file->getRealPath());
        $w = $img->width();
        $h = $img->height();
        $img->resizeCanvas($w < $h ? $h : null, $w > $h ? $w : null);
        $img->save(storage_path().'/app/content/user/upload'.$request->input('id').'.'.$ext);

        $img->resize(250, 250);
        $img->save(storage_path().'/app/content/user/upload_th'.$request->input('id').'.png');

        $url = route('thumb', array('type' => 'upl', 'id' => $request->input('id'), 'size' => '0', 'hash' => md5(date('r'))));
        return $url;
    }

    public function save(Request $request)
    {
        if(!\Auth::check())
            abort(403);

		$isModerator = \Auth::user()->role == \App\UserRole::Moderator;
		$isWebMaster = \Auth::user()->role == \App\UserRole::Webmaster;

		if(\Auth::user()->id != $request->input('id') && !($isWebMaster || $isModerator))
            abort(403);

		$user = User::find($request->input('id'));
		if(!isset($user))
			abort(403);
			
		$ctrs = array_keys(\App\LangResource::get(\App\ResourceType::Country));
		$devs = array_keys(\App\LangResource::get(\App\ResourceType::Device));

		$this->validate($request,
		[
			'real_name' => 'required',
			'email' => 'required|email',
			'password' => 'required_with:password_old|confirmed|min:3',
			'password_confirmation' => 'required_with:password_old',
			'gender' => 'required|integer|in:1,2',
			'birthday' => 'required|date',
			'pay_method' => 'in:bank,visa,master,paypal,webmoney,yandex',
			'country' => 'required|integer|in:'.implode(',',$ctrs),
			'city' => 'required',
			'dev1' => 'integer|in:'.implode(',',$devs),
			'dev2' => 'integer|in:'.implode(',',$devs),
			'dev3' => 'integer|in:'.implode(',',$devs),
			'dev4' => 'integer|in:'.implode(',',$devs),
			'dev5' => 'integer|in:'.implode(',',$devs),
		]);
			
		$pwo = $request->input('password_old');
		if(!empty($pwo)) // update password
		{
			$pw = $request->input('password');
					
			if(!\Auth::validate(['name' => $user->name, 'password' => $pwo]))
				return redirect()->back()->withInput()->withErrors(
				[
					'password_old' => "Старый пароль не подходит!",
				]);
			
			$user->password = bcrypt($pw);			
        }

		$crop = $request->input('photocrop');
        if(!empty($crop)) // update photo
        {
			$file = self::getUpload($request->input('id'));
			if(!isset($file))
				return redirect()->back()->withInput()->withErrors(
				[
					'photo' => 'Uploaded photo not found',
				]);
					
			$photo_crop = explode(',', $request->input('photocrop'));
			
			if(  count($photo_crop) != 4 || 
				(float)$photo_crop[0] < 0.0 || 
				(float)$photo_crop[1] < 0.0 || 
				(float)$photo_crop[2] > 1.0 || 
				(float)$photo_crop[3] > 1.0 || 
				(float)$photo_crop[0] > (float)$photo_crop[2] || 
				(float)$photo_crop[1] > (float)$photo_crop[3]
			  )
			{
				return redirect()->back()->withInput()->withErrors(
				[
					'photo' => 'Invalid coordinates',
				]);
			}
				
			$img = \Image::make($file);
			$w = (float)$img->width();
			$h = (float)$img->height();
			$img->crop((int)($w * ((float)$photo_crop[2] - (float)$photo_crop[0])), (int)($h * ((float)$photo_crop[3] - (float)$photo_crop[1])), (int)($w * (float)$photo_crop[0]), (int)($h * (float)$photo_crop[1]));
			
			$img->save($file);
			$user->touch();
			$user->generateThumbs($file);
        }

		$user->fill($request->only('real_name', 'email', 'birthday', 'gender', 'country', 'city', 'web', 'address', 'description', 'pay_method'));
		
		$devs = array();
		if($request->has('dev1'))
			$devs[] = $request->input('dev1');
		if($request->has('dev2'))
			$devs[] = $request->input('dev2');
		if($request->has('dev3'))
			$devs[] = $request->input('dev3');
		if($request->has('dev4'))
			$devs[] = $request->input('dev4');
		if($request->has('dev5'))
			$devs[] = $request->input('dev5');

		$exs = array();
		if($request->has('exname1'))
			$exs[] = $request->input('exname1');
		if($request->has('exname2'))
			$exs[] = $request->input('exname2');
		if($request->has('exname3'))
			$exs[] = $request->input('exname3');
		if($request->has('exname4'))
			$exs[] = $request->input('exname4');
		if($request->has('exname5'))
			$exs[] = $request->input('exname5');

		$user->devices = json_encode($devs);
		$user->exhibitions = json_encode($exs);

		if(!$user->save())
		{		
			return redirect()->back()->withInput()->withErrors(
			[
				'error' => 'Ошибка сохранения пользователя',
			]);
		}

		$process = new Process('convert -gravity center -size 150x100 -fill blue -pointsize 12 label:"Virtual Gallery\nImage By\n\n' . $user->real_name . '\n'.$user->email.'" ' . storage_path().'/app/content/thumbs/user/'.$user->id.'_copyright.gif');
		$process->mustRun();


		self::clearUpload($request->input('id'));
		return redirect()->back()->withInput()->with(
		[
			'success' => true,
		]);
    }

	public function emulate(\App\User $user)
	{
		if(!\Auth::check())
			abort(403);

		if(\Auth::user()->role != \App\UserRole::Moderator)
			abort(403);

		\Auth::login($user);
		return redirect('/');
	}
}