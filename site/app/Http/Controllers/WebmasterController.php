<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Registrar;
use App\Message;

class WebmasterController extends Controller
{
    public function messages()
    {
        return view('webmaster.messages');
    }
    public function users()
    {
        $users = \App\User::all();

        return view('webmaster.users', ['users' => $users]);
    }
    public function get_adduser()
    {
        return view('webmaster.createuser',
        [
            'success' => session('success'),
        ]);
    }
    public function genres()
    {
        $data = ['type' => \App\ResourceType::Genre, 'list' => \App\LangResource::get(\App\ResourceType::Genre)];

        return view('webmaster.genres', ['data' => $data, 'success' => session('success')]);
    }
    public function technics()
    {
        $data = ['type' => \App\ResourceType::Technic, 'list' => \App\LangResource::get(\App\ResourceType::Technic)];

        return view('webmaster.technics', ['data' => $data, 'success' => session('success')]);
    }
    public function devices()
    {
        $data = ['type' => \App\ResourceType::Device, 'list' => \App\LangResource::get(\App\ResourceType::Device)];

        return view('webmaster.devices', ['data' => $data, 'success' => session('success')]);
    }

    public function logout()
    {
        \Auth::logout();
        return redirect('/');
    }

    public function add_user(Request $request, Registrar $registrar)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:users,name|max:255',
            'email' => 'required|email|unique:users|max:255',
            'real_name' => 'required',
            'role' => 'required|integer|in:1,2,3,4',
            'password' => 'required|confirmed|min:3',
        ]);

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $data = $request->only('name', 'real_name', 'role', 'password', 'email');
        $data['revealedPassword'] = $request['password'];
        $user = $registrar->create($data);
        if(isset($user))
        {
            return redirect()->back()->with(
            [
                'success' => true,
            ]);
        }

        return redirect()->back()->withInput()->withErrors([
            'name' => 'Ошибка при регистрации. Пожалуйста, попробуйте заново',
        ]);
    }

    public function catitem($type, $id = 0)
    {
        $titles = [\App\ResourceType::Genre => 'Жанры', \App\ResourceType::Technic => 'Техники', \App\ResourceType::Device => 'Устройства'];
        if(!array_key_exists($type, $titles))
            abort(404);

        $res = null;
        if($id)
        {
            $res = \App\LangResource::get($type, $id);
            if(!$res)
                abort(404);
        }

        return view('webmaster.catitem',
            [
                'type' => $type,
                'id' => $id,
                'title' => $titles[$type] ,
                'name' => $res ? $res : '',
                'list' => \App\LangResource::get($type)
            ]);
    }

    public function post_catitem(Request $request)
    {
        $val = [
            'type' => 'required|in:'.\App\ResourceType::Genre.','.\App\ResourceType::Technic.','.\App\ResourceType::Device,
            'id' => 'required|integer',
            'action' => 'required|in:name,del',
        ];

        $validator = \Validator::make($request->all(), $val);
        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $list = \App\LangResource::get($request['type']);

        if($request['action'] == 'name')
            $val['item_name'] = 'string|required|max:100';
        if($request['action'] == 'del')
            $val['item_replace'] = 'required|in:'.implode(',', array_keys($list));

        $validator = \Validator::make($request->all(), $val);
        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        switch($request['type'])
        {
            case \App\ResourceType::Genre: $col = 'genre'; $cat = 'Жанр'; break;
            case \App\ResourceType::Technic: $col = 'art_technic'; $cat = 'Техника исполнения'; break;
            case \App\ResourceType::Device: $col = 'device'; $cat = 'Устройство'; break;
        }

        $locale = \App::getLocale();
        $new_name = $request['item_name'];
        if($request['action'] == 'del')
        {
            $repl = \App\LangResource::where('lang', $locale)->where('resource_type', $request['type'])->where('resource_id', $request['item_replace'])->first();
            if (!$repl)
                abort(404);
            $new_name = $repl->value;
        }

        $res = null;
        if($request['id'])
        {
            $res = \App\LangResource::where('lang', $locale)->where('resource_type', $request['type'])->where('resource_id', $request['id'])->first();
            if(!$res)
                abort(404);
            $old_name = $res->value;

            $msg = \View::make('emails.category_changed',['cat' => $cat, 'old_name' => $old_name, 'new_name' => $new_name])->render();
            $cur_id = $request['id'];
            \App\User::join('artworks', function ($join) use($col, $cur_id)
            {
                $join->on('users.id', '=', 'artworks.owner_id')->where('artworks.'.$col, '=', $cur_id);
            })->select('users.*')->distinct()->chunk(100, function($users) use($cat, $msg)
            {
                foreach ($users as $user)
                {
                    \App\Message::sendAdminMessage($user, 'Название поля '.$cat.' изменено', $msg);
                }
            });
        }

        if($request['action'] == 'name')
        {
            if($res)
                \App\LangResource::where('lang', $locale)->where('resource_type', $request['type'])->where('resource_id', $request['id'])->update(['value' => $request['item_name']]);
            else
            {
                $id = \App\LangResource::where('lang', $locale)->where('resource_type', $request['type'])->max('resource_id') + 1;
                \App\LangResource::create(['lang' => $locale, 'resource_type' => $request['type'], 'resource_id' => $id, 'value' => $request['item_name']]);
            }
        }

        if($request['action'] == 'del')
        {
            if(!$res)
                abort(404);

            \App\Artwork::where($col, $request['id'])->update([$col => $request['item_replace']]);
            \App\LangResource::where('lang', $locale)->where('resource_type', $request['type'])->where('resource_id', $request['id'])->delete();
        }
        
        return redirect('wm/'.($request['type']==\App\ResourceType::Genre ? 'genres' : ($request['type'] == \App\ResourceType::Technic ? 'technics' : 'devices')))->with(
        [
            'success' => true,
        ]);
    }
}