<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artwork;
use App\Slide;
use App\Slideshow;

class FullscreenController extends Controller
{

    public function artwork(Artwork $art)
    {
        $is_owner = \Auth::check() && \Auth::user()->id == $art->owner_id;
        return view('art_fullscreen', ['is_owner' => $is_owner, 'item' => $art]);
    }

    public function slideshow(Slideshow $show)
    {
		if(!$show->id)
        {
			$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
			if(empty($show))
				abort(404);
		}

        $is_owner = \Auth::check() && \Auth::user()->id == $show->created_by;
        return view('show_fullscreen', ['is_owner' => $is_owner, 'item' => $show]);
    }
}