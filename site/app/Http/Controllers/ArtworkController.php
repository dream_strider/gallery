<?php namespace App\Http\Controllers;

use Session;
use Input;
use Illuminate\Http\Request;
use App\Artwork;
use App\Collection;
use App\Offer;
use App\Comment;

class ArtworkController extends Controller {

	private static function clearUpload()
	{
        foreach(glob(storage_path().'/app/content/user/upload'.\Auth::user()->id.'.*') as $file)
            unlink($file);
        foreach(glob(storage_path().'/app/content/user/upload_th'.\Auth::user()->id.'.png') as $file)
            unlink($file);
	}

    public static function getUpload()
	{
        $ufs = glob(storage_path().'/app/content/user/upload'.\Auth::user()->id.'.*');
			if(count($ufs) > 0)
				return $ufs[0];
		return null;
	}

    public function index()
    {
        return view('artworks');
    }

    public function add()
    {
        if(!\Auth::check())
            abort(403);
        return view('add-artwork');
    }

    public function upload(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $this->validate($request,
        [
            'file' => 'required|image',
        ]);

		self::clearUpload();
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $file->move(storage_path().'/app/content/user/', 'upload'.\Auth::user()->id.'.'.$ext);

        $img = \Image::make(storage_path().'/app/content/user/upload'.\Auth::user()->id.'.'.$ext);
		\App\Util::makeThumb($img, 250);
        $img->save(storage_path().'/app/content/user/upload_th'.\Auth::user()->id.'.png');
		$img->destroy();
		
		return route('thumb', array('type' => 'upl', 'id' => \Auth::user()->id, 'size' => '0', 'hash' => md5(date('r'))));
    }

    public function edit(Request $request, $id = 0)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

		$art = null;
		if($id == 0)
		{
			$uplFile = self::getUpload();
			if(empty($uplFile))
				abort(404, 'Upload image not found');

            $img = \Image::make($uplFile);
            if(empty($img))
                abort(404, 'Upload image is invalid');

            $col = Collection::where('created_by', \Auth::user()->id)->where('default', 1)->first();

            $art = new Artwork;
			$art->created_by = \Auth::user()->id;
            $art->owner_id = \Auth::user()->id;
            $art->width = $img->width();
            $art->height = $img->height();
            $art->collection_id = $col->id;
            $art->allow_comments = 1;
            $art->auction_history = 1;
			$art->empty_fill = '#000000';
        }
		else
		{
			$art = Artwork::find($id);			
			if(empty($art))
				abort(404, 'Artwork not found');
			if($art->owner_id != \Auth::user()->id && !$isModerator)
				abort(403);
		}

        $auc = $art->auction;

        $isOwner = $art->owner_id == \Auth::user()->id;
        $canDelete = $isModerator || ($isOwner && !$art->onAuction() && !$art->hasOffers() && !$art->rented_until);
		
        $delMessage = '';
        if(!$canDelete)
        {
            if($art->onAuction())
                $delMessage = trans('app.art_delete_unavailable.auction');
            if($art->hasOffers())
                $delMessage = trans('app.art_delete_unavailable.offer');
            if($art->rented_until)
                $delMessage = trans('app.art_delete_unavailable.rented');
        }

		return view('edit-artwork',
        [
            'item' => $art,
            'success' => session('success'),
            'isNew' => $id == 0,
            'isAuthor' => $art->owner_id == $art->created_by,
            'isModerator' => $isModerator,
            'canSell' => $isOwner,
			'canDelete' => $canDelete,
            'deleteMessage' => $delMessage,
            'opt' => \Input::old('opt'),
        ]);
    }

    public function save(Request $request)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

        $id = $request->input("id");
		$art = null;
		if($id == 0)
		{
			$uplFile = self::getUpload();
			if(empty($uplFile))
				abort(404, 'Upload image not found');

			$art = new Artwork;
			$art->created_by = \Auth::user()->id;
            $art->owner_id = \Auth::user()->id;
		}
		else
		{
			$art = Artwork::find($id);			
			if(empty($art))
				abort(404, 'Artwork not found');
			if($art->owner_id != \Auth::user()->id && !$isModerator)
				abort(403);
		}

		$isAuthor = $art->owner_id == $art->created_by;
		
		$rules =
		[
			'visibility' => 'integer|in:0,1,2',
			'allow_comments' => 'boolean',
			//'device' => 'required|in:'.implode(',',array_keys(\App\LangResource::get(\App\ResourceType::Device))),
			'auction_history' => 'boolean',
			'collection_id' => 'required|integer',
		];
		
		if($isAuthor || $isModerator)
		{
            $rules = array_merge($rules,
			[
				'title' => 'required|string|max:255',
				'description' => 'string',
				'art_technic' => 'required|in:'.implode(',',array_keys(\App\LangResource::get(\App\ResourceType::Technic))),
				'genre' => 'required|in:'.implode(',',array_keys(\App\LangResource::get(\App\ResourceType::Genre))),
				'release_date' => 'integer|min:1700|max:'.date("Y"),
				'empty_fill' => 'string|regex:/^#[A-Fa-f0-9]{6}$/',
				'height' => 'required|numeric',
				'width' => 'required|numeric',
				'units' => 'integer|in:0,1',
			]);
		}

		if($isAuthor || empty($art->rented_until) || $isModerator)
		{
			$rules = array_merge($rules,
			[
				'buy_possible' => 'boolean',
				'rent_possible' => 'boolean',
				'buy_price' => 'required_if:buy_possible,1|integer|min:1',
				'rent_price' => 'required_if:rent_possible,1|integer|min:1',
			]);
		}


		
		$this->validate($request, $rules);
				
		$col = Collection::find($request->input('collection_id'));
		if(empty($col) || $col->created_by != $art->owner_id)
			return redirect()->back()->withInput()->withErrors(
			[
				'collection_id' => 'Коллекция не найдена',
			]);

		$art->fill($request->only(array_keys($rules)));

        $art->edited_by = \Auth::user()->id;
		if($request->input('buy_possible') != 1)
			$art->buy_price = null;
		if($request->input('rent_possible') != 1)
			$art->rent_price = null;

		if(!$art->save())
		{		
			return redirect()->back()->withInput()->withErrors(
			[
				'error' => 'Ошибка сохранения работы',
			]);
		}

        if($id == 0)
        {
            $art->storeImage($uplFile, \Auth::user()->id, \Auth::user()->email);
            self::clearUpload();
        }
        $art->generateThumbs();
		$col->generateThumbs();

        return redirect('artwork/edit/'.$art->id)->withInput()->with(
        [
           'success' => true,
        ]);
    }

    public function delete($id)
    {
        if(!\Auth::check())
            abort(403);

        $isModerator = \Auth::user()->role == \App\UserRole::Moderator;

        $art = Artwork::find($id);
        if(empty($art))
            abort(404, 'Artwork not found');
        if($art->owner_id != \Auth::user()->id && !$isModerator)
            abort(403);

        $auc = $art->auction;

        $canDelete = $isModerator || ( \Auth::check() && $art->owner_id == \Auth::user()->id && empty($art->rented_until) && (empty($auc) || $auc->currentStatus == \App\AuctionStatus::Payed));

        if(!$canDelete)
            abort(403);

        \App\Slide::where('artwork_id', $id)->delete();
        $art->delete();
        if(!empty($auc) && $auc->currentStatus != \App\AuctionStatus::Payed)
            $auc->delete();

        return redirect($isModerator ? '/' : 'user/profile');
    }

    public function cantSell($art_id, $usr_id)
    {
        $art = \App\Artwork::find($art_id);
        if(empty($art))
            abort(404, 'Artwork not found');

        $usr = \App\User::find($usr_id);

        $msg = '';

        if(empty($usr))
        {
            if (!$art->canBeSold())
                $msg = trans($art->canBeSold_Status);
        }
        else
        {
            if (!$art->canBeSoldTo($usr))
                $msg = trans($art->canBeSold_Status);
        }

        return view('cant_sell_art', ['msg' => $msg]);
    }

    public function make_offer($id)
    {
        if(!\Auth::check())
            abort(403);

        $art = Artwork::find($id);
        if(empty($art))
            abort(404, 'Artwork not found');

		$canBuy = $art->canBeSoldTo(\Auth::user());
        if(!$canBuy)
            return redirect()->route('cant_sell', ['art_id' => $id, 'usr_id' => \Auth::user()]);

        $offer = Offer::where('buyer_id', \Auth::user()->id)->where('artwork_id', $art->id)->first();
		if(empty($offer))
		{			
			$offer = new Offer();
			$offer->fill(['buyer_id' => \Auth::user()->id, 'artwork_id' => $art->id, 'price' => $art->buy_possible ? $art->buy_price : $art->rent_price * 12, 'rent_period' => $art->buy_possible ? null : 12]);
		}
        else
            return redirect()->route('view_offer', ['id' => $offer->id]);

        \Session::put('offer', $offer);
        return redirect()->route('view_offer');
    }

	public function view_offer($offer = null)
    {
        if(!\Auth::check())
            abort(403);

        if($offer == null)
            $offer = \Session::get('offer');
        else
            $offer = Offer::find($offer);

		if($offer == null)
            abort(404);

		$buyer = \Auth::user()->id == $offer->buyer_id;
		$owner = \Auth::user()->id == $offer->artwork->owner->id;		
		if(!$buyer && !$owner)
			abort(403);

		return view('offer',
        [
            'item' => $offer,
            'buyer' => $buyer,
            'meaccepted' => $buyer ? $offer->accepted_buyer : $offer->accepted_owner,
            'accepted' => $offer->accepted_buyer && $offer->accepted_owner
        ])->with(
        [
            'success' => Session::has('success') ? Session::get('success') : false,
        ]);
	}

	public function update_offer(Request $request)
    {
        $offer = $request->input("id");
		$new = 0;
        if($offer == null)
        {
            $offer = \Session::get('offer');
			$new = 1;
        }
        else
            $offer = Offer::find($offer);

		if($offer == null)
            abort(404);

		$buyer = \Auth::user()->id == $offer->buyer_id;
		$owner = \Auth::user()->id == $offer->artwork->owner->id;
		if(!$buyer && !$owner)
			abort(403);

        if(Input::has('accept'))
        {
            if($buyer)
                $offer->accepted_buyer = true;
            else
                $offer->accepted_owner = true;

            if(!$offer->save())
            {
                return redirect()->back()->withInput()->withErrors(
                    [
                        'error' => 'Ошибка сохранения',
                    ]);
            }

            if($offer->accepted_buyer && $offer->accepted_owner)
            {
                $msg = \View::make('emails.offer_pay_pending_buyer',['user' => \Auth::user(), 'item' => $offer])->render();
                \App\Message::sendAdminMessage($offer->buyer,  "Одобрено предложение о покупке работы ".$offer->artwork->title, $msg);

                $msg = \View::make('emails.offer_pay_pending_owner',['user' => \Auth::user(), 'item' => $offer])->render();
                \App\Message::sendAdminMessage($offer->artwork->owner,  "Одобрено предложение о покупке работы ".$offer->artwork->title, $msg);
            }

            return redirect()->back()->withInput()->with(
                [
                    'success' => true,
                ]);
        }

        $rent =  $request->input('rent') == '1';
        if(!$offer->artwork->rent_possible && $rent)
            return redirect()->back()->withInput()->withErrors(
                [
                    'error' => 'Аренда этой работы невозможна',
                ]);

        $this->validate($request,
        [
            'price' => 'required|integer|min:1',
            'rent_period' => 'integer|min:1'.($rent ? '|required' : ''),
            'comment' => 'string|max:255'
        ]);

        if($buyer)
        {
            $offer->accepted_buyer = true;
            $offer->accepted_owner = false;
        }
        else
        {
            $offer->accepted_buyer = false;
            $offer->accepted_owner = true;
        }

        $offer->price = $request->input('price');
        $offer->rent_period = $rent ? $request->input('rent_period') : null;

		if(!$offer->save())
        {
            return redirect()->back()->withInput()->withErrors(
                [
                    'error' => 'Ошибка сохранения',
                ]);
        }

        $comment = $request->input("comment");
        Comment::create(
        [
            'thread' => 'ofr' . $offer->id,
            'text' => ($offer->rent_period ? trans('app.offer_rent_comment', ['price' => $offer->price, 'rent' => $offer->rent_period]) : trans('app.offer_comment', ['price' => $offer->price])).' '.$comment,
            'approved' => 1,
            'sender_id' => \Auth::user()->id
        ]);

		if($new)
		{
			$msg = \View::make('emails.offer_initial_buyer',['item' => $offer])->render();
			\App\Message::sendAdminMessage($offer->buyer,  "Вы сделали предложение о покупке работы ".$offer->artwork->title, $msg);

			$msg = \View::make('emails.offer_initial_owner',['item' => $offer])->render();
			\App\Message::sendAdminMessage($offer->artwork->owner,  "Вам сделали предложение о покупке работы ".$offer->artwork->title, $msg);
		}
		else
		{
			$msg = \View::make('emails.offer_changed',['user' => \Auth::user(), 'item' => $offer])->render();
			\App\Message::sendAdminMessage($offer->buyer,  "Изменено предложение о покупке работы ".$offer->artwork->title, $msg);
            \App\Message::sendAdminMessage($offer->artwork->owner,  "Изменено предложение о покупке работы ".$offer->artwork->title, $msg);
		}
						
        return redirect("/artwork/offer/".$offer->id)->withInput()->with(
            [
                'success' => true,
            ]);
	}

    public function pay_offer($id)
    {
        $offer = Offer::find($id);
        if($offer == null)
            abort(404);

        if(!$offer->accepted_buyer || !$offer->accepted_owner)
            abort(403, "Unable to pay unaccepted offer");

        return view('offer_pay', ['item' => $offer]);
    }

    public function dopay_offer($id)
    {
        $offer = Offer::find($id);
        if($offer == null)
            abort(404);

        if(!$offer->accepted_buyer || !$offer->accepted_owner)
            abort(403, "Unable to pay unaccepted offer");

        $art = $offer->artwork;
        $seller = $art->owner;
        $purchaser = $offer->buyer;
        $price = $offer->price;
        $art->prev_owner_id = $seller->id;
        $art->owner_id = $purchaser->id;

        if($offer->rent_period > 0)
            $art->rented_until = \Carbon\Carbon::now()->addDay($offer->rent_period)->toDateTimeString();
        else
            $art->rented_until = null;

        $defcol = \App\Collection::where('created_by', $purchaser->id)->where('default', 1)->first();
        $art->collection_id = $defcol->id;

        $seller->sell_count++;
        $purchaser->purchase_count++;

        \App\Slide::where('artwork_id', $art->id)->delete();

        $art->save();
        $offer->delete();
        $seller->save();
        $purchaser->save();

		$msg = \View::make('emails.offer_payed_buyer',['item' => $offer])->render();
		\App\Message::sendAdminMessage($purchaser,  "Работа ".$offer->artwork->title." оплачена", $msg);

		$msg = \View::make('emails.offer_payed_owner',['item' => $offer])->render();
		\App\Message::sendAdminMessage($seller,  "Работа ".$offer->artwork->title." оплачена", $msg);
	
        return view('offer_payment_done', ['item' => $offer]);
    }

}
