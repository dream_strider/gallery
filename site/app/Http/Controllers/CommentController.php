<?php namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Comment;
use Carbon\Carbon;
use Auth;
	
class CommentController extends Controller
{

	public function add(Request $request)
	{
		$this->validate($request, 
		[
			'thread' => 'required',
			'comment' => 'required|max:255',
		]);
		
		$th = $request->input('thread');

		if(starts_with($th, 'art'))
		{
			$item = \App\Artwork::find(substr($th, 3));
			$text = "к работе ";
			$tpl = "comment_art";
		}
		else if(starts_with($th, 'col'))
		{
			$item = \App\Collection::find(substr($th, 3));
			$text = "к коллекции ";
			$tpl = "comment_col";
		}
		else
			abort(404);

		if(empty($item))
			abort(404);

		$users = [];
		$users[$item->owner->id] = $item->owner;
		foreach(Comment::where('thread', $th)->get() as $comment)
			$users[$comment->sender_id] = $comment->sender;

		$users = array_unique($users);

		$comment = Comment::create(
		[
			'thread' => $th,
			'text' => $request->input('comment'),
			'approved' => 1,
			'sender_id' => \Auth::user()->id
		]);

		$msg = \View::make('emails.'.$tpl, ['comment' => $comment, 'item' => $item])->render();
		foreach(array_values($users) as $eml)
		{
			if($eml->id != Auth::user()->id)
				\App\Message::sendAdminMessage($eml, 'Новый комментарий '.$text.$item->title, $msg);
		}

		return response()->json('OK');
	}

	public function deleteAll($thread)
	{
		if(!Auth::check())
			abort(403);

		if(starts_with($thread, 'art'))
			$item = \App\Artwork::find(substr($thread, 3));
		else if(starts_with($thread, 'col'))
			$item = \App\Collection::find(substr($thread, 3));
		else
			abort(404);

		if(empty($item))
			abort(404);

		if(!(Auth::user()->role == \App\UserRole::Moderator || Auth::user()->id == $item->owner->id))
			abort(403);

		Comment::where('thread', $thread)->delete();

		return response()->json('OK');
	}


	public function delete($id)
	{
		if(!Auth::check())
			abort(403);

		$comment = Comment::find($id);
		if(empty($comment))
			abort(404);

		if(starts_with($comment->thread, 'art'))
			$item = \App\Artwork::find(substr($comment->thread, 3));
		else if(starts_with($comment->thread, 'col'))
			$item = \App\Collection::find(substr($comment->thread, 3));
		else
			abort(404);

		if(empty($item))
			abort(404);

		if(!(Auth::user()->role == \App\UserRole::Moderator || Auth::user()->id == $item->owner->id))
			abort(403);

		$comment->delete();
		return response()->json('OK');
	}

	public function disable($thread)
	{
		if(!Auth::check())
			abort(403);

		if(starts_with($thread, 'art'))
			$item = \App\Artwork::find(substr($thread, 3));
		else if(starts_with($thread, 'col'))
			$item = \App\Collection::find(substr($thread, 3));
		else
			abort(404);

		if(empty($item))
			abort(404);

		if(!(Auth::user()->role == \App\UserRole::Moderator || Auth::user()->id == $item->owner->id))
			abort(403);

		$item->allow_comments = false;
		$item->save();

		return response()->json('OK');
	}

	public function enable($thread)
	{
		if(!Auth::check())
			abort(403);

		if(starts_with($thread, 'art'))
			$item = \App\Artwork::find(substr($thread, 3));
		else if(starts_with($thread, 'col'))
			$item = \App\Collection::find(substr($thread, 3));
		else
			abort(404);

		if(empty($item))
			abort(404);

		if(!(Auth::user()->role == \App\UserRole::Moderator || Auth::user()->id == $item->owner->id))
			abort(403);

		$item->allow_comments = true;
		$item->save();

		return response()->json('OK');
	}
}
