<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SlideshowController extends Controller
{
    public function create()
    {
        if(!\Auth::check())
            abort(403);

		$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
		if(isset($show))
			$show->delete();
			
		$show = \App\Slideshow::create(
		[
			'new' => 1,
			'title' => 'Новое Слайдшоу',			
			'created_by' => \Auth::user()->id,
			'edited_by' => \Auth::user()->id,
		]);
		
		return redirect('slideshow/edit');
	}

    public function edit(\App\Slideshow $show)
    {
        if(!\Auth::check())
            abort(403);

        if(!$show->id)
        {
			$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
			if(empty($show))
				abort(404);
            \Session::forget('edited.slideshow');
        }
		else
		{
			if($show->created_by != \Auth::user()->id)
				abort(403);
				
			$newshow = \App\Slideshow::create(
			[
				'new' => 1,
				'title' => $show->title,		
				'randomize' => $show->randomize,
				'created_by' => \Auth::user()->id,
				'edited_by' => \Auth::user()->id,
			]);
			
			$slides = [];
			foreach($show->slides as $s)
			{
				$slide = new \App\Slide($s->toArray());
				$slides[] = $slide;
			}
			
			$newshow->slides()->saveMany($slides);
			\Session::put('edited.slideshow', $show->id);
			$show = $newshow;
		}

        return view('edit-slideshow', ['item' => $show, 'success' => session('success')]);
    }

    public function params(\App\Slideshow $show)
    {
        if(!\Auth::check())
            abort(403);
			
		if(!$show->id)
        {
			$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
			if(empty($show))
				abort(404);
		}

		if($show->created_by != \Auth::user()->id)
			abort(403);
		$slides = $show->slides;

		$ret = '__order = '.$show->randomize.'; __sshowImages = '. (empty($slides) ? '[];' : json_encode($slides).';');
		$response = \Response::make($ret);
		$response->header('Content-Type', 'text/javascript');

		return $response;
	}
	
	public function save()
    {
        if(!\Auth::check())
            abort(403);
		$show = \App\Slideshow::where('created_by', \Auth::user()->id)->where('new', 1)->first();
		if(empty($show))
			abort(404);
	
		$id = \Session::get('edited.slideshow');
		if(!$id)
		{
			$show->new = 0;
		}
		else
		{
			$origshow = \App\Slideshow::find($id);
			if($origshow->created_by != \Auth::user()->id)
				abort(403);
		
			$origshow->title = $show->title;
			$origshow->randomize = $show->randomize;
		
			$slides = [];
			foreach($show->slides as $s)
			{
				$slide = new \App\Slide($s->toArray());
				$slides[] = $slide;
			}
			
			$origshow->slides()->delete();
			$origshow->slides()->saveMany($slides);
			$show->delete();
			\Session::forget('edited.slideshow');
			$show = $origshow;
		}

		$show->edited_by = \Auth::user()->id;
		$show->save();
		$show->generateThumbs();
						
		return redirect('slideshow/edit/'.$show->id)->with('success', true);
    }

    public function delete($id)
    {
        if(!\Auth::check())
            abort(403);

        $show = \App\Slideshow::find($id);
        if(empty($show))
            abort(404, 'Slideshow not found');
        if($show->created_by != \Auth::user()->id)
            abort(403);

        foreach($show->slides as $slide)
            $slide->delete();

        $show->delete();

        return redirect('user/profile');
    }
}
