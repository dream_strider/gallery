<?php namespace App;

class ViewCount
{
    public static function logVisit($res, $id)
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['Proxy-Client-IP']))
		{
			$ip=$_SERVER['Proxy-Client-IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip=explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		$ip = ip2long($ip);		
		if($ip === FALSE)
			return;
		
		$ip = sprintf('%u', $ip);

		$cnt = \DB::table('visits')->where('ip', $ip)->where('res', $res)->where('res_id', $id)->count();
		if($cnt > 0)
			return;
			
		\DB::table('visits')->insert(['ip' => $ip, 'res' => $res, 'res_id' => $id]);
		
		$table = null;
		switch($res)
		{
			case 'art': $table = 'artworks'; break;
			case 'col': $table = 'collections'; break;
			case 'auc': $table = 'auctions'; break;
		}

		if(isset($table))
			\DB::table($table)->where('id', $id)->increment('view_count');
	}
}