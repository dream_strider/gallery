<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = array('thread', 'approved', 'text', 'sender_id');

    public function sender()
    {
        return $this->belongsTo('App\User');
    }

    public static function countOf($thread)
    {
        return self::where('thread', $thread)->count();
    }

}
