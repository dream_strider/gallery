<?php namespace App;

use \Carbon\Carbon;
use \Carbon\CarbonInterval;

class Util
{
	public static function interval($seconds)
	{	
		$sec = intval($seconds);
		if($sec < 60)
			$sec = 60;
		$fmt = "";

		$days = intval($sec / 86400);
		$hours = bcmod(intval($sec / 3600), 24);
		$minutes = bcmod(intval($sec / 60), 60);

		if($days > 0)
			$fmt .= "%d".trans('common.days_short');
		if($days > 0 || $hours > 0)
			$fmt .= "%h".trans('common.hours_short');
		if($hours > 0 || $minutes > 0)
			$fmt .= "%i".trans('common.minutes_short');
			
		if(empty($fmt))
			$fmt = "%im";
		
		$interval = new \DateInterval('P'.$days.'DT'.$hours.'H'.$minutes.'M');
		return $interval->format($fmt);
	}
	
	public static function timeAgo($date)
	{	
		$now = time();
		$tm = strtotime($date);

		$fmt = "%e %h %Y";
		if($now - $tm < 86400)
			$fmt = "%k:%M";
		else if(date("Y", $tm) == date("Y", $now))
			$fmt = "%e %h";
			
		return strftime($fmt, $tm);
	}
	
	public static function makeThumb(\Intervention\Image\Image $img, $width, $height = null, $bgcolor='#000000')
	{
		if(!isset($height))
			$height = $width;
		
		$w = $img->width();
		$h = $img->height();
		
		if($h > $w)
		{
			$img->resize(null, $height, function ($constraint) { $constraint->aspectRatio(); });
			$w = $img->width();
			if($width > $w)
				$img->resizeCanvas($width, null, 'center', false, $bgcolor);
			else
			{
				$img->resize($width, null, function ($constraint) { $constraint->aspectRatio(); });
				$img->resizeCanvas(null, $height, 'center', false, $bgcolor);
			}
		}
		else
		{
			$img->resize($width, null, function ($constraint) { $constraint->aspectRatio(); });
			$h = $img->height();
			if($height > $h)
				$img->resizeCanvas(null, $height, 'center', false, $bgcolor);
			else
			{
				$img->resize(null, $height, function ($constraint) { $constraint->aspectRatio(); });
				$img->resizeCanvas($width, null, 'center', false, $bgcolor);
			}
		}		
	}
}