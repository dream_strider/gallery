<?php namespace App;

abstract class ResourceType
{
	const Country = 1;
	const Genre = 2;
	const Technic = 3;
	const Device = 4;
}

abstract class Visibility
{
	const All = 0;
	const Registered = 1;
	const Owner = 2;
}

abstract class UserRole
{
	const Author = 1;
	const Collector = 2;
	const Moderator = 3;
	const Webmaster = 4;
}

abstract class Units
{
	const Pix = 0;
	const Cm = 1;
}

abstract class SellConditions
{
	const Exclusive = 0;
	const NonExclusive = 1;
}

abstract class SellType
{
	const Sell = 0;
	const Rent = 1;
}

abstract class AuctionStatus
{
	const NotStarted = 0;
	const Started = 1;
	const Finished = 2;
	const Payed = 3;
}
