@extends('app')

@section('title', 'Невозможно продать работу')

@section('content')

    <div class="shaded-header">
        <h2>Невозможно продать работу</h2>
    </div>

    <div class="content-block">
        <p>{{ $msg }}</p>
    </div>

@stop
