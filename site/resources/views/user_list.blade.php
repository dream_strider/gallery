@extends('app')

@section('title', 'Пользователи: '.$cat)

@section('content')

<div class="shaded-header">
    <h3 style="margin-top: 0;">Пользователи</h3>
</div>
	

<div class="content-block">

	<div class="well">
        <div class="row">
			<div class="col-md-6 col-lg-8">
				&nbsp;
			</div>
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div id="usersearch" class="input-group">
				  <input type="text" class="form-control" placeholder="Поиск по имени/произведению" value="{{ $search }}" />
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button">Искать</button>
				  </span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="preview_list">
		<div class="header">
			<h3>{{ $cat }}</h3>
		</div>
		<hr/>
		<input id='total' type='hidden' value='{{ $count }}' />
		<div class="row showlist">
			@foreach ($list->take(config('gallery.query_page_size'))->get() as $item)
				@include('content.thumb_user', ['item' => $item])
			@endforeach
		</div>
		<div class='centered'>
			<button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="usr_{{ $cid }}" data-ajax-search="{{ $search }}">Показать еще</button>
		</div>
	</div>
</div>

@stop
