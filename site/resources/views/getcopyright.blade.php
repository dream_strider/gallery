@extends('app')

@section('title', 'Проверка подлинности')

@section('content')
    <div class="shaded-header">
        <h3 style="margin-top: 0;">Проверка подлинности</h3>
    </div>

    @if($success)
        <div class="alert alert-success">Изображение проверено</div>
    @endif

    <div class="content-block">
        <div class="row">
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="centered">
                    <img class="file-preview" src="{{ url('imagetocheck/o/'.$msg->id) }}" width="100%" />
                </div>
            </div>
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        @if($is_moderator)
        <div class="row">
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="centered" style="padding: 2em 0 2em 0;">
                    <img class="file-preview" src="{{ url('imagetocheck/c/'.$msg->id) }}" width="100%" />
                </div>
            </div>
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-lg-3 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-8 col-lg-6">
                <div class="centered">
                    <p>Если вы видите изображение с копирайтом под основным изображением, текст которого таков: "Virtual Gallery image by...", это означает, что изображение скачано с сайта галереи и принадлежит автору, имя и почта которого указаны на копирайте.</p>
                </div>
            </div>
            <div class="col-md-2 col-lg-3 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-lg-3 visible-md visible-lg">
                &nbsp;
            </div>
            <form class="noautocomplete" id="buy-form" action="{{ url('checkcopy/'.$msg->id) }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <div class="centered">
                        <label>Комментарий</label>
                        <textarea class="form-control form-control-ofs" style="width: 32em" name="text" placeholder="Введите комментарий, который будет отправлен пользователю, проверяющему это изображение" rows="4">{{ old('text') }}</textarea>

                        <button type="submit" name="submit" class="btn btn-lg btn-success btn-form" value="NO">НЕТ, Копирайт отсутствует</button>
                        <button type="submit" name="submit" class="btn btn-lg btn-danger btn-form" value="YES">ДА, Копирайт присутствует</button>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-lg-3 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        @endif
    </div>

@stop
