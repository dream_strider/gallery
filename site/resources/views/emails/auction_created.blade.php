Вы создали новый лот № {{ $item->id }}. На аукцион выставленна работа "{{ $item->artwork->title }}"<br>
Время начала аукциона: {{ $item->start }}<br>
Время подведения итогов : {{ $item->end }}<br>
<br>
Ссылка на аукцион: <a href="{{ url('auctions?p=focused&rowid=auc&itemid='.$item->id) }}">здесь</a>