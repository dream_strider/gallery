Ваша ставка на аукционе № {{ $item->id }} перекрыта. Пользователь {{ $item->winner->real_name }} поставил {{ $item->final_stake }} руб. Чтобы изменить свою ставку, перейдите по ссылке:<br/>
<br/>
Ссылка на аукцион: <a href="{{ url('auctions?p=focused&rowid=auc&itemid='.$item->id) }}">здесь</a>