Изображение проверено.<br/>
@if($result == 1)
Изображение принадлежит автору Галереи Виртуальных Коллекций.<br/>
@else
Изображение не принадлежит автору Галереи Виртуальных Коллекций.<br/>
@endif
<br/>
Комментарий администратора:<br/>
{{ $comment  }}<br/>
<br/>
Ссылка на изображение: <a style="color: #d38b01; text-decoration: underline;" href="{{ url('checkcopy/'.$mid) }}">здесь</a>