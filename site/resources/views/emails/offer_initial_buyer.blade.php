﻿Вы сделали предложение о покупке работы {{ $item->artwork->title }} у владельца {{$item->artwork->owner->real_name}}.<br/>
Вы предложили за нее {{ $item->price }} руб{{ $item->rent_period ? ' за аренду на срок '.$item->rent_period.' месяцев' : ''}}.<br/>
<br/>
Ссылка на предложение: <a href="{{ url('artwork/offer/'.$item->id) }}">здесь</a>