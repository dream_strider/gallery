Здравствуйте, {{ $real_name }}<br/><br/>
Пожалуйста, активируйте свою учётную запись: <a style="color: #d38b01; text-decoration: underline;" href="{{ url('auth/activate/'.$code) }}">здесь</a><br/>
@if($revealedPassword)
Ваш пароль: {{ $revealedPassword }}
@endif