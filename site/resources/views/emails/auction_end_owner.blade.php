Ваш Аукцион № {{ $item->id }} завершился. На аукцион выставленна работа "{{ $item->artwork->title }}"<br>
@if( isset($item->winner_id))
    Аукцион выиграл пользователь {{ $item->winner->real_name }} с наивысшей ставкой {{ $item->final_stake }} руб.<br>
@else
    Не было сделано ни одной ставки.<br>
@endif
<br>
Ссылка на аукцион: <a href="{{ url('auctions?p=focused&rowid=auc&itemid='.$item->id) }}">здесь</a>