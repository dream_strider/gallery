На Вашем аукционе № {{ $item->id }} сделана новая ставка. Пользователь {{ $item->winner->real_name }} поставил {{ $item->final_stake }} руб.<br/>
<br/>
Ссылка на аукцион: <a href="{{ url('auctions?p=focused&rowid=auc&itemid='.$item->id) }}">здесь</a>