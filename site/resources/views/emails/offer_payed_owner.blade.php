﻿Ваша работа {{ $item->artwork->title }} оплачена. Пользователь {{ $item->buyer->real_name }} поставил его в свою коллекцию.<br/>
<br/>
Ссылка на произведение: <a href="{{ url('user/profile?fu='.$item->buyer->id.'&p=focused&rowid=art&itemid='.$item->artwork->id) }}">здесь</a>