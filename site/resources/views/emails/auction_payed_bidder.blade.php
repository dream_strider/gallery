Вы {{ $item->sell_type == 1 ? 'арендовали' : 'приобрели' }} лот № {{ $item->id }}, {{ $item->artwork->title }}. Чтобы разместить его у себя на экране, откройте произведение и нажмите кнопку "Повесить".<br/>
<br/>
Ссылка на произведение: <a href="{{ url('user/profile?fu='.$item->winner_id.'&p=focused&rowid=art&itemid='.$item->artwork->id) }}">здесь</a>