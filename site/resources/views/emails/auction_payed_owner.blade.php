Ваш лот № {{ $item->id }} оплачен. Пользователь {{ $item->winner->real_name }} поставил его в свою коллекцию.<br/>
<br/>
Ссылка на аукцион: <a href="{{ url('auctions?p=focused&rowid=auc&itemid='.$item->id) }}">здесь</a>