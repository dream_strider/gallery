﻿Вы преобрели работу {{$item->artwork->title}} {{ $item->rent_period ? ' в аренду на срок '.$item->rent_period.' месяцев' : ''}}. Чтобы разместить ее у себя на экране, откройте произведение и нажмите кнопку "Повесить".<br/>
<br/>
Ссылка на произведение: <a href="{{ url('user/profile?fu='.$item->buyer->id.'&p=focused&rowid=art&itemid='.$item->artwork->id) }}">здесь</a>