﻿Пользователь {{$user->real_name}} изменил предложение о покупке работы {{ $item->artwork->title }}.<br/>
Он предложил за нее {{ $item->price }} руб{{ $item->rent_period ? ' за аренду на срок '.$item->rent_period.' месяцев' : ''}}.<br/>
<br/>
Ссылка на предложение: <a href="{{ url('artwork/offer/'.$item->id) }}">здесь</a>