@extends('app')

@section('title', $item->title)

@section('css')
<link rel="stylesheet" media="screen" href="{{ asset('/css/show.css') }}">
@overwrite

@section('js')
<script src="{{ asset('/ext/jquery.fullscreen/jquery.fullscreen-min.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.core.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.utils.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.slider.js') }}"></script>
<script src="{{ asset('/ext/history/jquery.history.js') }}"></script>
<script src="{{ asset('/js/transitions.js') }}"></script>
<script src="{{ asset('/js/show.js') }}"></script>
@overwrite

@section('main')

@if($is_owner)

<div id="background">
    <div id="sshow">

        <!-- Slides Container -->
        <div u="slides" style="position: absolute; left: 0px; top: 0px; width:100%; height:100%; overflow: hidden;">
            <div>
                <div class="slide" style="background: {{ $item->empty_fill }} url({{ $item->originalUrl() }}) no-repeat center center;-webkit-background-size: contain;-moz-background-size: contain;background-size: contain;">
                </div>
            </div>
        </div>

        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;display: none;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px;display: none;">
        </span>
        <!-- Arrow Navigator Skin End -->
    </div>

    <div id="controls" class="pict-fullscreen-bottom">

        <div class="ssshow-contollers fclose">
            <a id="aclose" href="javascript:void(0);" class="glyphicon glyphicon-remove"></a>
        </div>


        <div class="ssshow-contollers fmode">
            <a href="#" id="fmode" class="glyphicon glyphicon-fullscreen"></a>
            <p>Во весь экран</br>
                <em>(Enter)</em>
            </p>
        </div>

    </div>
</div>

@else
<h1 style="text-align:center;color:#fff;">Нельзя просматривать чужую работу!</h1>
@endif



@overwrite