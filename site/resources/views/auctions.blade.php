@extends('app')

@section('title', 'Аукционы')

@section('content')

<div class="shaded-header">
    <h3 style="margin-top: 0;">Аукционы</h3>
</div>

<div class="well">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-4">
			<select id="aucsort" class="selectpicker" data-width="100%" title="Сортировка" value="{{ $ord }}">
				<option value="new" {{ $ord == 'new' ? 'selected' : '' }}>Новые</option>
				<option value="stake" {{ $ord == 'stake' ? 'selected' : '' }}>Наивысшая ставка</option>
				<option value="bids" {{ $ord == 'bids' ? 'selected' : '' }}>Кол-во заявок</option>
			</select>
		</div>
		<div class="visible-lg col-lg-3">&nbsp;</div>
		<div class="col-sm-12 col-md-6 col-lg-5">
			@if($crit != 'all')
			<div id="aucsearch" class="input-group">
				<input type="text" class="form-control" placeholder="Поиск по имени/произведению" value="{{ $search }}" />
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button">Искать</button>
				  </span>
			</div>
			@endif
		</div>
	</div>
</div>

@if(($crit == 'all' || $crit == 'my') && Auth::check() && Auth::user()->role != \App\UserRole::Moderator)
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>C Моим участием</h3>
            @if($crit == 'all') <a href="{{ url('/auctions/my') }}">показать все</a>&nbsp;&nbsp; @endif
        </div>
        <hr/>
        @if($crit == 'my') <input id='total' type='hidden' value='{{ App\Repository::query('auc_my', $search)->count() }}' /> @endif

        @include('content.auc_my', ['crit' => $crit, 'ord' => $ord, 'search' => $search])

        @if($crit == 'my')
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="auc_my" data-ajax-search="{{ $search }}" data-ajax-order="{{ $ord }}">Показать еще</button>
        </div>
        @endif
    </div>
</div>
@endif

@if($crit == 'all' || $crit == 'active')
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Действующие аукционы</h3>
            @if($crit == 'all') <a href="{{ url('/auctions/active') }}">показать все</a>&nbsp;&nbsp; @endif
        </div>
        <hr/>
        @if($crit == 'active') <input id='total' type='hidden' value='{{ App\Repository::query('auc_active', $search)->count() }}' /> @endif

        @include('content.auc_active', ['crit' => $crit, 'ord' => $ord, 'search' => $search])

        @if($crit == 'active')
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="auc_active" data-ajax-search="{{ $search }}" data-ajax-order="{{ $ord }}>Показать еще</button>
        </div>
        @endif
    </div>
</div>
@endif

@if($crit == 'all' || $crit == 'past')
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Результаты торгов</h3>
            @if($crit == 'all') <a href="{{ url('/auctions/past') }}">показать все</a>&nbsp;&nbsp; @endif
        </div>
        <hr/>
        @if($crit == 'past') <input id='total' type='hidden' value='{{ App\Repository::query('auc_past', $search)->count() }}' /> @endif

        @include('content.auc_past', ['crit' => $crit, 'ord' => $ord, 'search' => $search])

        @if($crit == 'past')
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="auc_past" data-ajax-search="{{ $search }}" data-ajax-order="{{ $ord }}>Показать еще</button>
        </div>
        @endif
    </div>
</div>
@endif


@stop
