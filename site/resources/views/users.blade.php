@extends('app')

@section('title', 'Пользователи')

@section('content')


<div class="shaded-header">
    <h3 style="margin-top: 0;">Пользователи</h3>
</div>
	
<div class="content-block">
	<div class="well">	
        <div class="row">
			<div class="col-md-6 col-lg-8">
				&nbsp;
			</div>
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div id="usersearch" class="input-group">
				  <input type="text" class="form-control" placeholder="Поиск по имени/произведению" />
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button">Искать</button>
				  </span>
				</div>
			</div>
		</div>
	</div>
	
        <div class="content-block">
            <div class="preview_list">
                <div class="header">
                    <h3>Новички</h3>
                    <a href="{{ url('/user/list/new') }}">показать всех</a>&nbsp;&nbsp;
                </div>
                <hr/>				
				@include('content.usr_new_block')				
            </div>
        </div>
        <div class="content-block">
            <div class="preview_list">
                <div class="header">
                    <h3>Авторы</h3>
                    <a href="{{ url('/user/list/authors') }}">показать всех</a>&nbsp;&nbsp;
                </div>
                <hr/>
				@include('content.usr_authors_block')
            </div>
        </div>

        <div class="content-block">
            <div class="preview_list">
                <div class="header">
                    <h3>Коллекционеры</h3>
                    <a href="{{ url('/user/list/collectors') }}">показать всех</a>&nbsp;&nbsp;
                </div>
                <hr/>
				@include('content.usr_collects_block')
            </div>
        </div>
</div>
@stop
