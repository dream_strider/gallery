@extends('app')

@section('title', 'Работы')

@section('content')

<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Работы</h3>
        </div>
        <hr/>
        <input id='total' type='hidden' value='{{ App\Repository::query('art')->count() }}' />
        <div class="row showlist">
            @foreach (App\Repository::query('art')->with('author')->with('owner')->orderBy('created_at', 'desc')->take(config('gallery.query_page_size'))->get() as $item)
                @include('content.thumb_artwork', ['item' => $item])
            @endforeach
        </div>
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="art">Показать еще</button>
        </div>
    </div>
</div>

@stop
