@extends('app')

@section('title', 'Ошибка активации')

@section('content')

<div class="content-block">
    <div class="alert alert-danger">
        Ошибка Активации! Возможно, пользователь не зарегистрирован или уже активирован.
    </div>
</div>

@stop
