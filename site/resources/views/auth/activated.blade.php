@extends('app')

@section('title', 'Спасибо за регистрацию')

@section('content')
<div class="shaded-header">
    <h3 style="margin-top: 0;">Спасибо за регистрацию!</h3>
</div>

<div class="content-block">
    Спасибо за регистрацию! Теперь вам доступны все функции сайта
</div>

@stop
