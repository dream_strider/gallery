@extends('app')

@section('content')
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Сброс пароля</h3>
            <br/>
            <hr />

            <div class="row">
                <div class="col-sm-9 col-md-6 col-lg-4" >

                    <p>Если вы забыли свой пароль или хотите получить повторное письмо для активации нового пользователя, введите e-mail, к которому привязан ваш аккаунт</p>
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if (count($errors) > 0 && session('last_action') == 'password/email' )
                    <div class="alert alert-danger">
                        Ошибка!<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="noautocomplete inset " role="form" method="POST" action="{{ url('/password/email') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <label>Адрес электронной почты (e-mail)</label>
                        <input type="email" class="form-control form-control-ofs" placeholder="e-mail" name="email" value="{{ old('email') }}">

                        <input type="submit" class="btn btn-lg btn-info btn-form" value="Выслать ссылку" />

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop