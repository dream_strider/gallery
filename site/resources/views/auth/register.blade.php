@extends('app')

@section('content')
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Регистрация нового пользователя</h3>
            <br/>
            <hr />

            <div class="row">
                <div class="col-sm-9 col-md-6 col-lg-4" >

                    @if (count($errors) > 0 && session('last_action') == 'auth/register' )
						<div class="alert alert-danger">
							Ошибка регистрации!<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="noautocomplete inset " role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<label>Имя пользователя (логин)</label>
						<input type="text" class="form-control form-control-ofs" placeholder="Логин" name="name" value="{{ old('name') }}">

                        <label>Адрес электронной почты (e-mail)</label>
                        <input type="text" class="form-control form-control-ofs" placeholder="e-mail" name="email" value="{{ old('email') }}">
						
                        <label>Реальное Имя и Фамилия</label>
                        <input type="text" class="form-control form-control-ofs" placeholder="Имя и Фамилия" name="real_name" value="{{ old('real_name') }}">

                        <label>Тип регистрации</label>
                        <select class="selectpicker" data-width="100%" title="Тип регистрации" name="role" value="{{ old('role') }}">
                            <option value="" {{ empty(old('role')) ? 'selected' : '' }}>Не задан</option>
                            <option value="1" {{ old('role') == 1 ? 'selected' : '' }}>Автор</option>
                            <option value="2" {{ old('role') == 2 ? 'selected' : '' }}>Коллекционер</option>
                        </select>

						<label>Пол</label>
						<select class="selectpicker" data-width="100%" title="Пол" name="gender" value="{{ old('gender') }}">
                            <option value="" {{ empty(old('gender')) ? 'selected' : '' }}>Не задан</option>
							<option value="1" {{ old('gender') == 1 ? 'selected' : '' }}>Мужской</option>
							<option value="2" {{ old('gender') == 2 ? 'selected' : '' }}>Женский</option>
						</select>
						
						<label>Дата рождения</label>
						<input type="text" class="form-control datepicker" value="{{ old('birthday') }}" data-date-format="yyyy-mm-dd" name="birthday">
						
						<label>Страна</label>
						<select class="selectpicker" data-container="body" data-width="100%" title="Страна" name="country" value="{{ old('country') }}">
                            <option value="" {{ empty(old('country')) ? 'selected' : '' }}>Не задана</option>
							@foreach(\App\LangResource::get(\App\ResourceType::Country) as $key => $value)
								<option value="{{ $key }}" {{ $key == old('country') ? 'selected' : '' }}>{{ $value }}</option>
							@endforeach
						</select>
						
						<label>Город</label>
						<input type="text" class="form-control form-control-ofs" placeholder="Название города" name="city" value="{{ old('city') }}">
						
						<label>Адрес</label>
						<textarea class="form-control" placeholder="Улица, дом, квартира" rows="9" name="address">{{ old('address') }}</textarea>						 
						
                        <label>Пароль</label>
                        <input type="password" class="form-control form-control-ofs" placeholder="пароль" name="password">

                        <label>Подтвердите Пароль</label>
                        <input type="password" class="form-control form-control-ofs" placeholder="подтвердите пароль" name="password_confirmation" >

                        {!! Recaptcha::render(array('lang' => App::getLocale())); !!}

                        <input type="submit" class="btn btn-lg btn-info btn-form" value="Зарегистрироваться" />

					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
