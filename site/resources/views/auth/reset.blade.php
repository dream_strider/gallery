@extends('app')

@section('content')
<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Сброс пароля</h3>
            <br/>
            <hr />

            <div class="row">
                <div class="col-sm-9 col-md-6 col-lg-4" >
                    @if (count($errors) > 0 && session('last_action') == 'password/reset' )
                    <div class="alert alert-danger">
                        Ошибка!<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="noautocomplete inset " role="form" method="POST" action="{{ url('/password/reset') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">

                        <label>Адрес электронной почты (e-mail)</label>
                        <input type="email" class="form-control form-control-ofs" placeholder="e-mail" name="email" value="{{ old('email') }}">

                        <label>Новый Пароль</label>
                        <input type="password" class="form-control form-control-ofs" placeholder="пароль" name="password">
                        <label>Подтвердите Пароль</label>
                        <input type="password" class="form-control form-control-ofs" placeholder="подтвердите пароль" name="password_confirmation">

                        <input type="submit" class="btn btn-lg btn-info btn-form" value="Сменить пароль" />

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop