@extends('app')

@section('title', 'Пароль изменён')

@section('content')
<div class="shaded-header">
    <h3 style="margin-top: 0;">Пароль изменён</h3>
</div>

<div class="content-block">
    Пароль успешно изменён
</div>

@stop
