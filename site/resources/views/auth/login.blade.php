@extends('app')

@section('title', 'Вход в систему')

@section('content')


<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Вход в систему</h3>
            <br/>
            <hr />

            <div class="row">
                <div class="col-sm-9 col-md-6 col-lg-4" >

                    @if (count($errors) > 0 && session('last_action') == 'auth/login' )
                    <div class="alert alert-danger alert-login">
                        Ошибка входа<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="loginForm" action="{{ url('/auth/login') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" placeholder="логин" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="пароль" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <input class="loginRememberme" type="checkbox" name="remember" />
                            <label for="remember">Запомнить меня</label>
                            <button type="submit" class="btn btn-success pull-right" name="Login" value="Вход">Вход</button>
                            <br/><a href="{{ url('/auth/register') }}">Зарегистрироваться</a>
                            <br/><a href="{{ url('/password/email') }}">Забыли пароль?</a>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
