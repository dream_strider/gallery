@extends('app')

@section('title', 'Результаты поиска')

@section('content')

<div class="shaded-header">
    <h3 style="margin-top: 0;">Результаты поиска работ</h3>
</div>

<div class="content-block">
    <div class="preview_list">

        <input id='total' type='hidden' value='{{ App\Repository::query('art', $search)->count() }}' />
        <div class="row showlist">
            @foreach (App\Repository::query('art', $search)->with('author')->with('owner')->orderBy('created_at', 'desc')->take(config('gallery.query_page_size'))->get() as $item)
                @include('content.thumb_artwork', ['item' => $item])
            @endforeach
        </div>
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="art" data-ajax-search="{{ $search }}">Показать еще</button>
        </div>
    </div>
</div>
@stop
