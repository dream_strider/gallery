@extends('app')

@section('title', 'Добавить Работу')

@section('css')
@parent
    <link rel="stylesheet" media="screen" href="{{ asset('/ext/enyo.dropzone/dropzone.min.css') }}" />
@stop

@section('js')
@parent
<script type="text/javascript" src="{{ asset('/ext/enyo.dropzone/dropzone.min.js') }}"></script>
@stop


@section('content')

<div class="form-block">
    <h2>Добавить Работу</h2>
    <div class="form-area">
        <div class="row">
            <div class="col-md-12 col-lg-8 col-lg-offset-2">
                <div class="centered inset">

                    <img class="file-preview" src="" style="display: none;" />

                    <form id="dzform" class="noautocomplete dropzone dz-clickable" style="min-height: 300px; min-width: 400px; border-radius: 7px;" method="POST" action="{{ url('/artwork/upload') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="fallback">
							<input type="file" name="file" />
							<div>
								Выберите файл произведения на компьютере нажав кнопку "Выбрать файл". Файл должен быть формата JPG или PNG и максимального размера 20 МБ.
							</div>
							<button type="button" class="btn btn-lg btn-info btn-form btn-file">Выбрать файл</button>
						</div>
                        <div class="dz-message">Перетащите файл сюда, или кликните для выбора файла.

                        </div>
                    </form>

                    <div class="upload-error alert alert-danger" style="display: none;">
                        Ошибка при загрузке файла. Пожалуйста, проверьте что файл соответствует формату JPG или PNG и его размер меньше 20 МБ.
                    </div>
                    <div class="upload-success alert alert-success" style="display: none;">
                        Файл успешно загружен
                    </div>

                </div>

                <div class="fileprogress" style="width: 100%; display: none;">
                    <h3 style="text-align: center">Загружается файл произведения...</h3>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only progress-text">0% Complete</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <a type="button" href="{{ url('artwork/edit') }}" class="btn btn-form btn-lg btn-success disabled">ПРОДОЛЖИТЬ</a>
    <a type="button" href="{{ url('/') }}" class="btn btn-form btn-lg btn-default">ОТМЕНА</a>
</div>

@stop


