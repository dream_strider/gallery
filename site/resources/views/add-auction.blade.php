@extends('app')

@section('title', 'Начать аукцион')

@section('content')

<div class="form-block">

    <h2>Начать Аукцион</h2>

    @if (count($errors) > 0 && session('last_action') == 'auction/create' )
    <div class="alert alert-danger">
        Возникли ошибки при создании аукциона:
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="noautocomplete" id="auc-create-form" action="{{ url("auction/create") }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="art" value="{{ $item->id }}">

        <div style="padding: 20px">

            <div class="row">
                <div class="col-md-8">
                    <div class="centered">
                        <img class="file-preview" src="{{ $item->thumbUrl('large') }}" width="100%" />
                    </div>
                </div>
                <div class="col-md-4 inset" style="padding-top:0">
                    <div class="fdataitem-props" style="min-height: 300px">
                        <h2>{{ $item->title }}</h2>
                        <div class="intro">{{ $item->description }}</div>
                        <br/>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Автор:</div>@if($item->author)<div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->author->id) }}">{{ $item->author->real_name }}</a></div>@else Удален @endif</div>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Владелец:</div><div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></div></div>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">В Коллекции:</div><div class="col-sm-6 col-md-7">{{ $item->collection->title }}</div></div>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Жанр:</div><div class="col-sm-6 col-md-7">{{ $item->genreText }}</div></div>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Техника исполнения:</div><div class="col-sm-6 col-md-7">{{ $item->technicText }}</div></div>
                        <div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Размер:</div><div class="col-sm-6 col-md-7">{{ $item->width }}x{{ $item->height }}</div></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-3"><h3>Тип</h3>
                    <div class="greenbox">
                        <label class="onoff selltypeswitch">Продажа<input type="checkbox" class="ios-switch" name="sell_type" off_value="0" value="1" {{ old('sell_type') == 1 ? 'checked' : '' }}><div></div>Аренда</label>
                        <div class="rentperiod">
                            <label>Период аренды (дней)</label>
                            <input type="number" class="form-control" name="rent_period" value="{{ old('rent_period') }}" min="1">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3"><h3>Эксклюзивность</h3>
                    <div class="greenbox">
                        <label class="onoff selltypeswitch">Повтор: Нет<input type="checkbox" class="ios-switch" name="repeat_possible" off_value="0" value="1" {{ old('repeat_possible') == 1 ? 'checked' : '' }}><div></div>Да</label>
                        <br/>
                        <label>Тип аукциона</label>
                        <div class="auctypeselect">
                            <select class="selectpicker" data-width="100%" title="Тип аукциона" name="sell_conditions">
                                <option value="0" {{ old('sell_conditions') == 0 ? 'selected' : '' }}>Эксклюзивный</option>
                                <option value="1" {{ old('sell_conditions') == 1 ? 'selected' : '' }}>Неэксклюзивный</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3"><h3>Цена</h3>
                    <div class="greenbox">
                        <label>Стартовая (руб.)</label>
                        <input type="number" class="form-control" name="initial_stake" value="{{ old('initial_stake') }}" min="1">
                        <label>Шаг аукциона (руб.)</label>
                        <input type="number" class="form-control" name="stake_step" value="{{ old('stake_step') }}" min="1">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3"><h3>Время</h3>
                    <div class="greenbox">
                        <label>Начало торгов</label>
                        <div class="clearfix"></div>
                        <div class="pull-left" style="width: 50%; padding-right: 10px;">
                            <input type="text" class="form-control datepicker" value="{{ old('auction_start_date', date('Y-m-d') ) }}" data-date-format="yyyy-mm-dd" name="auction_start_date">
                        </div>
                        <div class="pull-left" style="width: 50%; padding-left: 10px;">
                            <select class="selectpicker" data-width="100%" name="auction_start_time">
                                <option value="00:00" {{ old('auction_start_time') == "00:00" ? 'selected' : '' }}>00:00</option>
                                <option value="03:00" {{ old('auction_start_time') == "03:00" ? 'selected' : '' }}>03:00</option>
                                <option value="06:00" {{ old('auction_start_time') == "06:00" ? 'selected' : '' }}>06:00</option>
                                <option value="09:00" {{ old('auction_start_time') == "09:00" ? 'selected' : '' }}>09:00</option>
                                <option value="12:00" {{ old('auction_start_time') == "12:00" ? 'selected' : '' }}>12:00</option>
                                <option value="15:00" {{ old('auction_start_time') == "15:00" ? 'selected' : '' }}>15:00</option>
                                <option value="18:00" {{ old('auction_start_time') == "18:00" ? 'selected' : '' }}>18:00</option>
                                <option value="21:00" {{ old('auction_start_time') == "21:00" ? 'selected' : '' }}>21:00</option>
                                <option value="23:00" {{ old('auction_start_time') == "23:00" ? 'selected' : '' }}>23:00</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <label>Подведение итогов</label>
                        <div class="clearfix"></div>
                        <div class="pull-left" style="width: 50%; padding-right: 10px;">
                            <input type="text" class="form-control datepicker" value="{{ old('auction_end_date', date('Y-m-d')) }}" data-date-format="yyyy-mm-dd" name="auction_end_date">
                        </div>
                        <div class="pull-left"  style="width: 50%; padding-left: 10px;">
                            <select class="selectpicker" data-width="100%" name="auction_end_time">
                                <option value="00:00" {{ old('auction_end_time') == "00:00" ? 'selected' : '' }}>00:00</option>
                                <option value="03:00" {{ old('auction_end_time') == "03:00" ? 'selected' : '' }}>03:00</option>
                                <option value="06:00" {{ old('auction_end_time') == "06:00" ? 'selected' : '' }}>06:00</option>
                                <option value="09:00" {{ old('auction_end_time') == "09:00" ? 'selected' : '' }}>09:00</option>
                                <option value="12:00" {{ old('auction_end_time') == "12:00" ? 'selected' : '' }}>12:00</option>
                                <option value="15:00" {{ old('auction_end_time') == "15:00" ? 'selected' : '' }}>15:00</option>
                                <option value="18:00" {{ old('auction_end_time') == "18:00" ? 'selected' : '' }}>18:00</option>
                                <option value="21:00" {{ old('auction_end_time') == "21:00" ? 'selected' : '' }}>21:00</option>
                                <option value="23:00" {{ old('auction_end_time') == "23:00" ? 'selected' : '' }}>23:00</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="violetbox">
                        <h4 style="float: left;">Дополнительная информация</h4><button type="button" data-form="auc-create-form" class="btn btn-form btn-success pull-right">Получить лот</button>
                        <textarea class="form-control comment-entry" placeholder="Введите описание" rows="8" name="description">{{ old('description') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
@stop