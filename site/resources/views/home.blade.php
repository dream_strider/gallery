@extends('app')

@section('title', 'Главная')

@section('content')
<div class="shaded-header">
    <h3 style="margin-top: 0;">Последние материалы</h3>
</div>

@if(Auth::check() && Auth::user()->greeting && Auth::user()->role != \App\UserRole::Moderator)
    @include('content.greeting')
@endif

<div class="content-block">
	@include('content.art_preview_block')
</div>

<div class="content-block">
	@include('content.col_preview_block')
</div>

<div class="content-block">
	@include('content.auc_preview_block')
</div>

@stop
