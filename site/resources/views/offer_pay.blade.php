@extends('app')

@section('title', 'Оплата аукциона')

@section('content')

<div class="content-block">
    <p>Здесь будет интеграция с платёжной системой</p>
    <p>Оплатите {{ $item->price }} руб.</p>
    <a type="button" href="{{ url('artwork/offer/'.$item->id.'/dopay') }}" class="btn btn-success">Подтвердить оплату</a>
</div>

@stop