@extends('app')

@section('title', 'Коллекции')

@section('content')

<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>Коллекции</h3>
        </div>
        <hr/>
        <input id='total' type='hidden' value='{{ App\Repository::query('col')->count() }}' />
        <div class="row showlist">
            @foreach (App\Repository::query('col')->with('owner')->with('artworkCountRelation')->orderBy('created_at', 'desc')->take(config('gallery.query_page_size'))->get() as $item)
                @include('content.thumb_collection', ['item' => $item])
            @endforeach
        </div>
        <div class='centered'>
            <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="col">Показать еще</button>
        </div>
    </div>
</div>

@stop
