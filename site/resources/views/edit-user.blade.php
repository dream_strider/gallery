@extends($extends)

@section('title', 'Редактировать Профиль')

@section('content')

<div id="art-edit" class="form-block">
    <h2>Редактирование Профиля</h2>

    @if($success)
    <div class="alert alert-success">Профиль успешно сохранен</div>
    @endif

    @if (count($errors) > 0 && session('last_action') == 'user/save' )
    <div class="alert alert-danger">
        Возникли ошибки при сохранении профиля:
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="form-area">
        <div class="row">
            <div class="col-md-12 col-lg-3 inset form-preview">

                <div class="centered">
                    <img style="max-width: 250px;" src="{{ $user->thumbUrl('mid') }}" width="100%" />
                    <h3>{{ $user->real_name }}</h3>
                    <h4><i>{{ $user->roleName }}</i></h4>
                </div>

            </div>
            <div class="col-md-12 col-lg-9 form-content">
                <div class="col-md-5 form-menu">

                    <ul role="form-menu">
                        <li data-option="prof-edit-acc" {{ $opt == 'prof-edit-acc' || $opt == '' ? 'class=active' : '' }}>Учётная запись</li>
                        <li data-option="prof-edit-psw" {{ $opt == 'prof-edit-psw' ? 'class=active' : '' }}>Пароль</li>
                        <li data-option="prof-edit-photo" {{ $opt == 'prof-edit-photo' ? 'class=active' : '' }}>Фото</li>
                        <li data-option="prof-edit-pers" {{ $opt == 'prof-edit-pers' ? 'class=active' : '' }}>Персональные данные</li>
                        @if($user->role != \App\UserRole::Moderator)
                        <li data-option="prof-edit-pay" {{ $opt == 'prof-edit-pay' ? 'class=active' : '' }}>Методы оплаты</li>
                        @endif
                        <li data-option="prof-edit-place" {{ $opt == 'prof-edit-place' ? 'class=active' : '' }}>Места</li>
                        @if($user->role != \App\UserRole::Moderator)
                        <li data-option="prof-edit-dev" {{ $opt == 'prof-edit-dev' ? 'class=active' : '' }}>Устройства</li>
                        @endif
                        @if($user->role == \App\UserRole::Author)
                        <li data-option="prof-edit-exp" {{ $opt == 'prof-edit-exp' ? 'class=active' : '' }}>Выставки<span></span></li>
                        @endif
                        @if($isModerator)
                            <li data-option="prof-edit-del" {{ $opt == 'prof-edit-del' ? 'class=active' : '' }}>Удаление пользователя</li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-7 form-options">
                     <form class="noautocomplete" id="prof-edit-form" action="{{ url('user/save') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <input type="hidden" name="opt" value="{{ $opt }}">
                        <input type="hidden" name="photocrop">

                        <section id="prof-edit-acc" class="form-screen inset form-options-1" {{ $opt != 'prof-edit-acc' && $opt != '' ? 'style=display:none;' : '' }}>
                             <label>Имя пользователя (логин)</label>
                             <input type="text" disabled class="form-control form-control-ofs" value="{{ old('name', $user->name) }}" >
                             <label>Реальное Имя и Фамилия</label>
                             <input type="text" class="form-control form-control-ofs" placeholder="Имя и Фамилия" name="real_name" value="{{ old('real_name', $user->real_name) }}">
                             <label>Адрес электронной почты (e-mail)</label>
                             <input type="text" class="form-control form-control-ofs" placeholder="e-mail" name="email" value="{{ old('email', $user->email) }}">
                        </section>

                        <section id="prof-edit-psw" class="form-screen inset form-options-2" {{ $opt != 'prof-edit-psw'? 'style=display:none;' : '' }}>
                             <label>Текущий пароль</label>
                             <input type="password" class="form-control form-control-ofs" placeholder="текущий пароль" name="password_old" value="">
                             <label>Новый пароль</label>
                             <input type="password" class="form-control form-control-ofs" placeholder="новый пароль" name="password" value="">
                             <label>Подтвердите пароль</label>
                             <input type="password" class="form-control form-control-ofs" placeholder="подтвердите пароль" name="password_confirmation" value="" >
                        </section>

                        <section id="prof-edit-photo" class="form-screen form-options-3" {{ $opt != 'prof-edit-photo'? 'style=display:none;' : '' }}>
                             <div class="centered inset">
                                 <div>
                                     Выберите фото для профиля на компьютере нажав кнопку "Загрузить фото". Файл должен быть формата JPG или PNG и максимального размера 20 МБ.
                                 </div>
                                 <button type="button" class="btn btn-lg btn-info btn-form btn-file">ЗАГРУЗИТЬ ФОТО</button>

                                 <div class="upload-error alert alert-danger" style="display: none;">
                                     Ошибка при загрузке файла. Пожалуйста, проверьте что файл соответствует формату JPG или PNG и его размер меньше 20 МБ.
                                 </div>
                                 <div class="upload-success alert alert-success" style="display: none;">
                                     Файл успешно загружен
                                 </div>

                                 <div class="fileprogress" style="width: 100%; display: none;">
                                     <h3 style="text-align: center">Загружается фото профиля...</h3>
                                     <div class="progress">
                                         <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                             <span class="sr-only progress-text">0% Complete</span>
                                         </div>
                                     </div>
                                 </div>

                                 <img class="file-preview crop" src="" style="display: none;"/>

                                 <button id="cancel_crop" type="button" class="btn btn-lg btn-form" style="display:none;">ОТМЕНИТЬ</button>
                             </div>
                        </section>
						
						<section id="prof-edit-pers" class="form-screen form-options-4 inset" {{ $opt != 'prof-edit-pers'? 'style=display:none;' : '' }}>       
							<label>Пол</label>
							<select class="selectpicker" data-width="100%" title="Пол" name="gender" value="{{ old('gender', $user->gender) }}">
								<option value="1" {{ old('gender', $user->gender) == 1 ? 'selected' : '' }}>Мужской</option>
								<option value="2" {{ old('gender', $user->gender) == 2 ? 'selected' : '' }}>Женский</option>
							</select>
							<label>Дата рождения</label>
							<input type="text" class="form-control datepicker" value="{{ old('birthday', $user->birthday) }}" data-date-format="yyyy-mm-dd" name="birthday">
							<label>Веб-сайт</label>
							<input type="text" class="form-control form-control-ofs" placeholder="Адрес веб-сайта" name="web" value="{{ old('web', $user->web) }}">
                            @if($user->role != \App\UserRole::Moderator)
                                <label>Информация о творчестве</label>
							    <textarea class="form-control" placeholder="Описание творчества в свободной форме" rows="8" name="description">{{ old('description', $user->description) }}</textarea>
                            @endif
						</section>

                         <section id="prof-edit-pay" class="form-screen form-options-2 inset" {{ $opt != 'prof-edit-pay'? 'style=display:none;' : '' }}>
                             <h3>Выберите метод оплаты</h3>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="bank" {{ old('pay_method', $user->pay_method) == 'bank' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/bank.png') no-repeat 3px 3px;"></div>
                                 <span>Банковский перевод</span>
                             </div>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="visa" {{ old('pay_method', $user->pay_method) == 'visa' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/visa.png') no-repeat 3px 3px;"></div>
                                 <span>Карта Виза</span>
                             </div>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="master" {{ old('pay_method', $user->pay_method) == 'master' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/euromaster.png') no-repeat 3px 3px;"></div>
                                 <span>Мастер Кард</span>
                             </div>

                             <div class="clearfix"></div>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="paypal" {{ old('pay_method', $user->pay_method) == 'paypal' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/paypal.png') no-repeat 3px 3px;"></div>
                                 <span>PayPal</span>
                             </div>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="webmoney" {{ old('pay_method', $user->pay_method) == 'webmoney' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/webmoney.png') no-repeat 3px 3px;"></div>
                                 <span>Web Money</span>
                             </div>

                             <div class="payment_item">
                                 <input type="radio" name="pay_method" value="yandex" {{ old('pay_method', $user->pay_method) == 'yandex' ? 'checked="checked"' : '' }} />
                                 <div class="payment_img" style="background: url('img/yandex.png') no-repeat 3px 3px;"></div>
                                 <span>Яндекс.Деньги</span>
                             </div>

                             <div class="clearfix"></div>

                         </section>

						<section id="prof-edit-place" class="form-screen inset form-options-1" {{ $opt != 'prof-edit-place'? 'style=display:none;' : '' }}>
							<label>Страна</label>
							<select class="selectpicker" data-width="100%" title="Страна" name="country" value="{{ old('country', $user->country) }}">
								@foreach(\App\LangResource::get(\App\ResourceType::Country) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('country', $user->country) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
							<label>Город</label>
							<input type="text" class="form-control form-control-ofs" placeholder="Название города" name="city" value="{{ old('city', $user->city) }}">
							<label>Адрес</label>
							<textarea class="form-control" placeholder="Улица, дом, квартира" rows="9" name="address">{{ old('address', $user->address) }}</textarea>						 
						</section>
						
						<section id="prof-edit-dev" class="form-screen inset form-options-2" {{ $opt != 'prof-edit-dev'? 'style=display:none;' : '' }}>
							<label>Устройство 1</label>
							<select class="selectpicker" data-width="100%" title="" name="dev1" value="{{ old('dev1', $user->dev1) }}">
								<option value="">&nbsp;</option>
								@foreach(\App\LangResource::get(\App\ResourceType::Device) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('dev1', $user->dev1) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
							<label>Устройство 2</label>
							<select class="selectpicker" data-width="100%" title="" name="dev2" value="{{ old('dev2', $user->dev2) }}">
								<option value="">&nbsp;</option>
								@foreach(\App\LangResource::get(\App\ResourceType::Device) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('dev2', $user->dev2) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
							<label>Устройство 3</label>
							<select class="selectpicker" data-width="100%" title="" name="dev3" value="{{ old('dev3', $user->dev3) }}">
								<option value="">&nbsp;</option>
								@foreach(\App\LangResource::get(\App\ResourceType::Device) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('dev3', $user->dev3) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
							<label>Устройство 4</label>
							<select class="selectpicker" data-width="100%" title="" name="dev4" value="{{ old('dev4', $user->dev1) }}">
								<option value="">&nbsp;</option>
								@foreach(\App\LangResource::get(\App\ResourceType::Device) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('dev4', $user->dev4) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
							<label>Устройство 5</label>
							<select class="selectpicker" data-width="100%" title="" name="dev5" value="{{ old('dev5', $user->dev5) }}">
								<option value="">&nbsp;</option>
								@foreach(\App\LangResource::get(\App\ResourceType::Device) as $key => $value)
									<option value="{{ $key }}" {{ $key == old('dev5', $user->dev5) ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
						</section>
						
                        @if($isAuthor)
						<section id="prof-edit-exp" class="form-screen inset form-options-3" {{ $opt != 'prof-edit-exp'? 'style=display:none;' : '' }}>
							<label>Выставка 1</label>
							<input type="text" class="form-control form-control-ofs form-control-spoiler" placeholder="" name="exname1" value="{{ old('exname1', $user->exname1) }}">
							<label>Выставка 2</label>
							<input type="text" class="form-control form-control-ofs form-control-spoiler" placeholder="" name="exname2" value="{{ old('exname2', $user->exname2) }}">
							<label>Выставка 3</label>
							<input type="text" class="form-control form-control-ofs form-control-spoiler" placeholder="" name="exname3" value="{{ old('exname3', $user->exname3) }}">
							<label>Выставка 4</label>
							<input type="text" class="form-control form-control-ofs form-control-spoiler" placeholder="" name="exname4" value="{{ old('exname4', $user->exname4) }}">
							<label>Выставка 5</label>
							<input type="text" class="form-control form-control-ofs form-control-spoiler" placeholder="" name="exname5" value="{{ old('exname5', $user->exname5) }}">
						</section>
						@endif

                         @if($isModerator)
                             <section id="prof-edit-del" class="form-screen inset form-options-1" {{ $opt != 'prof-edit-del' ? 'style=display:none;' : '' }}>
                                 <button type="button" class="btn btn-form btn-lg btn-danger" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить пользователя?" data-action="{{ url('user/delete/'.$user->id) }}">УДАЛИТЬ</button>
                             </section>
                         @endif

                     </form>
                    <form class="noautocomplete" method="post" action="user/upload" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <input type="file" name="file" />
                    </form>

                </div>
            </div>
        </div>
    </div>
    <button type="button" data-form="prof-edit-form" class="btn btn-form btn-lg btn-success">ГОТОВО</button>
    <a type="button" href="{{ url('/') }}" class="btn btn-form btn-lg btn-default">ОТМЕНА</a>
</div>

@stop