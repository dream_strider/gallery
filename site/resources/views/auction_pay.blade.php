@extends('app')

@section('title', 'Оплата аукциона')

@section('content')

<div class="content-block">
    <p>Здесь будет интеграция с платёжной системой</p>
    <p>Оплатите {{ $item->final_stake }} руб.</p>
    <a type="button" href="{{ url('auction/dopay/'.$item->id) }}" class="btn btn-success">Подтвердить оплату</a>
</div>

@stop