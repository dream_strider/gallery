@extends('app')

@section('title', 'Редактировать Работу')

@section('content')

<div id="art-edit" class="form-block">
    <h2>Редактирование Работы</h2>

    @if($success)
    <div class="alert alert-success">Работа успешно сохранена</div>
    @endif

    @if (count($errors) > 0 && session('last_action') == 'artwork/save' )
    <div class="alert alert-danger">
        Возникли ошибки при сохранении работы:
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="form-area">
        <div class="row">
            <div class="col-md-12 col-lg-3 inset form-preview">

                <div class="centered">
                    <a href="#" data-rowid="art" data-itemid="{{ $item->id }}" class="select_thumb">
                        <img class="file-preview" src="{{ url( $isNew ? $item->uploadThumbUrl() : $item->thumbUrl('mid')) }}" width="100%" />
                    </a>
                    <h3>{{ $isNew ? 'Новая работа' : $item->title }}</h3>
                    <p class="text">{{ str_limit($item->description, 80, '...') }}</p>
                </div>

            </div>
            <div class="col-md-12 col-lg-9 form-content">
                <div class="col-md-5 form-menu">

                    <ul role="form-menu">
                        @if($isAuthor || $isModerator)
                            <li data-option="art-edit-desc" {{ $opt == 'art-edit-desc' || $opt == '' ? 'class=active' : '' }}>Название и Описание <span></span></li>
                        @endif
                        <li data-option="art-edit-col" {{ $opt == 'art-edit-col' || ($opt == '' && !$isAuthor && !$isModerator) ? 'class=active' : '' }}>Коллекция</li>
                        @if($isAuthor || $isModerator)
                            <li data-option="art-edit-cat" {{ $opt == 'art-edit-cat' ? 'class=active' : '' }}>Жанр</li>
                            <li data-option="art-edit-tech" {{ $opt == 'art-edit-tech' ? 'class=active' : '' }}>Техника Исполнения</li>
                            <li data-option="art-edit-base" {{ $opt == 'art-edit-base' ? 'class=active' : '' }}>Размер и Дата Создания</li>
                        @endif
                        @if($canSell || $isModerator)
                            <li data-option="art-edit-auc" {{ $opt == 'art-edit-auc' ? 'class=active' : '' }}>Опции Продажи</li>
                        @endif
                        <li data-option="art-edit-soc" {{ $opt == 'art-edit-soc' ? 'class=active' : '' }}>Социальность</li>
                        @if(!$isNew)
                        <li data-option="art-edit-del" {{ $opt == 'art-edit-del' ? 'class=active' : '' }}>Удаление работы</li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-7 form-options">
                    <form class="noautocomplete" id="art-edit-form" action="{{ url('artwork/save') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <input type="hidden" name="opt" value="{{ $opt }}">

                        @if($isAuthor || $isModerator)
                        <section id="art-edit-desc" class="form-screen inset form-options-1" {{ $opt != 'art-edit-desc' && $opt != '' ? 'style=display:none;' : '' }}>
                            <input type="text" class="form-control" placeholder="Введите название работы" name="title" value="{{ old('title', $item->title) }}" >
                            <hr/>
                            <textarea class="form-control" placeholder="Введите описание" rows="12" name="description">{{ old('description', $item->description) }}</textarea>
                        </section>
                        @endif
                        <section id="art-edit-col" class="form-screen form-menu scrollable form-options-2" {{ !($opt == 'art-edit-col' || ($opt == '' && !$isAuthor && !$isModerator)) ? 'style=display:none;' : '' }}>
                            <input type="text" id="collection_id" name="collection_id" style="display:none;" value="{{ old('collection_id', $item->collection_id) }}">
                            <ul role="single-select" data-select="collection_id">
                                @foreach (App\Repository::query('col')->where('created_by', $item->owner_id)->orderBy('default', 'desc')->orderBy('title', 'asc')->get() as $i)
                                    <li data-option="{{ $i->id }}" style="padding-left: 20px;padding-top:7px; padding-bottom:7px;">
                                        <img src="{{ url($i->thumbUrl('small'))}}" width="50px" height="50px" />&nbsp;{{ $i->title }}
                                    </li>
                                @endforeach
                            </ul>
                        </section>

                        @if($isAuthor || $isModerator)
                        <section id="art-edit-cat" class="form-screen form-menu scrollable form-options-3" {{ $opt != 'art-edit-cat' ? 'style=display:none;' : '' }}>
                            <input type="text" id="genre" name="genre" style="display:none;" value="{{ old('genre', $item->genre) }}">
                            <ul role="single-select" data-select="genre">
                                <li data-option="">Не задан</li>
                                @foreach (App\LangResource::get(App\ResourceType::Genre) as $key=>$value)
                                    <li data-option="{{ $key }}">{{ $value }}</li>
                                @endforeach
                            </ul>
                        </section>
                        <section id="art-edit-tech" class="form-screen form-menu scrollable form-options-4" {{ $opt != 'art-edit-tech' ? 'style=display:none;' : '' }}>
                            <input type="text" id="art_technic" name="art_technic" style="display:none;" value="{{ old('art_technic', $item->art_technic) }}">
                            <ul role="single-select" data-select="art_technic">
                                <li data-option="">Не задана</li>
                                @foreach (App\LangResource::get(App\ResourceType::Technic) as $key=>$value)
                                    <li data-option="{{ $key }}">{{ $value }}</li>
                                @endforeach
                            </ul>
                        </section>
                        <section id="art-edit-base" class="form-screen inset form-options-1" {{ $opt != 'art-edit-base' ? 'style=display:none;' : '' }}>
                            <h3>Физический размер произведения</h3>
                            <span>По умолчанию указаны данные загруженного файла. Реальный размер нужно указать в случае если он отличается от загруженного.</span>
                            <br/><br/>
                            <label>Единицы Измерения</label>
                            <label class="onoff pull-right">px<input type="checkbox" class="ios-switch" name="units" off_value="0" value="1" {{ old('units', $item->units) == 1 ? 'checked' : '' }} ] ><div></div>см</label>
                            <br/><br/>
                            <div class="col-md-6">
                                <label>Ширина</label>
                                <input type="text" class="form-control" placeholder="Ширина" name="width" value="{{ old('width', $item->width) }}" >
                            </div>
                            <div class="col-md-6">
                                <label>Высота</label>
                                <input type="text" class="form-control" placeholder="Высота" name="height" value="{{ old('height', $item->height) }}" >
                            </div>
                            <br/><br/><br/>
                            <h3>Год Создания</h3>
                            <span>Возраст произведения может существенно увеличить его стоимость. Пожалуйста укажите верную дату.</span>
                            <br/><br/>

                            <select class="selectpicker" data-width="100%" title="Год создания" name="release_date" value="{{ old('release_date', $item->release_date) }}">
                                <option value="" {{ old('release_date', $item->release_date) == null ? 'selected' : '' }}>Не Задан</option>
                                @for($i=(int)date("Y"); $i>=100; $i--)
                                    <option value="{{ $i }}" {{ old('release_date', $item->release_date) == $i ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor
                            </select>

                            <h3>Цвет заполнения</h3>
                            <br/>
							<input type="text" value="{{ old('empty_fill', $item->empty_fill) }}" class="form-control colorpick" name="empty_fill"/>
							
                        </section>
                        @endif

                        @if($canSell || $isModerator)
                        <section id="art-edit-auc" class="form-screen inset form-options-2" {{ $opt != 'art-edit-auc' ? 'style=display:none;' : '' }}>
                            <h3>Опции продажи</h3>
                            <span>В галерее виртуальных коллекций возможна продажа и аренда работ. На конкретную работу можно также объявить аукцион.</span>
                            <br/><br/>
                            <label>Продажа возможна</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="buy_possible" off_value="0" value="1" {{ old('buy_possible', $item->buy_possible) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Аренда возможна</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="rent_possible" off_value="0" value="1" {{ old('rent_possible', $item->rent_possible) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Стоимость Продажи (руб)</label>
                            <input type="text" class="form-control pull-right" style="width: 130px;" placeholder="Стоимость" name="buy_price" value="{{ old('buy_price', $item->buy_price) }}">
                            <br/><br/>
                            <label>Стоимость Аренды (руб/мес)</label>
                            <input type="text" class="form-control pull-right" style="width: 130px;" placeholder="Стоимость" name="rent_price" value="{{ old('rent_price', $item->rent_price) }}" >
                            <br/><br/><br/><br/>
                            @if(!$isNew)
                                <div class="centered">
                                    <a type="button" href="{{ url('auction/start/'.$item->id) }}" class="btn btn-lg btn-info">НАЧАТЬ АУКЦИОН</a>
                                </div>
                            @endif
                        </section>
                        @endif
                        <section id="art-edit-soc" class="form-screen inset form-options-3" {{ $opt != 'art-edit-soc' ? 'style=display:none;' : '' }}>
                            <h3>Настройки приватности</h3>
                            <br/><br/>
                            <label>Запретить Просмотр</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="visibility" off_value="0" value="2" {{ old('visibility', $item->visibility) == 2 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Разрешить Комментарии</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="allow_comments" off_value="0" value="1" {{ old('allow_comments', $item->allow_comments) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Показывать историю аукционов</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="auction_history" off_value="0" value="1" {{ old('auction_history', $item->auction_history) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                        </section>

                        @if(!$isNew)
                        <section id="art-edit-del" class="form-screen inset form-options-1" {{ $opt != 'art-edit-del' ? 'style=display:none;' : '' }}>
                            @if($canDelete)
                            <button type="button" class="btn btn-form btn-lg btn-danger" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить работу?" data-action="{{ url('artwork/delete/'.$item->id) }}">УДАЛИТЬ</button>
                            @else
                            <span>{{ $deleteMessage }}</span>
                            @endif
                        </section>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>
    <button type="button" data-form="art-edit-form" class="btn btn-form btn-lg btn-success">ГОТОВО</button>
    <a type="button" href="{{ url('/') }}" class="btn btn-form btn-lg btn-default">ОТМЕНА</a>
</div>

@stop