@extends('webmaster.app')

@section('title', 'Жанры')

@section('content')
    <div class="shaded-header">
        <h3 style="margin-top: 0;">Жанры</h3>
    </div>

    @include('webmaster.listedit')
@stop
