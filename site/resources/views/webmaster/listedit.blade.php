<div class="panel panel-default">
    <div class="panel-body">

        @if($success)
            <div class="alert alert-success">Изменения применены</div>
        @endif

        <ul class="list-group">
            @foreach($data['list'] as $key=>$value)
                <li class="list-group-item" data-option="{{ $key }}"><a href="/wm/catitem/{{ $data['type'] }}/{{ $key }}">{{ $value }}</a></li>
            @endforeach
        </ul>
        <div class="centered"><a type="button" href="/wm/catitem/{{ $data['type'] }}" class="btn btn-lg btn-info">ДОБАВИТЬ</a></div>
    </div>
</div>

