<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Галерея Виртуальных Коллекций - @yield('title')</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="{{ url('/') }}"/>
    <meta name="title" content="Галерея Виртуальных Коллекций - @yield('title')">
    <meta name="keywords" content="">
    <meta name="description" content="Галерея Виртуальных Коллекций - сайт для обмена и покупки цифровых произведений искусства">
    <meta name="author" content="Oleg L">
    <meta name="date" content="20.04.2015">
    <meta name="lang" content="ru">
    <meta name="robots" content="noindex,nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <base href="{{ url('/') }}/">
    <meta name="abstract" content="Галерея Виртуальных Коллекций - сайт для обмена и покупки цифровых произведений искусства">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.png">

    @section('css')
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/bootstrap/css/bootstrap.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/bootstrap-select/bootstrap-select.min.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/bootstrap-datepicker/css/datepicker.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/mcustomscrollbar/jquery.mCustomScrollbar.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/jquery.jcrop/jquery.Jcrop.min.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/elastislide/elastislide.css') }}" />
        <link rel="stylesheet" media="screen" href="{{ asset('/ext/datatables/css/jquery.dataTables.min.css') }}"/>
        <link rel="stylesheet" media="screen" href="{{ asset('/css/site.css') }}" />
    @show

    <!--[if lt IE 9]>
        <script src="site/ext/html5shiv/html5shiv.js"></script>
        <script src="site/ext/respond/respond.min.js"></script>
        <![endif]-->
        <script src="{{ asset('/ext/modernizr/modernizr.js') }}"></script>
        <script src="{{ asset('/ext/jquery/jquery-2.0.3.min.js') }}"></script>

    @section('js')
        <script src="{{ asset('/ext/jquery.ui/jquery-ui-1-11-2.min.js') }}"></script>
        <script src="{{ asset('/ext/jquery.cookie/jquery.cookie.js') }}"></script>
        <script src="{{ asset('/ext/jquery.form/jquery.form.min.js') }}"></script>
        <script src="{{ asset('/ext/scrollto/scrollto.js') }}"></script>
        <script src="{{ asset('/ext/jquery.mousewheel/jquery.mousewheel.js') }}"></script>
        <script src="{{ asset('/ext/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('/ext/jquery.jcrop/jquery.Jcrop.min.js') }}"></script>
        <script src="{{ asset('/ext/elastislide/elastislide.js') }}"></script>
        <script src="{{ asset('/ext/history/jquery.history.js') }}"></script>

        <script src="{{ asset('/ext/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/ext/bootstrap-select/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('/ext/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('/ext/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('/ext/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
        <script src="{{ asset('/ext/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('/js/site.js') }}"></script>
    @show

</head>
<body>

@section('main')
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <div class="site_title">
                    <p>Галерея Виртуальных Коллекций</p>
                </div>
            </div>
        </nav>
    </div>

    <div class="container">

        <div class="view eqh-wrap">
            <div class="sidebar eqh">
                <div class="sidebar-area">
                    @include('webmaster.sidebar')
                </div>
            </div>
            <div class="content eqh">
                <div class="content-area eqh">
                    @yield('content')
                </div>
            </div>
        </div>

        <footer>
            <div class="view">
                <div class="sidebar"></div>
                <div class="content">
                    <p>&copy; Галерея Виртуальных Коллекций 2015</p>
                    <a href="{{ url('/legal') }}" style="color: #bbb">Политика прав</a>
                    <a href="{{ url('/mission') }}" style="color: #bbb">Цели Сайта</a>

                </div>
            </div>
        </footer>

    </div>
@show

@include('content.confirm_dialog')

<div id="phonediv" class="visible-xs"></div>
</body>
</html>
