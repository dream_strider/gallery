@extends('webmaster.app')

@section('title', 'Добавление Пользователя')

@section('content')
<div class="shaded-header">
    <h3 style="margin-top: 0;">Добавление Пользователя</h3>
</div>

<div class="row">
    <div class="col-sm-9 col-md-6 col-lg-4" >

        @if (count($errors) > 0 && session('last_action') == 'wm/add_user' )
        <div class="alert alert-danger">
            Ошибка добавления пользователя!<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if($success)
        <div class="alert alert-success">Пользователь зарегистрирован</div>
        @endif


        <form class="noautocomplete inset " role="form" method="POST" action="{{ url('/wm/add_user') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label>Имя пользователя (логин)</label>
            <input type="text" class="form-control form-control-ofs" placeholder="Логин" name="name" value="{{ old('name') }}">

            <label>Адрес электронной почты (e-mail)</label>
            <input type="text" class="form-control form-control-ofs" placeholder="e-mail" name="email" value="{{ old('email') }}">

            <label>Реальное Имя и Фамилия</label>
            <input type="text" class="form-control form-control-ofs" placeholder="Имя и Фамилия" name="real_name" value="{{ old('real_name') }}">

            <label>Тип регистрации</label>
            <select class="selectpicker" data-width="100%" title="Тип регистрации" name="role" value="{{ old('role') }}">
                <option value="" {{ empty(old('role')) ? 'selected' : '' }}>Не задан</option>
                <option value="1" {{ old('role') == 1 ? 'selected' : '' }}>Автор</option>
                <option value="2" {{ old('role') == 2 ? 'selected' : '' }}>Коллекционер</option>
                <option value="3" {{ old('role') == 3 ? 'selected' : '' }}>Модератор</option>
                <option value="4" {{ old('role') == 4 ? 'selected' : '' }}>Вебмастер</option>
            </select>

            <label>Пароль</label>
            <input type="password" class="form-control form-control-ofs" placeholder="пароль" name="password">

            <label>Подтвердите Пароль</label>
            <input type="password" class="form-control form-control-ofs" placeholder="подтвердите пароль" name="password_confirmation" >

            <input type="submit" class="btn btn-lg btn-info btn-form" value="Зарегистрировать" />

        </form>
    </div>
</div>
@stop
