<div id="sidebar" class="hidden-xs collapsible-xs collapse">

    <div class="userblock">
        <a href="{{ url('user/profile') }}"><img src="{{ url(Auth::user()->thumbUrl('small')) }}" width="58" height="58" /></a>
        <a href="{{ url('user/profile') }}"><div class="name">{{ Auth::user()->real_name }}</div></a>
        <a href="{{ url('/wm/logout') }}">Выход</a>
    </div>

    <div class="mainmenu">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="{{ url('/wm/messages') }}">Сообщения</a></li>
            <li><a href="{{ url('/wm/users') }}">Пользователи</a></li>
            <li><a href="{{ url('/wm/genres') }}">Жанры</a></li>
            <li><a href="{{ url('/wm/technics') }}">Техники</a></li>
            <li><a href="{{ url('/wm/devices') }}">Устройства</a></li>
        </ul>
    </div>

</div>
