@extends('webmaster.app')

@section('title', '{{$title}}')

@section('content')
    <div class="shaded-header">
        <h3 style="margin-top: 0;">{{$title.' - '.($name ? $name : 'Новый')}}</h3>
    </div>

    @if (count($errors) > 0 && session('last_action') == 'wm/catitem/'.$type.'/'.$id )
        <div class="alert alert-danger">
            Возникли ошибки при применении изменений:
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="javascript:void(0);" data-panel="tab-content" data-tab="name" data-input="action">{{ $name ? 'Переименовать' : 'Имя Элемента' }}</a></li>
        @if($name)
            <li role="presentation"><a href="javascript:void(0);" data-panel="tab-content" data-tab="del" data-input="action">Удалить</a></li>
        @endif
    </ul>

    <form class="noautocomplete" action="{{ url('wm/catitem/'.$type.'/'.$id) }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="type" value="{{ $type }}">
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" id="action" name="action" value="name">

        <div id="name" class="tab-content" style="padding: 10px;">
            <label>Название</label>
            <input type="text" class="form-control" name="item_name" value="{{$name}}">
        </div>

        <div id="del" class="tab-content" style="padding: 10px;display: none;">
            <label>Заменить при удалении на:</label>
            <select class="selectpicker" data-width="100%" name="item_replace" style="position:relative;">
                <option value="" selected>Не Задан</option>
                @foreach($list as $key=>$value)
                    @if($key != $id)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="centered"><button type="submit" class="btn btn-lg btn-info">Применить</button></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
    </form>

@stop
