@extends('webmaster.app')

@section('title', 'Пользователи')

@section('content')
    <div class="shaded-header">
        <div class="row">
            <div class="col-md-7 col-lg-9">
                <h3 style="margin-top: 0;">Пользователи</h3>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-3">
                <a type="button" href="{{ url('/wm/createuser') }}" class="btn btn-default">Добавить Пользователя</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="padding: 10px 20px 10px 20px;">


            <table class="table table-striped table-bordered order-column" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Логин</th>
                    <th>Имя</th>
                    <th>Роль</th>
                    <th>Почта</th>
                    <th>Активирован</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                </tr>
                </tfoot>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td><a style="text-decoration: underline;" href="{{ url('user/edit/'.$user->id) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->real_name }}</td>
                        <td>{{ $user->role == App\UserRole::Author ? 'Автор' : ($user->role == App\UserRole::Collector ? 'Коллекционер' : ($user->role == App\UserRole::Moderator ? 'Модератор' : 'Вебмастер')) }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->active ? "Да" : "Нет" }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@stop
