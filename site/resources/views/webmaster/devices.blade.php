@extends('webmaster.app')

@section('title', 'Устройства')

@section('content')
    <div class="shaded-header">
        <h3 style="margin-top: 0;">Устройства</h3>
    </div>

    @include('webmaster.listedit')
@stop
