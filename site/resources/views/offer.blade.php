@extends('app')

@section('title', $buyer ? 'Покупка рыботы' : 'Продажа работы')

@section('content')

    <div class="shaded-header">
        <h3 style="margin-top: 0;">{{ $buyer ? 'Покупка рыботы '.$item->artwork->title : 'Продажа работы '.$item->artwork->title  }}</h3>
    </div>

    @if($success)
        <div class="alert alert-success">Предложение принято</div>
    @endif

    @if (count($errors) > 0 && session('last_action') == 'artwork/offer' )
        <div class="alert alert-danger">
            Возникли ошибки:
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="content-block">
        <div class="row">
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="centered">
                    <a href="#" data-rowid="art" data-itemid="{{ $item->artwork->id }}" class="select_thumb">
                        <img class="file-preview" src="{{ url( $item->artwork->thumbUrl('mid')) }}" width="100%" />
                    </a>
                    <h3>Владелец: <a href="{{ url('user/profile/'.$item->artwork->owner->id) }}">{{ $item->artwork->owner->real_name }}</a></h3>
                    <h3>Покупатель:  <a href="{{ url('user/profile/'.$item->buyer->id) }}">{{ $item->buyer->real_name }}</a></h3>
                </div>
            </div>
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">

                @if($meaccepted)
                    <div class="alert alert-success">
                        Вы приняли это предложение
                    </div>
                @endif
                @if($accepted)
                    @if($buyer)
                    <div class="alert alert-success">
                        Продавец принял это предложение
                    </div>
                    <div class="centered">
                        <a href="{{url('artwork/offer/'.$item->id.'/pay')}}" type="button" class="btn btn-form btn-lg btn-danger">ОПЛАТИТЬ</a>
                    </div>
                    @else
                    <div class="alert alert-success">
                        Покупатель принял это предложение
                    </div>
                    @endif
                @endif

                <form class="noautocomplete" id="buy-form" action="{{ url('artwork/offer') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $item->id }}">

                    <div class="form-group has-success">
                        <label class="control-label" for="offer">Текущее предложение (руб):</label>
                        <input type="text" class="form-control" id="offer_price" name="price" value="{{ old('price', $item->price) }}" {{ $accepted ? 'readonly' : '' }}>
                    </div>

                    @if($item->artwork->rent_possible)
                        <div class="checkbox has-success">
                            <label>
                                <input type="checkbox" id="rent_cb" name="rent" value="1" {{ $item->rent_period > 0 ? 'checked' : '' }} {{ $accepted ? 'disabled' : '' }}> Арендовать
                            </label>
                        </div>
                    @endif

                    <div id="rent_period_div" class="form-group has-success" {{ !$item->rent_period > 0 ? "style=display:none;" : "" }}>
                        <label class="control-label" for="rent">Срок аренды (мес):</label>
                        <input type="text" class="form-control" id="offer_rent_period" name="rent_period" value="{{ old('rent_period', $item->rent_period) }}" {{ $accepted ? 'readonly' : '' }}>
                    </div>

                    <textarea class="form-control comment-entry" placeholder="Ваш комментарий" rows="2" name="comment" style="{{ $accepted ? 'display: none;' : '' }}"></textarea>

                    <div class="centered">
                        @if(!$meaccepted)
                        <div id="offer_accept" {{ empty($item->id) ? 'style=display:none;' : '' }}>
                            <input type="submit" class="btn btn-form btn-lg btn-success" name="accept" value="ПРИНЯТЬ"/>
                        </div>
                        @endif

                        <div id="offer_change" {{ !empty($item->id) ? 'style=display:none;' : '' }}>
                            <input type="submit" class="btn btn-form btn-lg btn-info" name="offer" value="ПРЕДЛОЖИТЬ"/>
                            <a href="{{ Request::url() }}" type="button" class="btn btn-form btn-lg btn-danger">ОТМЕНА</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
        </div>

        @if($item->id > 0)
        <div class="row">
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div id="item-comments">
                    @include('content.comment_list', ['thread' => 'ofr'.$item->id, 'item' => $item->artwork])
                </div>
            </div>
            <div class="col-md-3 col-lg-4 visible-md visible-lg">
                &nbsp;
            </div>
        </div>
        @endif
    </div>

@stop
