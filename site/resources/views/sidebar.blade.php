@if (Route::getCurrentRoute()->getActionName() != 'App\Http\Controllers\Auth\AuthController@getLogin' && count($errors) > 0 && session('last_action') == 'auth/login' )
<div class="alert alert-danger alert-login">
    Ошибка входа<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div id="sidebar" class="hidden-xs collapsible-xs collapse">

@if (Auth::check())

<div class="userblock">
	<a href="{{ url('user/profile') }}"><img src="{{ url(Auth::user()->thumbUrl('small')) }}" width="58" height="58" /></a>
	<a href="{{ url('user/profile') }}"><div class="name">{{ Auth::user()->real_name }}</div></a>
	<a href="{{ url('/auth/logout') }}">Выход</a>
</div>

@elseif( Route::getCurrentRoute()->getActionName() != 'App\Http\Controllers\Auth\AuthController@getLogin')

<form class="loginForm" action="{{ url('/auth/login') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <input type="text" placeholder="логин" class="form-control" name="name" value="{{ old('name') }}">
    </div>
    <div class="form-group">
        <input type="password" placeholder="пароль" class="form-control" name="password">
    </div>
    <div class="form-group">
        <input class="loginRememberme" type="checkbox" name="remember" />
        <label for="remember">Запомнить меня</label>
        <button type="submit" class="btn btn-success pull-right" name="Login" value="Вход">Вход</button>
        <br/><a href="{{ url('/auth/register') }}">Зарегистрироваться</a>
        <br/><a href="{{ url('/password/email') }}">Забыли пароль?</a>
    </div>
    <div class="clearfix"></div>
</form>

@endif
    
<div class="mainmenu">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('/') }}">Главная</a></li>
		@if (Auth::check())
        <li><a href="{{ url('/user/profile') }}">Мой Профиль</a></li>
		@endif
    </ul>
</div>

@include('sidebar.messages_block')
@include('sidebar.auctions_block')
@include('sidebar.collections_block')
@include('sidebar.arts_block')

<div class="mainmenu">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('hall-of-fame') }}">Зал Славы</a></li>
        <li><a href="{{ url('users') }}">Пользователи</a></li>
        <li><a href="{{ url('mission') }}">О Нас</a></li>
    </ul>
</div>

</div>
