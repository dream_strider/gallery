<div id="usr_new_block" class="row ajax">
	@foreach (App\Repository::query('usr')->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc')->take(6)->get() as $item)
		@include('content.thumb_user', ['item' => $item])
	@endforeach
</div>
