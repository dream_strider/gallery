@extends('content.focused')

@section('class', 'faddslide')

@section('main')
<div id="sshow_select_img">
    <ul class="addlist">
        @foreach (App\Repository::query('art')->where('owner_id', Auth::user()->id)->get() as $i)
        <li data-itemid="{{ $i->id }}" data-col="{{ $i->collection_id }}">
            <div class="preview_pic"><img src="{{ $i->thumbUrl('mid') }}"></div>
            <div class="preview_title">{{ $i->title }}</div>
        </li>
        @endforeach
    </ul>
</div>
@stop

@section('props')

<input type="text" id="collection_ref" name="collection_ref" style="display:none;" value="0">
<ul class="col-select" role="single-select" data-select="collection_ref">
    <li data-option="0" style="padding-left: 20px;padding-top:7px; padding-bottom:7px;">Все Работы</li>
    @foreach (App\Repository::query('col')->where('created_by', Auth::user()->id)->orderBy('default', 'desc')->orderBy('title', 'asc')->get() as $i)
    <li data-option="{{ $i->id }}" style="padding-left: 20px;padding-top:7px; padding-bottom:7px;"><img src="{{ $i->thumbUrl('small') }}" width="50px" height="50px" />&nbsp;{{ $i->title }}</li>
    @endforeach
</ul>

@stop
