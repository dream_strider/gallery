<div class="thumb_item @if($item->currentStatus == \App\AuctionStatus::Started) auc_active @endif col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="javascript:void(0);" data-rowid="auc" data-itemid="{{ $item->id }}" class="select_thumb">
                <img src="{{ url($item->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->title }}</h4>
            <p class="lbl">Статус:&nbsp{{ $item->currentStatusText }}</p>
            <p class="lbl">Владелец лота:&nbsp<a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></p>
			<p class="lbl">
                @if($item->currentStatus == \App\AuctionStatus::Started)
                    До окончания:&nbsp {{ Util::interval($item->remTime) }}
                @elseif($item->currentStatus == \App\AuctionStatus::NotStarted)
                    До начала:&nbsp {{ Util::interval($item->remTime) }}
                @else &nbsp;
                @endif</p>
        </div>
    </div>
</div>