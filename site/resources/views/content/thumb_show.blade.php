<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="{{ url('slideshow/fullscreen/'.$item->id) }}" >
                <img src="{{ url($item->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->title }}</h4>
            <p class="text">{{ str_limit($item->description, 80, '...') }}</p>
        </div>
    </div>
</div>