<div id="profile_art_block" class="preview_list {{ $tab == '' ? 'ajax' : '' }}" data-ajax-user_id="{{ $user_id }}" data-ajax-tab="{{ $tab }}" data-ajax-user_role="{{ $user_role }}" data-ajax-me="{{ $me }}">
	<div class="header">
		<h3>Работы</h3>
		&nbsp;&nbsp;
		@if($tab == '')
		<a href="{{ url('/user/profile/'.$user_id.'?tab=art') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('art')->where('owner_id', $user_id)->count() }}</span></a>&nbsp;&nbsp;
		@endif
		@if($user_role == \App\UserRole::Author && $me)
		<a type="button" href="{{ url('artwork/add') }}" class="btn btn-success">Добавить работу</a>
		@endif
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('art')->where('owner_id', $user_id)->with('author')->with('owner')->orderBy('created_at', 'desc')->take($tab == 'art' ? 10000 : 6)->get() as $item)
			@include('content.thumb_artwork', ['item' => $item])
		@endforeach
	</div>
</div>
