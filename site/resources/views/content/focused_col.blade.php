@extends('content.focused')

@section('header')

@if($my || $isModerator)
<a type="button" href="{{ url('collection/edit/'.$item->id) }}" class="btn btn-success pull-left">Редактировать</a>
@endif

@stop

@section('datah')
<div class="gallery centered">
    <ul id="carousel" class="elastislide-list">
        @foreach ($item->artworks as $art)
            <li data-preview="{{ url($art->thumbUrl('large')) }}" data-title="{{ $art->title }}" data-desc="{{ $art->description }}"><a href="javascript:void(0);"><img src="{{ url($art->thumbUrl('mid')) }}" alt="{{ $art->title }}" /></a></li>
        @endforeach
    </ul>
</div>
@stop

@section('main')
<div class="centered" style="margin-top: 50px;">
    <h2 id="carousel_preview_title"></h2>
    <img width="100%" id="carousel_preview" />
    <p id="carousel_preview_desc"></p>
</div>
@stop

@section('props')
@if(Auth::check())
<a href="javascript:void(0);" data-toggle="modal" data-target="#confirmModal" data-confirm="Сообщить модератору?" data-action="{{ url('report/col/'.$item->id) }}" class="glyphicon glyphicon-flag report" style="font-size: 1.0em; z-index: 1200; color: #ff0000;" title="Сообщить модератору"></a>
@endif
<h2>{{ $item->title }}</h2>
<div class="intro">{{ $item->description }}</div>
<br/>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Владелец:</div><div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></div></div>
<br/>

@include('content.comments', ['thread' => 'col'.$item->id, 'item' => $item])

@stop
