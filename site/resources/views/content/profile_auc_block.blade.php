<div id="profile_auc_block" class="preview_list {{ $tab == '' ? 'ajax' : '' }}" data-ajax-user_id="{{ $user_id }}" data-ajax-tab="{{ $tab }}">
	<div class="header">
		<h3>Аукционы</h3>
		&nbsp;&nbsp;
		@if($tab == '')
		<a href="{{ url('/user/profile/'.$user_id.'?tab=auc') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('auc')->where('created_by', $user_id)->count() }}</span></a>&nbsp;&nbsp;
		@endif
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('auc')->where('created_by', $user_id)->with('artwork')->with('owner')->orderBy('created_at', 'desc')->take($tab == 'auc' ? 10000 : 6)->get() as $item)
			@include('content.thumb_auction', ['item' => $item])
		@endforeach
	</div>
</div>
