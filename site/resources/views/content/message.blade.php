<li class="{{ $item->_read ? '' : 'unread' }}" data-mid="{{ $item->id}}" data-tid="{{ $item->reply ? $item->reply : $item->id }}" data-time="{{ strtotime($item->date_sent) }}">
  <a href="javascript:void(0);" class="del" alt="Удалить"><span class="glyphicon glyphicon-remove-circle"></span></a>
  <a href="javascript:void(0);" class="hdr">
	<div>
      @if(substr($item->subject, 0, 12)=='to:Webmaster')<span class="label label-warning header_label">Вебмастеру</span>@endif
	  <img src="{{ isset($item->sender) ? $item->sender->thumbUrl('small') : 'img/Servise_rus.svg'}}" alt="{{ isset($item->sender) ? $item->sender->real_name : 'Галерея виртуальных коллекций' }}" title="{{ isset($item->sender) ? $item->sender->real_name : 'Галерея виртуальных коллекций' }}"" width="40px" height="40px" />
	  <div class="tophdr">
	    <span class="date">{{ Util::timeAgo($item->date_sent) }}</span>
	    <span class="name">{{ isset($item->sender) ? $item->sender->real_name : 'Галерея виртуальных коллекций' }}</span>
	  </div>
	  <span class="header">{{ $item->subject }}</span>
    </div>
  </a>
  <div class="fulltext">
    {!! $item->text !!}

    @if($item->sender && $item->sender_id != Auth::user()->id)
      <hr/>
      <a href="javascript:void(0);" alt="Ответить" class="reply to-user"><span class="glyphicon glyphicon-share-alt"></span>Ответить</a>
      @if(substr($item->subject, 0, 12)=='to:Webmaster' && $item->service != \App\UserRole::Webmaster)<a href="javascript:void(0);" alt="отправить вебмастеру" class="reply to-webmaster" data-mid="{{ $item->id}}" data-tid="{{ $item->reply ? $item->reply : $item->id }}"><span class="glyphicon glyphicon-forward"></span>Отправить вебмастеру</a>@endif
      <textarea data-reply-mid="{{ $item->id }}"></textarea>
      <button class="btn btn-success send-reply" data-mid="{{ $item->id}}" data-tid="{{ $item->reply ? $item->reply : $item->id }}">Отправить</button>
      <div class="clearfix"></div>
    @endif
    <ul id="thread_{{ $item->id }}"></ul>
  </div>
</li>
