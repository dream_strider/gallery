<div id="art_preview_block" class="preview_list ajax">
	<div class="header">
		<h3>Работы</h3>
		<a href="{{ url('/artwork/list') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('art')->count() }}</span></a>&nbsp;&nbsp;
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('art')->with('author')->with('owner')->orderBy('created_at', 'desc')->take(6)->get() as $item)
			@include('content.thumb_artwork', ['item' => $item])
		@endforeach
	</div>
</div>
