<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="{{ url('user/profile/'.$item->id) }}">
                <img src="{{ $item->thumbUrl('mid') }}" />
            </a>
        </div>
        <div class="caption">
            <h5>{{ $item->real_name }}</h5>
            <p class="lbl"><strong>{{ $item->role == App\UserRole::Author ? 'Автор' : 'Коллекционер' }}</strong></p>
            <p class="lbl">Работ:&nbsp{{ $item->artworkCount }}</p>
            <p class="lbl">Коллекций:&nbsp{{ $item->collectionCount }}</p>
        </div>
    </div>
</div>
