<div id="hall_purchase" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-search="{{ $search }}">
	@foreach (App\Repository::query('hfpurchase', $search)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
	@include('content.thumb_fame', ['item' => $item, 'caption' => 'куплено работ', 'value' => $item->purchase_count])
	@endforeach
</div>
