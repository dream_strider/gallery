<div id="comments" class="comments">
	<ol class="quip-comment-list">
		@foreach ( App\Comment::where('thread', $thread)->with('sender')->orderBy('created_at', 'desc')->get() as $comment)
		<li class="quip-comment">
			<div class="quip-comment-body">
				<a href="{{ url('user/profile/'.$comment->sender->id) }}"><img src="{{ url($comment->sender->thumbUrl('small')) }}" width="32" height="32" /></a>

				<span class="quip-comment-author"><strong>{{ $comment->sender->real_name }}</strong></span>
				@if (Auth::check() && ($item->owner->id == Auth::user()->id || Auth::user()->role == \App\UserRole::Moderator))
					<button type="submit" class="btn btn-danger btn-xs" style="float:right;" data-form="post-comment" data-action="javascript: updateComments('{{ url('/comment/delete/'.$comment->id) }}');" data-toggle="modal" data-target="#confirmModal" data-confirm="Delete comment?">X</button>
				@endif
				<p>{{ $comment->text }}</p>
				<div class="quip-break"></div>
			</div>
		</li>
		@endforeach
	</ol>
</div>
