<div id="hall_artviews" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-search="{{ $search }}">
	@foreach (App\Repository::query('hfartviews', $search)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
		@include('content.thumb_fame', ['item' => $item, 'caption' => 'просмотров', 'value' => $item->count])
	@endforeach
</div>
