<div class="row"><div class="col-sm-6" style="text-align: left;padding:0;"><strong>Комментарии</strong></div><div class="col-sm-6" style="text-align: right;"><strong class="quip-count">{{ App\Comment::countOf($thread) }}</strong></div></div>

@if (Auth::check() && ($item->allow_comments || $item->owner->id == Auth::user()->id ))
<form id="post-comment" action="{{ url('/comment') }}" class="postcomment" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="thread" value="{{ $thread }}">
    @if($item->owner->id == Auth::user()->id || Auth::user()->role == \App\UserRole::Moderator)
        <button type="submit" class="btn btn-danger" style="margin-bottom: 1rem;" data-action="javascript: updateComments('{{ url('/comment/delete_all/'.$thread) }}');" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить все комментарии?">Удалить все</button>
        <a id="disable_comments_btn" type="button" class="btn btn-danger" style="margin-bottom: 1rem;{{ !$item->allow_comments ? 'display: none;' : '' }}" href="javascript: updateComments('{{ url('/comment/disable/'.$thread) }}', 1);">Запретить комментарии</a>
        <a id="enable_comments_btn" type="button" class="btn btn-info" style="margin-bottom: 1rem;{{ $item->allow_comments ? 'display: none;' : '' }}" href="javascript: updateComments('{{ url('/comment/enable/'.$thread) }}', 2);">Разрешить комментарии</a>
    @endif
    <textarea class="form-control comment-entry" placeholder="Ваш комментарий" rows="2" name="comment">{{ old('comment') }}</textarea>
    <div class="comment-actions" style="display:none">
        <button type="submit" class="btn btn-success" data-form="post-comment" data-submit="{{ url('/comment') }}">Отправить</button>
    </div>
</form>
@else
    @if (!Auth::check())
        <p>Пожалуйста, авторизуйтесь для того, чтобы комментировать</p>
    @elseif(!$item->allow_comments && $item->owner->id != Auth::user()->id)
        <p>Комментирование запрещено владельцем</p>
    @endif
@endif

<div id="item-comments">
	@include('content.comment_list', ['thread' => $thread, 'item' => $item])
</div>