<div class="fmodal">
    <div class="focused @yield('class')">
        <div class="row fcontent" style="overflow: hidden;">
            <div class="fdataitem-cont col-sm-7 col-md-8 col-lg-9" style="height:100%; padding:0;">
                <a href="javascript:void(0);" class="glyphicon glyphicon-remove fclose"></a>
                <div class="row fheader shownotempty">
                    <div class="inset">
                        @yield('header')
                    </div>
                </div>
                @yield('datah')
                <div class="row" style="height:100%">
                    <div class="fdataitem-main col-sm-12">
                        @yield('main')
                    </div>
                </div>
            </div>
            <div class="fdataitem-props scrollable col-sm-5 col-md-4 col-lg-3">
                @yield('props')
            </div>
        </div>
    </div>
</div>