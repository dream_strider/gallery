@extends('content.focused')

@section('class', 'fauc')

@section('header')

@if($isModerator)
    <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить аукцион?" data-action="{{ url('auction/delete/'.$item->id) }}">УДАЛИТЬ</button>
@endif

@if($item->currentStatus == App\AuctionStatus::Finished && Auth::check() && $item->winner_id == Auth::user()->id)
    <a type="button" href="{{ url('auction/pay/'.$item->id) }}" class="btn btn-success pull-left">Оплатить лот</a>
@endif

<h3>@if($item->artwork) {{ $item->artwork->title }} @else Работа удалена @endif</h3>
@stop

@section('datah')

@include('content.auc_params', ['item' => $item])

@stop

@section('main')
<div class="centered" style="margin-top: 50px;">
    <img width="100%" src="@if($item->artwork) {{ url($item->artwork->thumbUrl('large')) }} @endif"/>
    <div class="row">
        <div class="col-sm-6" style="text-align: left;">Просмотров: {{ $item->view_count }}</div>
        <div class="col-sm-6" style="text-align: right; display: none;">Следят: 0</div>
    </div>
</div>
@stop

@section('props')
@if(Auth::check())
<a href="javascript:void(0);" data-toggle="modal" data-target="#confirmModal" data-confirm="Сообщить модератору?" data-action="{{ url('report/auc/'.$item->id) }}" class="glyphicon glyphicon-flag report" style="font-size: 1.0em; z-index: 1200; color: #ff0000;" title="Сообщить модератору"></a>
@endif
<h2>{{ $item->title }}</h2>
@if($item->artwork)
<div class="intro">{{ $item->artwork->description }}</div>
<br/>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Автор:</div>@if($item->artwork->author)<div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->artwork->author->id) }}">{{ $item->artwork->author->real_name }}</a></div>@else Удален @endif</div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Владелец:</div><div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->artwork->owner->id) }}">{{ $item->artwork->owner->real_name }}</a></div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">В Коллекции:</div><div class="col-sm-6 col-md-7">{{ $item->artwork->collection->title }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Жанр:</div><div class="col-sm-6 col-md-7">{{ $item->artwork->genreText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Техника исполнения:</div><div class="col-sm-6 col-md-7">{{ $item->artwork->technicText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Размер:</div><div class="col-sm-6 col-md-7">{{ $item->artwork->width }}x{{ $item->artwork->height }}</div></div>
<br/>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Тип Аукциона:</div><div class="col-sm-6 col-md-7">{{ $item->sell_typeText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Условия владения:</div><div class="col-sm-6 col-md-7">{{ $item->sell_conditionsText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Повтор возможен:</div><div class="col-sm-6 col-md-7">{{ $item->repeat_possible ? 'Да' : 'Нет' }}</div></div>
<br/>
@endif

<div class="row"><div class="col-sm-6" style="text-align: left;padding:0;"><strong>История ставок</strong></div><div class="col-sm-6" style="text-align: right;"><strong class="quip-count">{{ $item->numBids }}</strong></div></div>

<div id="bid_error" class="alert alert-danger" style="display: none"></div>

@if(!$my && !$isModerator)
    @if(!Auth::check())
        <input type="hidden" name="auc_id" value="{{ $item->id }}" />
        Пожалуйста, зарегистрируйтесь, чтобы сделать ставку
    @elseif(empty(Auth::user()->pay_method))
        <input type="hidden" name="auc_id" value="{{ $item->id }}" />
        Пожалуйста, укажите метод оплаты в настройках вашего профиля, чтобы сделать ставку
    @else
        <form id="post-bid" action="makebid" class="postbid" method="post" {{$item->currentStatus == \App\AuctionStatus::Started ? '' : 'style=display:none;' }}>
            <input type="hidden" name="auc_id" value="{{ $item->id }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input class="form-control comment-entry" type="number" min="1" placeholder="Ваша ставка" name="bid" />
            <div class="comment-actions" style="display:none">
                <textarea class="form-control" placeholder="комментарий" rows="4" name="comment"></textarea>
            </div>
            <button type="button" class="btn btn-success" data-form="post-bid" data-value="1" style="margin-top: 10px;">Сделать ставку</button>
        </form>
    @endif
@else
    <input type="hidden" name="auc_id" value="{{ $item->id }}" />
@endif

<div id="item-comments">
    <div class="comments">
        @include('content.bids', ['item' => $item])
    </div>
</div>

@stop
