<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="{{ url('artwork/offer/'.$item->id) }}">
                <img src="{{ url($item->artwork->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->artwork->title }}</h4>
            <p class="lbl">Цена:&nbsp;{{ $item->price }}&nbsp;руб.</p>
            <p class="lbl">Владелец:&nbsp<a href="{{ url('user/profile/'.$item->artwork->owner->id) }}">{{ $item->artwork->owner->real_name }}</a></p>
            <p class="lbl">Покупатель:&nbsp<a href="{{ url('user/profile/'.$item->buyer_id) }}">{{ $item->buyer->real_name }}</a></p>
        </div>
    </div>
</div>