<li>
      <a href="#" data-rowid="{{ $item->kind }}" data-itemid="{{ $item->id }}" class="select_thumb">
		<img src="{{ url($item->thumbUrl('small')) }}" width="27px" height="27px"/>
        {{ $item->title }}
      </a>
	  <span class="small">Просмотров: {{ $item->view_count }} / Комментарии: {{ App\Comment::countOf($item->kind.$item->id) }}</span>
</li>
