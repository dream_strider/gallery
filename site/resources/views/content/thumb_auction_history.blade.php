<div class="thumb_item @if($item->currentStatus == \App\AuctionStatus::Started) auc_active @endif col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="javascript:void(0);" data-rowid="auc" data-itemid="{{ $item->id }}" class="select_thumb">
                <img src="{{ url($item->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->title }}</h4>
            <p class="lbl">Статус:&nbsp{{ $item->currentStatusText }}</p>
            <p class="lbl">Владелец лота:&nbsp @if(isset($item->owner))<a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a>@else Удален @endif</p>
            <p class="lbl">Победитель:&nbsp @if(isset($item->winner))<a href="{{ url('user/profile/'.$item->winner->id) }}">{{ $item->winner->real_name }}</a>@else Удален @endif</p>
            <p class="lbl">Ставка:&nbsp {{ $item->final_stake }} руб.</p>
        </div>
    </div>
</div>