<div id="profile_col_block" class="preview_list {{ $tab == '' ? 'ajax' : '' }}" data-ajax-user_id="{{ $user_id }}" data-ajax-tab="{{ $tab }}" data-ajax-me="{{ $me }}">
	<div class="header">
		<h3>Коллекции</h3>
		&nbsp;&nbsp;
		@if($tab == '')
		<a href="{{ url('/user/profile/'.$user_id.'?tab=col') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('col')->where('created_by', $user_id)->count() }}</span></a>&nbsp;&nbsp;
		@endif
		@if($me)
		<a type="button" href="{{ url('collection/edit') }}" class="btn btn-success">Добавить коллекцию</a>
		@endif
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('col')->where('created_by', $user_id)->with('owner')->with('artworkCountRelation')->orderBy('created_at', 'desc')->take($tab == 'col' ? 10000 : 6)->get() as $item)
			@include('content.thumb_collection', ['item' => $item])
		@endforeach
	</div>
</div>
