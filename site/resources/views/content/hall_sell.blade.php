<div id="hall_sell" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-search="{{ $search }}">
	@foreach (App\Repository::query('hfsell', $search)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
	@include('content.thumb_fame', ['item' => $item, 'caption' => 'продано работ', 'value' => $item->sell_count])
	@endforeach
</div>
