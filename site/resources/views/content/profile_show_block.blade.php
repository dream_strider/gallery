<div id="profile_show_block" class="preview_list {{ $tab == '' ? 'ajax' : '' }}" data-ajax-user_id="{{ $user_id }}" data-ajax-tab="{{ $tab }}" data-ajax-me="{{ $me }}">
	<div class="header">
		<h3>Слайд Шоу</h3>
		&nbsp;&nbsp;
		@if($tab == '')
		<a href="{{ url('/user/profile/'.$user_id.'?tab=show') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('show')->where('created_by', $user_id)->count() }}</span></a>&nbsp;&nbsp;
		@endif
		<a type="button" href="{{ url('slideshow/create') }}" class="btn btn-success">Добавить слайд шоу</a>
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('show')->where('created_by', $user_id)->orderBy('created_at', 'desc')->take($tab == 'show' ? 10000 : 6)->get() as $item)
			@include('content.thumb_show', ['item' => $item])
		@endforeach
	</div>
</div>
