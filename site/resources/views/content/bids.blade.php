<ol id="bids" class="quip-comment-list">
    @foreach ( $item->bids->sortByDesc(function($b){return $b->id;}) as $bid)
    <li class="quip-comment">
        <div class="quip-comment-body">
            <img href="{{ url('user/profile/'.$bid->bidder->id) }}" src="{{ url($bid->bidder->thumbUrl('small')) }}" width="32" height="32" />

            <span class="quip-comment-author"><strong>{{ $bid->bidder->real_name }}</strong></span>
            <span style="float:right"><strong>{{ $bid->stake }}</strong></span>
            <p>{{ $bid->comment }}</p>
            <div class="quip-break"></div>
        </div>
    </li>
    @endforeach
</ol>
