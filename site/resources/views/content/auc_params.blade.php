<div id="auction_params" class="row" style="background-color: #222222; color: #eee;">
    <div class="col-sm-4 auc-el">Последняя ставка:<br/>
        @if($item->final_stake > 0)
            <span>{{ $item->final_stake }}</span> руб. (<a href="{{ url('user/profile/'.$item->winner->id) }}">{{ $item->winner->real_name }}</a>)
        @else
            <span>Нет ставок</span>
        @endif
    </div>

    @if( $item->currentStatus == \App\AuctionStatus::Started )
        <div class="col-sm-4 auc-el">Мин. ставка:<br/><span style="color: #ee0;">{{ $item->final_stake > 0 ? $item->final_stake + $item->stake_step : $item->initial_stake }}</span> руб.</div>
    @endif

    <div class="col-sm-4 auc-el">
        @if( $item->currentStatus == \App\AuctionStatus::Started )
            До окончания:<br/>
            <span style="color: #A00" class="auction_started">{{ Util::interval($item->remTime) }}</span>
        @elseif( $item->currentStatus == \App\AuctionStatus::NotStarted )
            До начала:<br/>
            <span style="color: #3a3">{{ Util::interval($item->remTime) }}</span>
        @else
            Статус аукциона:<br/>
            <span style="color: #3a3">{{ $item->currentStatusText }}</span>
        @endif
    </div>
</div>
