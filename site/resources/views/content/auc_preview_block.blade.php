<div id="auc_preview_block" class="preview_list ajax">
	<div class="header">
		<h3>Аукционы</h3>
		<a href="{{ url('/auction/list') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('auc')->count() }}</span></a>&nbsp;&nbsp;
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('auc')->with('artwork')->with('owner')->orderBy('created_at', 'desc')->take(6)->get() as $item)
			@include('content.thumb_auction', ['item' => $item])
		@endforeach
	</div>
</div>
