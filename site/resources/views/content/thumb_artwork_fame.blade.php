<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="javascript:void(0);" data-rowid="art" data-itemid="{{ $item->id }}" class="select_thumb">
                <img src="{{ url($item->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->title }}</h4>
            <p class="lbl">Стоимость:&nbsp{{ $price }} руб.</p>
            <p class="lbl">Автор:&nbsp @if($item->author)<a href="{{ url('user/profile/'.$item->author->id) }}">{{ $item->author->real_name }}</a>@else Удален @endif</p>
            <p class="lbl">Владелец:&nbsp<a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></p>
        </div>
    </div>
</div>