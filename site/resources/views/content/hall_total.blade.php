<div id="hall_total" class="ajax">
	<h4 style="margin-top: 0;">Всего авторов: {{ App\Repository::query('usr')->where('role', App\UserRole::Author)->count() }}</h4>
	<h4 style="margin-top: 0;">Всего коллекционеров: {{ App\Repository::query('usr')->where('role', App\UserRole::Collector)->count() }}</h4>
</div>
