<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="{{ url('user/profile/'.$item->id) }}">
                <img src="{{ $item->thumbUrl('mid') }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->real_name }}</h4>
            <p class="lbl">{{ $caption }}:&nbsp{{ $value }}</p>
        </div>
    </div>
</div>
