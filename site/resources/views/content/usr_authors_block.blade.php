<div id="usr_authors_block" class="row ajax">
	@foreach (App\Repository::query('usr')->where('role', App\UserRole::Author)->with('artworkCountRelation')->with('collectionCountRelation')->orderBy('created_at', 'desc')->take(6)->get() as $item)
		@include('content.thumb_user', ['item' => $item])
	@endforeach
</div>
