<div id="auc_active" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-ord="{{ $ord }}" data-ajax-search="{{ $search }}">
    @foreach (App\Repository::query('auc_active', $search, $ord)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
        @include('content.thumb_auction', ['item' => $item])
    @endforeach
</div>
