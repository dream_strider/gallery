<li>
    <a href="{{ url( isset($item->sender) ? 'user/profile/'.$item->sender->id : 'messages') }}">
        <img src="{{ isset($item->sender) ? $item->sender->thumbUrl('small') : 'img/Servise_rus.svg'}}" width="27px" height="27px" />
        {{ $item->subject }}
    </a>
</li>
