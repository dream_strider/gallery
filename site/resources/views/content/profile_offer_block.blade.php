<div id="profile_offer_block" class="preview_list {{ $tab == '' ? 'ajax' : '' }}" data-ajax-user_id="{{ $user_id }}" data-ajax-tab="{{ $tab }}" data-ajax-me="{{ $me }}">
	<div class="header">
		<h3>Предложения о покупке</h3>
		&nbsp;&nbsp;
		@if($tab == '')
		<a href="{{ url('/user/profile/'.$user_id.'?tab=offer') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('offer')->count() }}</span></a>&nbsp;&nbsp;
		@endif
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('offer')->orderBy('updated_at', 'desc')->take($tab == 'offer' ? 10000 : 6)->get() as $item)
			@include('content.thumb_offer', ['item' => $item])
		@endforeach
	</div>
</div>
