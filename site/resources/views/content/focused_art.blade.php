@extends('content.focused')

@section('header')

@if($canEdit)
<a type="button" href="{{ url('artwork/edit/'.$item->id) }}" class="btn btn-success pull-left">Редактировать</a>
@endif
@if($my)
<a type="button" href="{{ url('artwork/fullscreen/'.$item->id) }}" class="btn btn-info pull-left">Повесить</a>
@endif
@if($my && $item->canBeSoldOnAuction())
<a type="button" href="{{ url('auction/start/'.$item->id) }}" class="btn btn-info pull-left">Начать Аукцион</a>
@endif
@if(!$my && Auth::check() && $item->canBeSoldTo(Auth::user())))
<a type="button" href="{{ url('artwork/buy/'.$item->id) }}" class="btn btn-info pull-left">Покупка</a>
@endif
@if($my && $item->offers->count())
<a type="button" href="{{ url('user/profile?tab=offer') }}" class="btn btn-info pull-left">Предложения</a>
@endif
@if($onAuction)
<button type="button" data-rowid="auc" data-itemid="{{ $auctionId }}" class="select_thumb btn btn-danger pull-left">Открыть Аукцион</button>
@endif

@stop

@section('datah')
@stop

@section('main')
<div class="centered" style="margin-top: 50px;">
    <img width="100%" src="{{ url($item->thumbUrl('large')) }}"/>
    <div class="row">
        <div class="col-sm-6" style="text-align: left;">Просмотров: {{ $item->view_count }}</div>
        <div class="col-sm-6" style="text-align: right; display: none;">Следят: 0</div>
    </div>
</div>
@stop

@section('props')
@if(Auth::check())
<a href="javascript:void(0);" data-toggle="modal" data-target="#confirmModal" data-confirm="Сообщить модератору?" data-action="{{ url('report/art/'.$item->id) }}" class="glyphicon glyphicon-flag report" style="font-size: 1.0em; z-index: 1200; color: #ff0000;" title="Сообщить модератору"></a>
@endif
<h2>{{ $item->title }}</h2>
<div class="intro">{{ $item->description }}</div>
<br/>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Автор:</div>@if($item->author)<div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->author->id) }}">{{ $item->author->real_name }}</a></div>@else Удален @endif</div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Владелец:</div><div class="col-sm-6 col-md-7"><a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">В Коллекции:</div><div class="col-sm-6 col-md-7">{{ $item->collection->title }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Жанр:</div><div class="col-sm-6 col-md-7">{{ $item->genreText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Техника исполнения:</div><div class="col-sm-6 col-md-7">{{ $item->technicText }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Год создания:</div><div class="col-sm-6 col-md-7">{{ $item->release_date }}</div></div>
<div class="row ditem"><div class="col-sm-6 col-md-5 lbl">Размер:</div><div class="col-sm-6 col-md-7">{{ $item->width }}x{{ $item->height }}</div></div>
@if($item->auction_history)
<div class="row ditem"><div class="col-sm-12 col-md-5 lbl">История аукционов:</div><div class="col-sm-6 col-md-7"><a href="{{ url('auction/history/'.$item->id) }}">показать</a></div></div>
@endif
<br/>

@include('content.comments', ['thread' => 'art'.$item->id, 'item' => $item])

@stop
