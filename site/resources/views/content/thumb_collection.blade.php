<div class="thumb_item col-sm-6 col-md-3 col-lg-2" >
    <div class="thumbnail">
        <div class="thumbimage">
            <a href="javascript:void(0);" data-rowid="col" data-itemid="{{ $item->id }}" class="select_thumb">
                <img src="{{ url($item->thumbUrl('mid')) }}" />
            </a>
        </div>
        <div class="caption">
            <h4>{{ $item->title }}</h4>
            <p class="text">{{ str_limit($item->description, 80, '...') }}</p>
            <p class="lbl">Работ:&nbsp{{ $item->artworkCount }}</p>
            <p class="lbl">Владелец:&nbsp<a href="{{ url('user/profile/'.$item->owner->id) }}">{{ $item->owner->real_name }}</a></p>
        </div>
    </div>
</div>