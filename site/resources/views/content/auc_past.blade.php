<div id="auc_past" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-ord="{{ $ord }}" data-ajax-search="{{ $search }}">
    @foreach (App\Repository::query('auc_past', $search, $ord)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
        @include('content.thumb_auction_history', ['item' => $item])
    @endforeach
</div>
