<div id="hall_expensive" class="row showlist {{ $crit == 'all' ? 'ajax' : ''}}" data-ajax-crit="{{ $crit }}" data-ajax-search="{{ $search }}">
	@foreach (App\Repository::query('hfexpensive', $search)->take($crit == 'all' ? 6 : config('gallery.query_page_size'))->get() as $item)
	@include('content.thumb_artwork_fame', ['item' => $item->artwork, 'price' => $item->final_stake])
	@endforeach
</div>
