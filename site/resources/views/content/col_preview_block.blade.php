<div id="col_preview_block" class="preview_list ajax">
	<div class="header">
		<h3>Коллекции</h3>
		<a href="{{ url('/collection/list') }}">показать&nbsp;<span class="badge">{{ App\Repository::query('col')->count() }}</span></a>&nbsp;&nbsp;
	</div>
	<hr/>
	<div class="row">
		@foreach (App\Repository::query('col')->with('owner')->with('artworkCountRelation')->orderBy('created_at', 'desc')->take(6)->get() as $item)
			@include('content.thumb_collection', ['item' => $item])
		@endforeach
	</div>
</div>
