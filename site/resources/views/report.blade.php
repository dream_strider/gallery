@extends('app')

@section('title', 'Контакты')

@section('content')

    <div class="content-block">


        @if(Auth::check())

            <h2>Сообщить модератору</h2>

            <div class="row">
                <div class="col-sm-9 col-md-6 col-lg-4" >

                    @if (count($errors) > 0 && session('last_action') == 'report' )
                        <div class="alert alert-danger">
                            Ошибка!<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if($success)
                        <div class="alert alert-success">Сообщение отправлено</div>
                    @endif

                    <form class="noautocomplete inset " role="form" method="POST" action="{{ url('/report') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="what" value="{{ $what }}">
                        <input type="hidden" name="id" value="{{ $id }}">

                        <label>Введите текст сообщения, описывающий проблему</label>
                        <textarea class="form-control form-control-ofs" name="text" placeholder="Текст" rows="4">{{ old('text') }}</textarea>

                        <input type="submit" class="btn btn-lg btn-info btn-form" value="Отправить" />
                    </form>
                </div>
                <div class="col-sm-3 col-md-6 col-lg-8" >
                    <div class="centered">
                        <img class="file-preview" src="{{ url( $item->thumbUrl('mid')) }}" width="100px" />
                        <h3>{{ $what == 'usr' ? $item->real_name : $item->title }}</h3>
                    </div>
                </div>
            </div>

        @endif
    </div>

@stop
