@extends('app')

@section('title', 'Сообщения')

@section('content')

<div class="content-block">

	<ul id="usermessages" style="display:none;"></ul>

    <div class="preview_list">
        <div class="header">
            <h3>Сообщения</h3>
			&nbsp;&nbsp;
        </div>
        <hr/>
		
	<!--  <div class="msgdivider">Сегодня</div> -->
	  <ul class="msglist">
	  </ul>

	  <div class='centered'>
		<button id="showmore" style="display: none;" type="button" class="btn btn-form btn-lg btn-success">Показать еще</button>
	  </div>
		
    </div>
</div>

@stop
