@extends('app')

@section('title', 'Оплата завершена')

@section('content')

<div class="content-block">
    <p>Работа {{ $item->artwork->title }} оплачена.</p>
    <a type="button" href="#" class="btn btn-success">Готово</a>
</div>

@stop