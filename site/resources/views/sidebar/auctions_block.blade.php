<div id="auctions_block" class="ajax sideblock auctions_block">
    <a href="{{ url('/auctions') }}">
        <div class="header">
            <div class="icon"></div>
            <div class="title">Аукционы</div>
            <span class="pull-right num">{{ App\Repository::query('auc')->count() }}</span>
        </div>
    </a>
    <ul class="nav nav-pills nav-stacked hidden-xs">
        @foreach (App\Repository::query('auc')->orderBy('created_at', 'desc')->take(2)->get() as $item)
            @include('content.summary_block', ['item' => $item])
        @endforeach
    </ul>
    <div class="more hidden-xs"><a href="{{ url('/auctions') }}">показать все</a></div>
</div>