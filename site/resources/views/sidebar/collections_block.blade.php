<div id="collections_block" class="ajax sideblock collections_block">
    <a href="{{ url('/collection/list') }}">
        <div class="header">
            <div class="icon"></div>
            <div class="title">Коллекции</div>
            <span class="pull-right num">{{ App\Repository::query('col')->count() }}</span>
        </div>
    </a>
    <ul class="nav nav-pills nav-stacked hidden-xs">
        @foreach (App\Repository::query('col')->orderBy('created_at', 'desc')->take(2)->get() as $item)
        @include('content.summary_block', ['item' => $item])
        @endforeach
    </ul>
    <div class="more hidden-xs"><a href="{{ url('/collection/list') }}">показать все</a></div>
</div>