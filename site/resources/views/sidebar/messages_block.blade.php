@if (Auth::check())

<div id="messages_block" class="ajax sideblock messages_block">
    <a href="{{ url('messages') }}"><div class="header">
            <div class="icon"></div>
            <div class="title">Сообщения</div>
            <span class="pull-right num">{{ App\Repository::query('msg')->where('read', '=', 0)->count() }}</span>
        </div></a>
    <ul class="nav nav-pills nav-stacked hidden-xs">
        @foreach (App\Repository::query('msg')->where('read', '=', 0)->with('sender')->orderBy('date_sent', 'desc')->take(2)->get() as $item)
            @include('content.message_summary_block', ['item' => $item])
        @endforeach
    </ul>
    <div class="more hidden-xs"><a href="{{ url('messages') }}">показать все</a></div>
</div>

@endif