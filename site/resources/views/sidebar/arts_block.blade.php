<div id="arts_block" class="ajax sideblock arts_block">
    <a href="{{ url('/artwork/list') }}">
        <div class="header">
            <div class="icon"></div>
            <div class="title">Работы</div>
            <span class="pull-right num">{{ App\Repository::query('art')->count() }}</span>
        </div>
    </a>
    <ul class="nav nav-pills nav-stacked hidden-xs">
        @foreach (App\Repository::query('art')->orderBy('created_at', 'desc')->take(2)->get() as $item)
        @include('content.summary_block', ['item' => $item])
        @endforeach
    </ul>
    <div class="more hidden-xs"><a href="{{ url('/artwork/list') }}">показать все</a></div>
</div>