@extends('app')

@section('title', $item->title)

@section('css')
<link rel="stylesheet" media="screen" href="{{ asset('/css/show.css') }}">
@overwrite

@section('js')
<script src="{{ asset('/ext/jquery.fullscreen/jquery.fullscreen-min.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.core.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.utils.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.slider.js') }}"></script>
<script src="{{ asset('/ext/history/jquery.history.js') }}"></script>
<script src="{{ asset('/js/transitions.js') }}"></script>
<script src="{{ asset('/js/show.js') }}"></script>

<script type="text/javascript" src="{{ url('slideshow/params/'.$item->id) }}"></script> 
@overwrite

@section('main')

@if($is_owner)

<div id="background">
  <div id="sshow">

        <!-- Slides Container -->
        <div u="slides" style="position: absolute; left: 0px; top: 0px; width:100%; height:100%; overflow: hidden;">
		    @foreach ($item->slides()->with('artwork')->orderBy($item->randomize ? DB::raw('RAND()') : 'num')->get() as $i)
				<div data-art="{{ $i->artwork_id }}">
				  <div class="slide" style="background: {{ $i->artwork->empty_fill }} url({{ $i->artwork->originalUrl() }}) no-repeat center center;-webkit-background-size: contain;-moz-background-size: contain;background-size: contain;">
				  </div>
				  @if($i->showtitle)				  
				  <div u=caption t="*" class="title1" style="left:0px;top: 40px;width:800px;height:42px;">{{ $i->artwork->title }}</div>
				  @endif
				</div>
            @endforeach
        </div>

        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;display: none;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px;display: none;">
        </span>
        <!-- Arrow Navigator Skin End -->
  </div>

  <div id="controls" class="pict-fullscreen-bottom">

            <div class="ssshow-contollers fclose">
                 <a id="sclose" href="javascript:void(0);" class="glyphicon glyphicon-remove"></a>
            </div>

            <div class="ssshow-contollers fedit">
                 <a href="{{ $item->id > 0 ? url('slideshow/edit/'.$item->id) : url('slideshow/edit') }}" class="glyphicon glyphicon-cog"></a>
                 <p>Редактировать<br/></p>
            </div>

            <div id="nav" class="centered">
                <div class="ssshow-contollers">
                    <a id="prev_slide" href="#" class="glyphicon glyphicon-backward" title="Предыдущая"></a>
                    <p>Предыдущая </br>
                      <em>(Стрелка Влево)</em>
                    </p>
                </div> 
                <div class="ssshow-contollers">
                    <a id="pause_slide" href="#" class="glyphicon glyphicon-pause" title="Пауза / Просмотр"></a>
                    <p>Пауза / Просмотр</br>
                      <em>(Пробел)</em>
                    </p>
                </div>
                <div class="ssshow-contollers">
                     <a id="next_slide" href="#" class="glyphicon glyphicon-forward" title="Следующая"></a>
                     <p>Следующая</br>
                       <em>(Стрелка Право)</em>
                     </p>
                </div>
           </div>

                <div class="ssshow-contollers fmode">
                     <a href="#" id="fmode" class="glyphicon glyphicon-fullscreen"></a>
                     <p>Во весь экран</br>
                       <em>(Enter)</em>
                     </p>
                </div>

  </div>
</div>

@else
<h1 style="text-align:center;color:#fff;">Нельзя просматривать чужое слайдшоу!</h1>
@endif

@overwrite