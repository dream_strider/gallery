@extends('app')

@section('title', 'История аукционов')

@section('content')

<div class="content-block">
    <div class="preview_list">
        <div class="header">
            <h3>История аукционов работы {{ $art->title }}</h3>
        </div>
        <hr/>
        <div class="row">
            @foreach ($list as $item)
                @include('content.thumb_auction_history', ['item' => $item])
            @endforeach
        </div>
    </div>
</div>

@stop
