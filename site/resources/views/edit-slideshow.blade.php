@extends('app')

@section('title', 'Редактировать Слайдшоу')

@section('js')
@parent
<script src="{{ asset('/ext/jssor.slider/jssor.core.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.utils.js') }}"></script>
<script src="{{ asset('/ext/jssor.slider/jssor.slider.js') }}"></script>
<script src="{{ asset('/js/transitions.js') }}"></script>

<script type="text/javascript" src="{{ $item->id > 0 ? url('slideshow/params/'.$item->id) : url('slideshow/params') }}"></script>
@stop

@section('content')

<div class="form-block">

    <div class="rename">
        <input class="renamebox" type="text" name="rename" value="{{ $item->title }}" />
    </div>

	@if($success)
    <div id="success" class="alert alert-success">Слайдшоу успешно сохранено</div>
    @endif

	
    <div class="form-area">
        <div class="row">

            <div class="col-md-12 col-lg-8 inset form-preview">

                <ul id="sshow_images" class="sortable_list">
                    @foreach ($item->slides()->with('artwork')->get() as $i)
                        <li>
                            <div class="sssetup-item" data-rowid="show" data-itemid="{{ $i->artwork->id }}">
                                <div class="preview_pic"><img src="{{ $i->artwork->thumbUrl('mid') }}"></div>
                            </div>
                        </li>
                    @endforeach
                    <a id="trash" href="#" class="glyphicon glyphicon-trash" style="display: none;"> </a>
                </ul>

                <div class="sssetup-item addwork">
                    <img src="{{ asset('/img/add_work.png') }}">
                    <span>добавить работу</span>
                </div>

            </div>
            <div class="col-md-12 col-lg-4 form-content">

                <h2>Настройки появления</h2>
                <input type="hidden" name="showid" value="{{ $item->id }}"/>
                <ul>
                    <li class="sssetup-li">
                        <label>Появление слайда</label>
                        <select class="selectpicker" data-width="100%" title="Слайд" name="slide">
                            <option value="fadetwins">Плавная замена</option>
                            <option value="rotateoverlap">Замена с поворотом</option>
                            <option value="switch">Замена местами</option>
                            <option value="doors">Двери</option>
                            <option value="shifttb">Сдвиг вверх</option>
                            <option value="shiftlr">Сдвиг влево</option>
                            <option value="swingout">Мозаика появление</option>
                            <option value="swingin">Мозаика исчезание</option>
                            <option value="rotatezoom">Зум с поворотом</option>
                        </select>
                    </li>
                    <li class="sssetup-li">
                        <label>Появление названия</label>
                        <select class="selectpicker" data-width="100%" title="Название" name="caption">
                            <option value="fade">Плавное появление</option>
                            <option value="movel">Слева</option>
                            <option value="mover">Справа</option>
                            <option value="movet">Сверху</option>
                            <option value="moveb">Снизу</option>
                            <option value="moverl">Слева с поворотом</option>
                            <option value="moverr">Справа с поворотом</option>
                            <option value="clip">Развёртывание</option>
                            <option value="zoom">Зум</option>
                            <option value="zoomj">Зум по волне</option>
                            <option value="rot">Поворот</option>
                        </select>
                    </li>

                    <li class="sssetup-li">
                        <div style="position: relative; width: 200px; height: 210px; margin: 0 auto;" id="demo_slider">
                        </div>
                    </li>


                    <li class="sssetup-darker"><p>Время показа слайда</p>
                        <input id="timing" type="range" min="3" max="120"/>
                        <div class="sssetup-slower">быстрее</div>
                        <div class="sssetup-faster">медленнее</div></li>
                    <li class="sssetup-darker"><label>Показывать название</label>
                        <label class="onoff pull-right"><em>Нет</em><input type="checkbox" class="ios-switch" name="title" checked ><div></div><em>Да</em></label></li>
                    <li class="sssetup-darker" style="text-align: center;"> <a type="button" id="applytoall" href="#" class="btn btn-info">Применить к данному слайдшоу</a></li>
                    <li class="sssetup-darker"><label>Случайный порядок</label>
                        <label class="onoff pull-right"><em>Нет</em><input type="checkbox" class="ios-switch" name="random" off_value="0" value="1" {{ $item->randomize == 1 ? 'checked' : '' }}><div></div><em>Да</em></label></li>
                </ul>

                <div class="sssetup-box-wrapper" style="padding-bottom: 10px;">
                    <a href="{{ url('/slideshow/save') }}" class="btn btn-success">Сохранить</a>
                    <a href="{{ url('/slideshow/fullscreen') }}" class="btn btn-info">Просмотр</a>
                    @if(Session::has('edited.slideshow'))
                    <a class="btn btn-danger" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить слайдшоу?" data-action="{{ url('slideshow/delete/'.Session::get('edited.slideshow')) }}">УДАЛИТЬ</a>
                    @endif
                </div>

                <div u="slides" style="position: absolute; width: 200px; height: 200px;top:0px;left:0px;overflow:hidden;display: none;">
                    <div>
                        <img u="image" src="{{ asset('/img/l1.jpg') }}">
                        <div u=caption t="*" class="title1" style="left:60px;top: 50px;width:80px;height:20px;">Слайд 1</div>
                    </div>
                    <div>
                        <img u="image" src="{{ asset('/img/l8.jpg') }}">
                        <div u=caption t="*" class="title1" style="left:60px;top: 50px;width:80px;height:20px;">Слайд 2</div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="applied" tabindex="-1" role="dialog" aria-labelledby="appliedLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="appliedLabel">Подтверждение</h4>
            </div>
            <div class="modal-body">
                Настройки слайда применены ко всему слайдшоу
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="saved" tabindex="-1" role="dialog" aria-labelledby="appliedLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="appliedLabel">Подтверждение</h4>
            </div>
            <div class="modal-body">
                Изменения сохранены
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

@stop