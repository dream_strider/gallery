@extends('app')

@section('title', 'Профиль пользователя')

@section('content')

<div class="shaded-header">
 <div class="row">
   <div class="user-info col-md-12 col-lg-4">
     <a href="{{ url('/user/profile/'.$user->id) }}"><img class="border" src="{{ url($user->thumbUrl('small')) }}" width="58" height="58" /></a>
     <div class="name"><a href="{{ url('/user/profile/'.$user->id) }}">{{ $user->real_name }}</a></div>
     <div class="where">{{ $user->city.', '.$user->countryText }}</div>
   </div>
   <div class="user-menu col-md-12 col-lg-8">
	 @if($me)
     <a href="{{ url('user/edit') }}"><div class="edit"></div></a>
	 @endif
     @if($isModerator)
         <a href="{{ url('user/edit/'.$user->id) }}"><div class="edit"></div></a>
     @endif
     <ul>
         <li {{ $tab == 'art' ? "class=active" : "" }}><a href="{{ url('/user/profile/'.$user->id.'?tab=art') }}">Работы</a></li>
         <li {{ $tab == 'col' ? "class=active" : "" }}><a href="{{ url('/user/profile/'.$user->id.'?tab=col') }}">Коллекции</a></li>
         <li {{ $tab == 'auc' ? "class=active" : "" }}><a href="{{ url('/user/profile/'.$user->id.'?tab=auc') }}">Аукционы</a></li>
         @if($me)
             <li {{ $tab == 'show' ? "class=active" : "" }}><a href="{{ url('/user/profile/'.$user->id.'?tab=show') }}">Слайд-шоу</a></li>
             <li {{ $tab == 'offer' ? "class=active" : "" }}><a href="{{ url('/user/profile/'.$user->id.'?tab=offer') }}">Предложения</a></li>
         @endif
     </ul>
     @if(Auth::check())
     <a href="javascript:void(0);" data-toggle="modal" data-target="#confirmModal" data-confirm="Сообщить модератору?" data-action="{{ url('report/usr/'.$user->id) }}" class="glyphicon glyphicon-flag report pull-right" style="font-size: 1.0em; color: #ff0000;" title="Сообщить модератору"></a>
     @endif
   </div>
 </div>
</div>

<div class="content-block">

    @if($me && Auth::user()->greeting)
        @include('content.greeting')
    @endif

    @if (Auth::check() && Auth::user()->role == App\UserRole::Moderator && $user->id != Auth::user()->id)
        <a href="{{ url('/user/emulate/'.$user->id) }}">Зайти на сайт как этот пользователь</a>
    @endif


	@if($tab == '' || $tab == 'art')
		@include('content.profile_art_block', ['user_id' => $user->id, 'user_role' => $user->role, 'me' => $me, 'tab' => $tab])
	@endif

    @if($tab == '' || $tab == 'col')
        @include('content.profile_col_block', ['user_id' => $user->id, 'me' => $me, 'tab' => $tab])
    @endif

    @if($tab == '' || $tab == 'auc')
        @include('content.profile_auc_block', ['user_id' => $user->id, 'tab' => $tab])
    @endif

	@if(($tab == '' || $tab == 'show') && $me)
		@include('content.profile_show_block', ['user_id' => $user->id, 'me' => $me, 'tab' => $tab])
	@endif

    @if(($tab == '' || $tab == 'offer') && $me)
        @include('content.profile_offer_block', ['user_id' => $user->id, 'me' => $me, 'tab' => $tab])
    @endif
</div>

@stop
