@extends('app')

@section('title', 'Контакты')

@section('content')

    <div class="content-block">


        @if(Auth::check())

        <h2>Написать веб-мастеру</h2>

        <div class="row">
            <div class="col-sm-9 col-md-6 col-lg-4" >

                @if (count($errors) > 0 && session('last_action') == 'contacts/webmaster' )
                    <div class="alert alert-danger">
                        Ошибка!<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if($success)
                    <div class="alert alert-success">Сообщение отправлено</div>
                @endif

                <form class="noautocomplete inset " role="form" method="POST" action="{{ url('/contacts/webmaster') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <label>Тема</label>
                    <input type="text" class="form-control form-control-ofs" placeholder="Тема" name="subject" value="{{ old('subject') }}">

                    <label>Текст сообщения</label>
                    <textarea class="form-control form-control-ofs" name="text" placeholder="Текст" rows="4">{{ old('text') }}</textarea>

                    <input type="submit" class="btn btn-lg btn-info btn-form" value="Отправить" />
                </form>
            </div>
        </div>

        @endif
    </div>

@stop
