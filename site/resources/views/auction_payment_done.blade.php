@extends('app')

@section('title', 'Оплата завершена')

@section('content')

<div class="content-block">
    <p>{{ $item->title }} оплачен.</p>
    <a type="button" href="#" class="btn btn-success">Готово</a>
</div>

@stop