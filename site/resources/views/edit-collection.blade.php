@extends('app')

@section('title', 'Редактировать Коллекцию')

@section('content')

<div id="col-edit" class="form-block">
    <h2>Редактирование Коллекции</h2>

    @if($success)
    <div class="alert alert-success">Коллекция успешно сохранена</div>
    @endif

    @if (count($errors) > 0 && session('last_action') == 'collection/save' )
    <div class="alert alert-danger">
        Возникли ошибки при сохранении коллекции:
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="form-area">
        <div class="row">
            <div class="col-md-12 col-lg-3 inset form-preview">

                <div class="centered">
                    <a href="#" data-rowid="col" data-itemid="{{ $item->id }}" class="select_thumb">
                        <img class="file-preview" src="{{ url( $item->thumbUrl('mid')) }}" width="100%" />
                    </a>
                    <h3>{{ $isNew ? 'Новая коллекция' : $item->title }}</h3>
                    <p class="text">{{ str_limit($item->description, 80, '...') }}</p>
                </div>

            </div>
            <div class="col-md-12 col-lg-9 form-content">
                <div class="col-md-5 form-menu">

                    <ul role="form-menu">
                        <li data-option="col-edit-desc" {{ $opt == 'col-edit-desc' || $opt == '' ? 'class=active' : '' }}>Название и Описание <span></span></li>
                        <li data-option="col-edit-opt" {{ $opt == 'col-edit-opt' ? 'class=active' : '' }}>Опции</li>

                        @if(!$isNew)
                        <li data-option="col-edit-del" {{ $opt == 'col-edit-del' ? 'class=active' : '' }}>Удаление коллекции</li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-7 form-options">
                    <form class="noautocomplete" id="col-edit-form" action="{{ url('collection/save') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <input type="hidden" name="opt" value="{{ $opt }}">

                        <section id="col-edit-desc" class="form-screen inset form-options-1" {{ $opt != 'col-edit-desc' && $opt != '' ? 'style=display:none;' : '' }}>
                            <input type="text" class="form-control" placeholder="Введите название коллекции" name="title" value="{{ old('title', $item->title) }}" >
                            <hr/>
                            <textarea class="form-control" placeholder="Введите описание" rows="12" name="description">{{ old('description', $item->description) }}</textarea>
                        </section>

                        <section id="col-edit-opt" class="form-screen inset form-options-2" {{ $opt != 'col-edit-opt' ? 'style=display:none;' : '' }}>
                            <h3>Опции</h3>
                            <br/><br/>
                            <label>Запретить Просмотр</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="visibility" off_value="0" value="2" {{ old('visibility', $item->visibility) == 2 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Разрешить Комментарии</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="allow_comments" off_value="0" value="1" {{ old('allow_comments', $item->allow_comments) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                            <label>Показывать рекламу</label>
                            <label class="onoff pull-right">Нет<input type="checkbox" class="ios-switch" name="allow_ad" off_value="0" value="1" {{ old('allow_ad', $item->allow_ad) == 1 ? 'checked' : '' }} ><div></div>Да</label>
                            <br/><br/>
                        </section>

                        @if(!$isNew)
                        <section id="col-edit-del" class="form-screen inset form-options-3" {{ $opt != 'col-edit-del' ? 'style=display:none;' : '' }}>
                            @if(!$item->default)
                            <button type="button" class="btn btn-form btn-lg btn-danger" data-toggle="modal" data-target="#confirmModal" data-confirm="Удалить коллекцию?" data-action="{{ url('collection/delete/'.$item->id) }}">УДАЛИТЬ</button>
                            @else
                            <span>Нельзя удалить коллекцию по умолчанию</span>
                            @endif
                        </section>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
    <button type="button" data-form="col-edit-form" class="btn btn-form btn-lg btn-success">ГОТОВО</button>
    <a type="button" href="{{ url('/') }}" class="btn btn-form btn-lg btn-default">ОТМЕНА</a>
</div>

@stop