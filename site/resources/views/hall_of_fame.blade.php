@extends('app')

@section('title', 'Зал славы')

@section('content')

<div class="shaded-header">
    <h3 style="margin-top: 0;">Зал Славы</h3>
</div>

<div class="content-block">
	
<div class="well">	
	<div class="row">
		<div class="col-md-6 col-lg-8">
			@include('content.hall_total')
		</div>
		<div class="col-sm-12 col-md-6 col-lg-4">
			@if($crit != 'all') 
			<div id="famesearch" class="input-group">
			  <input type="text" class="form-control" placeholder="Поиск по имени/произведению" value="{{ $search }}" />
			  <span class="input-group-btn">
				<button class="btn btn-default" type="button">Искать</button>
			  </span>
			</div>
			@endif
		</div>
	</div>
</div>

    @if($crit == 'all' || $crit == 'artviews')
    <div class="content-block">
        <div class="preview_list">
            <div class="header">
                <h3>Владельцы самых просматриваемых работ</h3>
                @if($crit == 'all') <a href="{{ url('/hall-of-fame/artviews') }}">показать всех</a>&nbsp;&nbsp; @endif
            </div>
            <hr/>
            @if($crit == 'artviews') <input id='total' type='hidden' value='{{ App\Repository::query('hfartviews', $search)->count() }}' /> @endif
            
			@include('content.hall_artviews', ['crit' => $crit, 'search' => $search])

            @if($crit == 'artviews')
            <div class='centered'>
                <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="hfartviews" data-ajax-search="{{ $search }}">Показать еще</button>
            </div>
            @endif
        </div>
    </div>
    @endif

    @if($crit == 'all' || $crit == 'colviews')
    <div class="content-block">
        <div class="preview_list">
            <div class="header">
                <h3>Владельцы самых просматриваемых коллекций</h3>
                @if($crit == 'all') <a href="{{ url('/hall-of-fame/colviews') }}">показать всех</a>&nbsp;&nbsp; @endif
            </div>
            <hr/>
            @if($crit == 'colviews') <input id='total' type='hidden' value='{{ App\Repository::query('hfcolviews', $search)->count() }}' /> @endif

			@include('content.hall_colviews', ['crit' => $crit, 'search' => $search])

            @if($crit == 'colviews')
            <div class='centered'>
                <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="hfcolviews" data-ajax-search="{{ $search }}">Показать еще</button>
            </div>
            @endif
        </div>
    </div>
    @endif

    @if($crit == 'all' || $crit == 'expensive')
    <div class="content-block">
        <div class="preview_list">
            <div class="header">
                <h3>Самые дорогие работы</h3>
                @if($crit == 'all') <a href="{{ url('/hall-of-fame/expensive') }}">показать все</a>&nbsp;&nbsp; @endif
            </div>
            <hr/>
            @if($crit == 'expensive') <input id='total' type='hidden' value='{{ App\Repository::query('hfexpensive', $search)->count() }}' /> @endif
            
			@include('content.hall_expensive', ['crit' => $crit, 'search' => $search])

            @if($crit == 'expensive')
            <div class='centered'>
                <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="hfexpensive" data-ajax-search="{{ $search }}">Показать еще</button>
            </div>
            @endif
        </div>
    </div>
    @endif

    @if($crit == 'all' || $crit == 'purchase')
    <div class="content-block">
        <div class="preview_list">
            <div class="header">
                <h3>Пользователи, купившие больше всего работ</h3>
                @if($crit == 'all') <a href="{{ url('/hall-of-fame/purchase') }}">показать всех</a>&nbsp;&nbsp; @endif
            </div>
            <hr/>
            @if($crit == 'purchase') <input id='total' type='hidden' value='{{ App\Repository::query('hfpurchase', $search)->count() }}' /> @endif

			@include('content.hall_purchase', ['crit' => $crit, 'search' => $search])

            @if($crit == 'purchase')
            <div class='centered'>
                <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="hfpurchase" data-ajax-search="{{ $search }}">Показать еще</button>
            </div>
            @endif
        </div>
    </div>
    @endif

    @if($crit == 'all' || $crit == 'sell')
    <div class="content-block">
        <div class="preview_list">
            <div class="header">
                <h3>Пользователи, продавшие больше всего работ</h3>
                @if($crit == 'all') <a href="{{ url('/hall-of-fame/sell') }}">показать всех</a>&nbsp;&nbsp; @endif
            </div>
            <hr/>
            @if($crit == 'sell') <input id='total' type='hidden' value='{{ App\Repository::query('hfsell', $search)->count() }}' /> @endif
            
			@include('content.hall_sell', ['crit' => $crit, 'search' => $search])

            @if($crit == 'sell')
            <div class='centered'>
                <button style="display: none;" type="button" class="btn btn-form btn-lg btn-success" data-showmore="hfsell" data-ajax-search="{{ $search }}">Показать еще</button>
            </div>
            @endif
        </div>
    </div>
    @endif
</div>

@stop