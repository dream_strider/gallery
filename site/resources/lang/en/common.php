<?php

return [
	'days_short' => 'd',
	'hours_short' => 'h',
	'minutes_short' => 'm',

    'role' =>
    [
        '1' => 'Author',
        '2' => 'Collector',
        '3' => 'Moderator',
    ]
];