<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "Пароль должен быть как минимум 3 символа и должен быть подтверждён.",
    "user" => "Пользователь с данным адресом электронной почты не найден",
    "token" => "Ссылка на сброс пароля не действительна. Запросите новую",
    "activation_sent" => "Ссылка на активацию отправлена на ваш почтовый адрес!",
    "sent" => "Ссылка на сброс пароля отправлена на ваш почтовый адрес!",
    "reset" => "Пароль изменён!",

];
