<?php

return [
	'days_short' => 'д',
	'hours_short' => 'ч',
	'minutes_short' => 'м',

    'role' =>
    [
        '1' => 'Автор',
        '2' => 'Коллекционер',
        '3' => 'Модератор',
    ]
];