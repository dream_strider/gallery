<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Size of page when querying items
	|--------------------------------------------------------------------------
	|
	| Usually applied when 'Show More' button is in effect
	|
	*/

	'query_page_size' => 24,

	/*
	|--------------------------------------------------------------------------
	| Period in days to remove abusive content
	|--------------------------------------------------------------------------
	|
	| Users are warned and obliged to remove abusive content during this period.
	| This parameter affects on time of urgent requests are sent to moderators
	| to perform action on remove abusive content
	|
	*/

	'abusive_content_remove_hours' => 48,
];