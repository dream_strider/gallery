<?php

use Illuminate\Database\Migrations\Migration;

class GalleryInitial extends Migration {

	private function commonFields($table)
	{
		$table->increments('id');
		$table->string('title', 255)->index();
		$table->text('description')->nullable();
		$table->tinyInteger('visibility')->default(2); // 0 - all, 1 - registered, 2 - private
		$table->boolean('allow_comments')->default(1);
		$table->integer('view_count')->default(0);

		$table->integer('created_by')->unsigned();
		$table->integer('edited_by')->unsigned();
		$table->timestamps();
		$table->softDeletes();

		$table->foreign('created_by')->references('id')->on('users');
		$table->foreign('edited_by')->references('id')->on('users');
	}

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->softDeletes();
            $table->string('activation_code',64);
            $table->tinyInteger('active')->default(0);
            $table->string('real_name', 255)->index();
            $table->tinyInteger('role')->index();    // 1 - Author, 2 - Collector, 3 - Moderator
            $table->date('birthday');
            $table->tinyInteger('gender')->default(0);  // 0 - Unknown, 1 - Male, 2 - Female
            $table->integer('country');
            $table->string('city', 255);
            $table->string('web', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->text('description');
            $table->json('devices')->nullable();
            $table->json('exhibitions')->nullable();
            $table->boolean('greeting')->default(1);
            $table->integer('sell_count')->default(0);
            $table->integer('purchase_count')->default(0);
            $table->string('pay_method', 64)->nullable();
		});

        Schema::create('messages', function($table)
        {
			$table->increments('id');
            $table->string('subject', 255);
            $table->text('text');
			$table->integer('sender_id')->unsigned()->nullable();
			$table->integer('recipient_id')->unsigned();
			$table->dateTime('date_sent');
            $table->boolean('read')->default(0);

			$table->foreign('sender_id')->references('id')->on('users');
			$table->foreign('recipient_id')->references('id')->on('users');
        });

        Schema::create('comments', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('thread', 255);
            $table->boolean('approved')->default(0);
            $table->text('text');
            $table->integer('sender_id')->unsigned()->nullable();

            $table->index('thread');
            $table->foreign('sender_id')->references('id')->on('users');
        });

        Schema::create('visits', function($table)
        {
            $table->integer('ip')->unsigned();
            $table->string('res', 8);
			$table->integer('res_id')->unsigned();
			
			$table->primary(['ip', 'res', 'res_id']);
        });

        Schema::create('lang_resources', function($table)
        {
			$table->string('lang', 2);
			$table->integer('resource_type')->unsigned();
			$table->integer('resource_id')->unsigned();
			$table->string('value', 512);
			$table->primary(['lang', 'resource_type', 'resource_id']);
        });

        Schema::create('collections', function($table)
        {
            $this->commonFields($table);

            $table->boolean('default')->default(0);
            $table->boolean('allow_ad')->default(1);
        });

        Schema::create('artworks', function($table)
        {
			$this->commonFields($table);

			$table->string('filename', 255)->nullable();
			$table->integer('art_technic')->unsigned();	// resource_type = 2
			$table->integer('genre')->unsigned();		// resource_type = 3
			$table->boolean('buy_possible')->default(0);
			$table->boolean('rent_possible')->default(0);
            $table->integer('buy_price')->nullable();
            $table->integer('rent_price')->nullable();
			$table->integer('release_date')->nullable();
			$table->date('rented_until')->nullable();
			$table->string('empty_fill', 7)->default("#000");
			$table->integer('height');	
			$table->integer('width');	
			$table->tinyInteger('units')->default(0); // 0 - px, 1 - cm
            $table->integer('device')->unsigned()->nullable();
			$table->boolean('auction_history')->default(1);
						
			$table->integer('owner_id')->unsigned();
			$table->integer('prev_owner_id')->unsigned()->nullable();
            $table->integer('collection_id')->unsigned();

			$table->foreign('owner_id')->references('id')->on('users');
			$table->foreign('prev_owner_id')->references('id')->on('users');
            $table->foreign('collection_id')->references('id')->on('collections');
        });

        Schema::create('auctions', function($table)
        {
            $this->commonFields($table);

            $table->integer('artwork_id')->unsigned();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('repeat_possible')->default(0);
            $table->boolean('payment_done')->default(0);
            $table->integer('initial_stake')->default(100);
            $table->integer('stake_step')->default(50);
            $table->tinyInteger('prev_status')->nullable();
            $table->integer('rent_period')->default(0);
            $table->tinyInteger('sell_conditions')->default(0); // 0 - exclusive, 1 - nonexclusive
            $table->tinyInteger('sell_type')->default(0);   // 0 - sell, 1 - rent
            $table->integer('winner_id')->unsigned()->nullable();
            $table->integer('final_stake')->nullable();
            $table->integer('final_stake_date')->dateTime()->nullable();

            $table->foreign('winner_id')->references('id')->on('users');
            $table->foreign('artwork_id')->references('id')->on('artworks');
        });

        Schema::create('bids', function($table)
        {
            $table->increments('id');
            $table->integer('auction_id')->unsigned();
            $table->integer('stake');
            $table->integer('bidder_id')->unsigned();
            $table->text('comment');

            $table->foreign('bidder_id')->references('id')->on('users');
            $table->foreign('auction_id')->references('id')->on('auctions');
        });

        Schema::create('slideshows', function($table)
        {
            $this->commonFields($table);
			$table->boolean('new')->default(0); 
			$table->boolean('randomize')->default(0); 
        });

        Schema::create('slides', function($table)
        {
            $table->increments('id');
            $table->tinyInteger('num');
            $table->integer('slideshow_id')->unsigned();
            $table->integer('artwork_id')->unsigned();
			$table->boolean('showtitle')->default(1); 
            $table->integer('delay')->default(5);
            $table->string('image_effect', 16)->default('fadetwins');
            $table->string('caption_effect', 16)->default('movel');

            $table->foreign('slideshow_id')->references('id')->on('slideshows');
            $table->foreign('artwork_id')->references('id')->on('artworks');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            if (Schema::hasColumn('users', 'deleted_at'))
                $table->dropColumn('deleted_at');
			if (Schema::hasColumn('users', 'activation_code'))
				$table->dropColumn('activation_code');
			if (Schema::hasColumn('users', 'active'))
				$table->dropColumn('active');
			if (Schema::hasColumn('users', 'real_name'))
				$table->dropColumn('real_name');
			if (Schema::hasColumn('users', 'role'))
				$table->dropColumn('role');
			if (Schema::hasColumn('users', 'birthday'))
				$table->dropColumn('birthday');
			if (Schema::hasColumn('users', 'gender'))
				$table->dropColumn('gender');
			if (Schema::hasColumn('users', 'country'))
				$table->dropColumn('country');
			if (Schema::hasColumn('users', 'city'))
				$table->dropColumn('city');
			if (Schema::hasColumn('users', 'web'))
				$table->dropColumn('web');
			if (Schema::hasColumn('users', 'address'))
				$table->dropColumn('address');
			if (Schema::hasColumn('users', 'description'))
				$table->dropColumn('description');
			if (Schema::hasColumn('users', 'devices'))
				$table->dropColumn('devices');
			if (Schema::hasColumn('users', 'exhibitions'))
				$table->dropColumn('exhibitions');
            if (Schema::hasColumn('users', 'greeting'))
                $table->dropColumn('greeting');
            if (Schema::hasColumn('users', 'sell_count'))
                $table->dropColumn('sell_count');
            if (Schema::hasColumn('users', 'purchase_count'))
                $table->dropColumn('purchase_count');
        });

        Schema::dropIfExists('comments');
        Schema::dropIfExists('messages');
		Schema::dropIfExists('visits');		
        Schema::dropIfExists('slides');
        Schema::dropIfExists('slideshows');
        Schema::dropIfExists('bids');
        Schema::dropIfExists('auctions');
        Schema::dropIfExists('artworks');
        Schema::dropIfExists('collections');
        Schema::dropIfExists('lang_resources');
    }
}
