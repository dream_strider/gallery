<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReplyToMessages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('messages', function($table)
		{
			$table->integer('reply')->unsigned()->nullable();
			$table->tinyInteger('service')->unsigned()->nullable();
			$table->integer('recipient_id')->unsigned()->nullable()->change();
			$table->dropColumn('read');
		});

		Schema::create('message_masks', function($table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('message_id');
			$table->boolean('read')->default(0);

			$table->unique(['user_id', 'message_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('messages', function($table)
		{
			if (Schema::hasColumn('messages', 'reply'))
				$table->dropColumn('reply');
			if (Schema::hasColumn('messages', 'service'))
				$table->dropColumn('service');
			$table->integer('recipient_id')->unsigned()->change();
		});

		Schema::dropIfExists('message_masks');
	}

}
