<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers', function($table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('buyer_id')->unsigned();
			$table->integer('artwork_id')->unsigned();
			$table->integer('price');
			$table->integer('rent_period')->nullable();
			$table->boolean('accepted_buyer')->default(0);
			$table->boolean('accepted_owner')->default(0);

			$table->foreign('buyer_id')->references('id')->on('users');
			$table->foreign('artwork_id')->references('id')->on('artworks');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers');
	}

}
