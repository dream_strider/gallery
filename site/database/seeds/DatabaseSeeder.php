<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\UserRole;
use App\ResourceType;
use App\Visibility;
use App\AuctionStatus;

use App\Message;
use App\Comment;
use App\User;
use App\Artwork;
use App\Auction;
use App\Bid;
use App\Collection;
use App\LangResource;
use App\Slide;
use App\Slideshow;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $this->call('GalleryInitialization');
    }

}

class GalleryInitialization extends Seeder
{
    private function clear_dir($dir, $top=true)
    {
        if (!is_dir($dir) || is_link($dir)) return unlink($dir);
        foreach (scandir($dir) as $file)
        {
            if ($file == '.' || $file == '..') continue;
            $this->clear_dir($dir . DIRECTORY_SEPARATOR . $file, false);
        }
        if(!$top)
            rmdir($dir);
    }

    public function run()
    {
        Model::unguard();

        DB::table('lang_resources')->delete();
        DB::table('slides')->delete();
        DB::table('slideshows')->delete();
        DB::table('bids')->delete();
        DB::table('auctions')->delete();
        DB::table('artworks')->delete();
        DB::table('collections')->delete();
        DB::table('messages')->delete();
        DB::table('comments')->delete();
        DB::table('users')->delete();

        $this->clear_dir(storage_path().'/app/content/artworks');
        $this->clear_dir(storage_path().'/app/content/thumbs/user');
        $this->clear_dir(storage_path().'/app/content/thumbs/art');
        $this->clear_dir(storage_path().'/app/content/thumbs/col');
        $this->clear_dir(storage_path().'/app/content/thumbs/show');
        $this->clear_dir(storage_path().'/app/content/user');

        $cntr_json = '[{"id":19,"title":"Австралия"},{"id":20,"title":"Австрия"},{"id":5,"title":"Азербайджан"},{"id":21,"title":"Албания"},{"id":22,"title":"Алжир"},{"id":23,"title":"Американское Самоа"},{"id":24,"title":"Ангилья"},{"id":25,"title":"Ангола"},{"id":26,"title":"Андорра"},{"id":27,"title":"Антигуа и Барбуда"},{"id":28,"title":"Аргентина"},{"id":6,"title":"Армения"},{"id":29,"title":"Аруба"},{"id":30,"title":"Афганистан"},{"id":31,"title":"Багамы"},{"id":32,"title":"Бангладеш"},{"id":33,"title":"Барбадос"},{"id":34,"title":"Бахрейн"},{"id":3,"title":"Беларусь"},{"id":35,"title":"Белиз"},{"id":36,"title":"Бельгия"},{"id":37,"title":"Бенин"},{"id":38,"title":"Бермуды"},{"id":39,"title":"Болгария"},{"id":40,"title":"Боливия"},{"id":235,"title":"Бонайре, Синт-Эстатиус и Саба"},{"id":41,"title":"Босния и Герцеговина"},{"id":42,"title":"Ботсвана"},{"id":43,"title":"Бразилия"},{"id":44,"title":"Бруней-Даруссалам"},{"id":45,"title":"Буркина-Фасо"},{"id":46,"title":"Бурунди"},{"id":47,"title":"Бутан"},{"id":48,"title":"Вануату"},{"id":233,"title":"Ватикан"},{"id":49,"title":"Великобритания"},{"id":50,"title":"Венгрия"},{"id":51,"title":"Венесуэла"},{"id":52,"title":"Виргинские острова, Британские"},{"id":53,"title":"Виргинские острова, США"},{"id":54,"title":"Восточный Тимор"},{"id":55,"title":"Вьетнам"},{"id":56,"title":"Габон"},{"id":57,"title":"Гаити"},{"id":58,"title":"Гайана"},{"id":59,"title":"Гамбия"},{"id":60,"title":"Гана"},{"id":61,"title":"Гваделупа"},{"id":62,"title":"Гватемала"},{"id":63,"title":"Гвинея"},{"id":64,"title":"Гвинея-Бисау"},{"id":65,"title":"Германия"},{"id":66,"title":"Гибралтар"},{"id":67,"title":"Гондурас"},{"id":68,"title":"Гонконг"},{"id":69,"title":"Гренада"},{"id":70,"title":"Гренландия"},{"id":71,"title":"Греция"},{"id":7,"title":"Грузия"},{"id":72,"title":"Гуам"},{"id":73,"title":"Дания"},{"id":231,"title":"Джибути"},{"id":74,"title":"Доминика"},{"id":75,"title":"Доминиканская Республика"},{"id":76,"title":"Египет"},{"id":77,"title":"Замбия"},{"id":78,"title":"Западная Сахара"},{"id":79,"title":"Зимбабве"},{"id":8,"title":"Израиль"},{"id":80,"title":"Индия"},{"id":81,"title":"Индонезия"},{"id":82,"title":"Иордания"},{"id":83,"title":"Ирак"},{"id":84,"title":"Иран"},{"id":85,"title":"Ирландия"},{"id":86,"title":"Исландия"},{"id":87,"title":"Испания"},{"id":88,"title":"Италия"},{"id":89,"title":"Йемен"},{"id":90,"title":"Кабо-Верде"},{"id":4,"title":"Казахстан"},{"id":91,"title":"Камбоджа"},{"id":92,"title":"Камерун"},{"id":10,"title":"Канада"},{"id":93,"title":"Катар"},{"id":94,"title":"Кения"},{"id":95,"title":"Кипр"},{"id":96,"title":"Кирибати"},{"id":97,"title":"Китай"},{"id":98,"title":"Колумбия"},{"id":99,"title":"Коморы"},{"id":100,"title":"Конго"},{"id":101,"title":"Конго, демократическая республика"},{"id":102,"title":"Коста-Рика"},{"id":103,"title":"Кот д`Ивуар"},{"id":104,"title":"Куба"},{"id":105,"title":"Кувейт"},{"id":11,"title":"Кыргызстан"},{"id":138,"title":"Кюрасао"},{"id":106,"title":"Лаос"},{"id":12,"title":"Латвия"},{"id":107,"title":"Лесото"},{"id":108,"title":"Либерия"},{"id":109,"title":"Ливан"},{"id":110,"title":"Ливия"},{"id":13,"title":"Литва"},{"id":111,"title":"Лихтенштейн"},{"id":112,"title":"Люксембург"},{"id":113,"title":"Маврикий"},{"id":114,"title":"Мавритания"},{"id":115,"title":"Мадагаскар"},{"id":116,"title":"Макао"},{"id":117,"title":"Македония"},{"id":118,"title":"Малави"},{"id":119,"title":"Малайзия"},{"id":120,"title":"Мали"},{"id":121,"title":"Мальдивы"},{"id":122,"title":"Мальта"},{"id":123,"title":"Марокко"},{"id":124,"title":"Мартиника"},{"id":125,"title":"Маршалловы Острова"},{"id":126,"title":"Мексика"},{"id":127,"title":"Микронезия, федеративные штаты"},{"id":128,"title":"Мозамбик"},{"id":15,"title":"Молдова"},{"id":129,"title":"Монако"},{"id":130,"title":"Монголия"},{"id":131,"title":"Монтсеррат"},{"id":132,"title":"Мьянма"},{"id":133,"title":"Намибия"},{"id":134,"title":"Науру"},{"id":135,"title":"Непал"},{"id":136,"title":"Нигер"},{"id":137,"title":"Нигерия"},{"id":139,"title":"Нидерланды"},{"id":140,"title":"Никарагуа"},{"id":141,"title":"Ниуэ"},{"id":142,"title":"Новая Зеландия"},{"id":143,"title":"Новая Каледония"},{"id":144,"title":"Норвегия"},{"id":145,"title":"Объединенные Арабские Эмираты"},{"id":146,"title":"Оман"},{"id":147,"title":"Остров Мэн"},{"id":148,"title":"Остров Норфолк"},{"id":149,"title":"Острова Кайман"},{"id":150,"title":"Острова Кука"},{"id":151,"title":"Острова Теркс и Кайкос"},{"id":152,"title":"Пакистан"},{"id":153,"title":"Палау"},{"id":154,"title":"Палестинская автономия"},{"id":155,"title":"Панама"},{"id":156,"title":"Папуа - Новая Гвинея"},{"id":157,"title":"Парагвай"},{"id":158,"title":"Перу"},{"id":159,"title":"Питкерн"},{"id":160,"title":"Польша"},{"id":161,"title":"Португалия"},{"id":162,"title":"Пуэрто-Рико"},{"id":163,"title":"Реюньон"},{"id":1,"title":"Россия"},{"id":164,"title":"Руанда"},{"id":165,"title":"Румыния"},{"id":9,"title":"США"},{"id":166,"title":"Сальвадор"},{"id":167,"title":"Самоа"},{"id":168,"title":"Сан-Марино"},{"id":169,"title":"Сан-Томе и Принсипи"},{"id":170,"title":"Саудовская Аравия"},{"id":171,"title":"Свазиленд"},{"id":172,"title":"Святая Елена"},{"id":173,"title":"Северная Корея"},{"id":174,"title":"Северные Марианские острова"},{"id":175,"title":"Сейшелы"},{"id":176,"title":"Сенегал"},{"id":177,"title":"Сент-Винсент"},{"id":178,"title":"Сент-Китс и Невис"},{"id":179,"title":"Сент-Люсия"},{"id":180,"title":"Сент-Пьер и Микелон"},{"id":181,"title":"Сербия"},{"id":182,"title":"Сингапур"},{"id":234,"title":"Синт-Мартен"},{"id":183,"title":"Сирийская Арабская Республика"},{"id":184,"title":"Словакия"},{"id":185,"title":"Словения"},{"id":186,"title":"Соломоновы Острова"},{"id":187,"title":"Сомали"},{"id":188,"title":"Судан"},{"id":189,"title":"Суринам"},{"id":190,"title":"Сьерра-Леоне"},{"id":16,"title":"Таджикистан"},{"id":191,"title":"Таиланд"},{"id":192,"title":"Тайвань"},{"id":193,"title":"Танзания"},{"id":194,"title":"Того"},{"id":195,"title":"Токелау"},{"id":196,"title":"Тонга"},{"id":197,"title":"Тринидад и Тобаго"},{"id":198,"title":"Тувалу"},{"id":199,"title":"Тунис"},{"id":17,"title":"Туркменистан"},{"id":200,"title":"Турция"},{"id":201,"title":"Уганда"},{"id":18,"title":"Узбекистан"},{"id":2,"title":"Украина"},{"id":202,"title":"Уоллис и Футуна"},{"id":203,"title":"Уругвай"},{"id":204,"title":"Фарерские острова"},{"id":205,"title":"Фиджи"},{"id":206,"title":"Филиппины"},{"id":207,"title":"Финляндия"},{"id":208,"title":"Фолклендские острова"},{"id":209,"title":"Франция"},{"id":210,"title":"Французская Гвиана"},{"id":211,"title":"Французская Полинезия"},{"id":212,"title":"Хорватия"},{"id":213,"title":"Центрально-Африканская Республика"},{"id":214,"title":"Чад"},{"id":230,"title":"Черногория"},{"id":215,"title":"Чехия"},{"id":216,"title":"Чили"},{"id":217,"title":"Швейцария"},{"id":218,"title":"Швеция"},{"id":219,"title":"Шпицберген и Ян Майен"},{"id":220,"title":"Шри-Ланка"},{"id":221,"title":"Эквадор"},{"id":222,"title":"Экваториальная Гвинея"},{"id":223,"title":"Эритрея"},{"id":14,"title":"Эстония"},{"id":224,"title":"Эфиопия"},{"id":226,"title":"Южная Корея"},{"id":227,"title":"Южно-Африканская Республика"},{"id":232,"title":"Южный Судан"},{"id":228,"title":"Ямайка"},{"id":229,"title":"Япония"}]';
        $countries = json_decode($cntr_json);

        foreach($countries as $country)
        {
            LangResource::create(
            [
                'lang' => 'ru',
                'resource_type' => ResourceType::Country,
                'resource_id' => $country->id,
                'value' => $country->title
            ]);
        }

        $genres = array();
        $genre_titles = array("Портрет","Анимализм","Батальный","Бытовой жанр","Городской пейзаж","Интерьер","Исторический","Мифологический","Морской пейзаж","Натюрморт","Пейзаж","Религиозный","Фэнтэзи","Ню (для взрослых)");

        $i=1;
        foreach($genre_titles as $genre)
        {
            $obj = LangResource::create(
                [
                    'lang' => 'ru',
                    'resource_type' => ResourceType::Genre,
                    'resource_id' => $i++,
                    'value' => $genre
                ]);
            $genres[] = $obj->resource_id;
        }

        $technics = array();
        $technic_titles = array("Живопись","Графика","Скульптура","Декоративно-прикладное искусство","Фотография","Компьютерная графика");

        $i=1;
        foreach($technic_titles as $technic)
        {
            $obj = LangResource::create(
                [
                    'lang' => 'ru',
                    'resource_type' => ResourceType::Technic,
                    'resource_id' => $i++,
                    'value' => $technic
                ]);
            $technics[] = $obj->resource_id;
        }

        $devices = array();
        $device_titles = array("Смартфон 4 дюйма и меньше","Смартфон 5 дюймов","Планшет 7 дюймов","Планшет 10 дюймов","Планшет 12 дюймов","LCD 15 дюймов","LCD 17 дюймов","LCD 21 дюйм","LCD 24 дюйма","LCD 30 дюймов","LCD 40 дюймов","LCD 50 дюймов","LCD 80 дюймов","LCD 100 дюймов и более");

        $i=1;
        foreach($device_titles as $device)
        {
            $obj = LangResource::create(
                [
                    'lang' => 'ru',
                    'resource_type' => ResourceType::Device,
                    'resource_id' => $i++,
                    'value' => $device
                ]);
            $devices[] = $obj->resource_id;
        }

        User::create(
        [
            'activation_code' => '',
            'active' => 1,
            'name' => "admin",
            'real_name' => 'admin',
            'role' => UserRole::Webmaster,
            'email' => 'admin@gallery.com',
            'password' => bcrypt('aaabbbccc'),
            'birthday' => '',
            'gender' => 0,
            'country' => 1,
            'web' => '',
            'city' => '',
            'address' => '',
            'description' => '',
        ]);
    }
}



class GalleryTestSeeder extends Seeder
{
    public function spawnComments($thread)
    {
        for($i=0; $i<rand(0,15); $i++)
        {
            Comment::create(
            [
                'thread' => $thread,
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'approved' => 1,
                'sender_id' => $this->moder[rand(0, count($this->moder) - 1)]
            ]);
        }
    }

    public function spawnModerators()
    {
        $this->moder = array();
        for($i = 0; $i < 5; $i++)
        {
            $name = 'moderator'.$i;
            if($i == 0)
                $name = "moderator";

            $us = User::create(
            [
                'activation_code' => '',
                'active' => 1,
                'name' => $name,
                'real_name' => 'Moderator '.$i,
                'role' => UserRole::Moderator,
                'email' => 'the_virt@pisem.net',
                'password' => bcrypt('123'),
                'birthday' => "2001-04-16",
                'gender' => rand(0,2),
                'country' => $this->countries[rand(0, count($this->countries) - 1)],
                'web' => 'http://aa.com',
                'city' => 'City',
                'address' => 'Some address',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            ]);
            $this->moder[] = $us->id;
            $us->generateThumbs(storage_path().'/app/seeding/ava'.rand(1,4).'.jpg');
        }
    }

    public function spawnAuthor($i, $name)
    {
        $us = User::create(
            [
                'activation_code' => '',
                'active' => 1,
                'name' => $name,
                'real_name' => 'Author '.$i,
                'role' => UserRole::Author,
                'email' => 'the_virt@pisem.net',
                'password' => bcrypt('123'),
                'birthday' => "2001-04-16",
                'gender' => rand(0,2),
                'country' => $this->countries[rand(0, count($this->countries) - 1)],
                'web' => 'http://aa.com',
                'city' => 'City',
                'address' => 'Some address',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'devices' => "[1,3,5]",
                'exhibitions' => '["Выставка в Париже", "Выставка в Москве, 2014"]',
                'sell_count' => rand(0,100),
                'purchase_count' => rand(0,100),
            ]);

        $us->generateThumbs(storage_path().'/app/seeding/ava'.rand(1,4).'.jpg');

        $this->authors[] = $us->id;

        $defcol = Collection::create(
            [
                'title' => 'Default Collection',
                'description' => 'Default Collection for user '.$name,
                'visibility' => Visibility::Owner,
                'allow_comments' => 0,
                'created_by' => $us->id,
                'edited_by' => $us->id,
                'view_count' => rand(0,100),

                'default' => 1,
                'allow_ad' => 0
            ]);

        $this->spawnComments('col'.$defcol->id);

        $arts = array();

        for($j = 0; $j < rand(0, 7); $j++)
        {
            $col = Collection::create(
                [
                    'title' => 'Collection '.$j,
                    'description' => 'Collection of user '.$name,
                    'visibility' => rand(0,2),
                    'allow_comments' => rand(0,1),
                    'created_by' => $us->id,
                    'edited_by' => $us->id,
                    'view_count' => rand(0,100),

                    'default' => 0,
                    'allow_ad' => rand(0,1)
                ]);

            $this->spawnComments('col'.$col->id);

            for($k = 0; $k < rand(0, 20); $k++)
            {
                $art = Artwork::create(
                    [
                        'title' => 'Artwork '.$k,
                        'description' => 'Artwork of user '.$name,
                        'visibility' => rand(0,2),
                        'allow_comments' => rand(0,1),
                        'created_by' => $us->id,
                        'edited_by' => $us->id,
                        'view_count' => rand(0,100),

                        'art_technic' => $this->technics[rand(0, count($this->technics) - 1)],
                        'genre' => $this->genres[rand(0, count($this->genres) - 1)],
                        'buy_possible' => rand(0,1),
                        'rent_possible' => rand(0,1),
                        'buy_price' => rand(100,1000),
                        'rent_price' => rand(100,1000),
                        'release_date' => "2001",
                        'empty_fill' => rand(0,100) > 50 ? "#000" : "#fff",
                        'height' => 1024,
                        'width' => 2048,
                        'units' => rand(0,1),
                        'device' => rand(0,100) > 20 ? $this->devices[rand(0, count($this->devices) - 1)] : null,
                        'auction_history' => rand(0,1),
                        'owner_id' => $us->id,
                        'collection_id' => rand(0,100) > 10 ? $col->id : $defcol->id,
                    ]);

                $this->spawnComments('art'.$art->id);

                $art->storeImage(storage_path().'/app/seeding/art'.rand(1,40).'.jpg');
                $art->generateThumbs();

                $arts[] = $art->id;

                $col->generateThumbs();
                $defcol->generateThumbs();

                if(rand(0,100) > 80)
                {
                    $status = rand(0, 3);
                    $start = $status == AuctionStatus::NotStarted ? Carbon::now()->addDay(rand(1,40)) : Carbon::now()->subDay(rand(3,40));
                    $diff = $start->diffInDays();
                    $end = $status == AuctionStatus::NotStarted ? $start->copy()->addDay(rand(1,40)) : ($status == AuctionStatus::Started ? Carbon::now()->addDay(rand(1,40)) : $start->copy()->addDay(rand(1,$diff)));

                    $auc = Auction::create(
                        [
                            'view_count' => rand(0,100),
                            'title' => 'Auction '.$k,
                            'description' => 'Auction for Artwork '.$k.' of user '.$name,
                            'visibility' => 0,
                            'allow_comments' => rand(0,1),
                            'created_by' => $us->id,
                            'edited_by' => $us->id,
                            'artwork_id' => $art->id,
                            'start' => $start->toDateTimeString(),
                            'end' => $end->toDateTimeString(),
                            'payment_done' => $status == AuctionStatus::Payed ? 1 : 0,
                            'repeat_possible' => rand(0,1),
                            'initial_stake' => rand(100, 500),
                            'stake_step' => rand(50, 200),
                            'prev_status' => $status,
                            'rent_period' => rand(7, 360),
                            'sell_conditions' => rand(0, 1),
                            'sell_type' => rand(0, 1)
                        ]);

                    $this->spawnComments('auc'.$auc->id);

                    if($status > AuctionStatus::NotStarted)
                        $this->started_auctions[] = $auc;
                }
            }
        }

        $shows = array();

        for($j = 0; $j < rand(0, 3); $j++)
        {
            $show = Slideshow::create(
                [
                    'title' => 'Show '.$j,
                    'description' => 'Slide Show of user '.$name,
                    'created_by' => $us->id,
                    'edited_by' => $us->id,
					'randomize' => rand(0,1),
                ]);

            $shows[] = $show;
        }

        if(count($shows) > 0)
        {
            for($j = 0; $j < rand(0, count($arts)-1); $j++)
            {
                $slide = Slide::create(
                    [
                        'num' => $j,
                        'slideshow_id' => $shows[rand(0, count($shows) - 1)]->id,
                        'artwork_id' => $arts[$j],
                        'delay' => rand(1, 10),
						'showtitle' => rand(0, 1),
                        'image_effect' => 'fadetwins',
						'caption_effect' => 'movel',
                    ]);
            }
        }

        foreach($shows as $show)
            $show->generateThumbs();
    }

    public function spawnAuthors()
    {
        $this->authors = array();
        for($i = 0; $i < 10; $i++)
        {
            $name = 'author'.$i;
            if($i == 0)
                $name = "author";

            $this->spawnAuthor($i, $name);
            gc_collect_cycles();
        }
    }


    public function spawnCollector($i, $name)
    {
        $us = User::create(
            [
                'activation_code' => '',
                'active' => 1,
                'name' => $name,
                'real_name' => 'Collect '.$i,
                'role' => UserRole::Collector,
                'email' => 'the_virt@pisem.net',
                'password' => bcrypt('123'),
                'birthday' => "2001-04-16",
                'gender' => rand(0,2),
                'country' => $this->countries[rand(0, count($this->countries) - 1)],
                'web' => 'http://aa.com',
                'city' => 'City',
                'address' => 'Some address',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'devices' => "[1,3,5]",
                'exhibitions' => '["Выставка в Париже", "Выставка в Москве, 2014"]',
                'sell_count' => rand(0,100),
                'purchase_count' => rand(0,100),
            ]);

        $us->generateThumbs(storage_path().'/app/seeding/ava'.rand(1,4).'.jpg');

        $this->collects[] = $us->id;

        $defcol = Collection::create(
            [
                'title' => 'Default Collection',
                'description' => 'Default Collection for user '.$name,
                'visibility' => Visibility::Owner,
                'allow_comments' => 0,
                'created_by' => $us->id,
                'edited_by' => $us->id,
                'view_count' => rand(0,100),

                'default' => 1,
                'allow_ad' => 0
            ]);

        $this->spawnComments('col'.$defcol->id);

        $arts = array();

        for($j = 0; $j < rand(0, 7); $j++)
        {
            $col = Collection::create(
                [
                    'title' => 'Collection '.$j,
                    'description' => 'Collection of user '.$name,
                    'visibility' => rand(0,2),
                    'allow_comments' => rand(0,1),
                    'created_by' => $us->id,
                    'edited_by' => $us->id,
                    'view_count' => rand(0,100),

                    'default' => 0,
                    'allow_ad' => rand(0,1)
                ]);

            $this->spawnComments('col'.$col->id);

            for($k = 0; $k < rand(0, 20); $k++)
            {
                $author = $this->authors[rand(0, count($this->authors) - 1)];

                $rent = rand(0,100) > 80;

                $art = Artwork::create(
                    [
                        'title' => 'Artwork '.$k,
                        'description' => 'Artwork of user '.$name,
                        'visibility' => rand(0,2),
                        'allow_comments' => rand(0,1),
                        'created_by' => $author,
                        'edited_by' => rand(0,100) > 20 ? $author : $us->id,
                        'view_count' => rand(0,100),

                        'art_technic' => $this->technics[rand(0, count($this->technics) - 1)],
                        'genre' => $this->genres[rand(0, count($this->genres) - 1)],
                        'buy_possible' => $rent ? 0 : rand(0,1),
                        'rent_possible' => $rent ? 0 : rand(0,1),
                        'buy_price' => rand(100,1000),
                        'rent_price' => rand(100,1000),
                        'release_date' => "2001",
                        'empty_fill' => rand(0,100) > 50 ? "#000" : "#fff",
                        'height' => 1024,
                        'width' => 2048,
                        'units' => rand(0,1),
                        'device' => rand(0,100) > 20 ? $this->devices[rand(0, count($this->devices) - 1)] : null,
                        'auction_history' => rand(0,1),
                        'rented_until' => $rent ? Carbon::now()->addDay(rand(1,40))->toDateTimeString() : null,
                        'owner_id' => $us->id,
                        'prev_owner_id' => $author,
                        'collection_id' => rand(0,100) > 10 ? $col->id : $defcol->id,
                    ]);

                $this->spawnComments('art'.$art->id);

                $art->storeImage(storage_path().'/app/seeding/art'.rand(1,40).'.jpg');
                $art->generateThumbs();

                $arts[] = $art->id;

                $col->generateThumbs();
                $defcol->generateThumbs();
            }
        }

        $shows = array();

        for($j = 0; $j < rand(0, 3); $j++)
        {
            $show = Slideshow::create(
                [
                    'title' => 'Show '.$j,
                    'description' => 'Slide Show of user '.$name,
                    'created_by' => $us->id,
                    'edited_by' => $us->id,
					'randomize' => rand(0,1),
                ]);

            $shows[] = $show;
        }

        if(count($shows) > 0)
        {
            for($j = 0; $j < rand(0, count($arts)-1); $j++)
            {
                $slide = Slide::create(
                    [
                        'num' => $j,
                        'slideshow_id' => $shows[rand(0, count($shows) - 1)]->id,
                        'artwork_id' => $arts[$j],
                        'delay' => rand(1, 10),
                        'showtitle' => rand(0, 1),
                        'image_effect' => 'fadetwins',
                        'caption_effect' => 'movel',
                    ]);
            }
        }
        foreach($shows as $show)
            $show->generateThumbs();
    }

    public function spawnCollectors()
    {
        $this->collects = array();

        for($i = 0; $i < 10; $i++)
        {
            $name = 'collector'.$i;
            if($i == 0)
                $name = "collect";

            $this->spawnCollector($i, $name);
            gc_collect_cycles();
        }
    }

    public function run()
    {
        Model::unguard();

        $this->countries = \DB::table("lang_resources")->where('lang', 'ru')->where('resource_type', ResourceType::Country)->lists('resource_id');
        $this->genres = \DB::table("lang_resources")->where('lang', 'ru')->where('resource_type', ResourceType::Genre)->lists('resource_id');
        $this->technics = \DB::table("lang_resources")->where('lang', 'ru')->where('resource_type', ResourceType::Technic)->lists('resource_id');
        $this->devices = \DB::table("lang_resources")->where('lang', 'ru')->where('resource_type', ResourceType::Device)->lists('resource_id');

        $this->started_auctions = array();
        $this->spawnModerators();
        $this->spawnAuthors();
        $this->spawnCollectors();




        foreach($this->started_auctions as $auc)
        {
            $num_bids = rand($auc->prev_status == AuctionStatus::Payed ? 1 : 0, 15);
            $winner = null;
            $stake = null;

            $st = rand($auc->initial_stake + $auc->stake_step, $auc->initial_stake + $auc->stake_step + 200);

            for($i = 0; $i < $num_bids; $i++)
            {
                $bid = Bid::create(
                [
                    'auction_id' => $auc->id,
                    'stake' => $st,
                    'bidder_id' => $this->collects[rand(0, count($this->collects) - 1)],
                    'comment' => 'Some comment'
                ]);

                $winner = $bid->bidder_id;
                $stake = $st;

                $st += rand($auc->stake_step, $auc->stake_step + 200);
            }

            $auc->winner_id = $winner;
            $auc->final_stake = $stake;
            $auc->final_stake_date = Carbon::now();
            $auc->save();
        }

        foreach($this->collects as $user)
        {
            for($i = 0; $i < rand(10, 30); $i++)
            {
                $msg = Message::create(
                [
                    'subject' => 'Message '.$i,
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    'recipient_id' => $user,
                    'sender_id' => rand(0,100) > 20 ? $this->authors[rand(0, count($this->authors) - 1)] : null,
                    'date_sent' => Carbon::now()->toDateTimeString(),
                    'read' => rand(0,1)
                ]);
            }
        }

        foreach($this->authors as $user)
        {
            for($i = 0; $i < rand(10, 30); $i++)
            {
                $msg = Message::create(
                [
                    'subject' => 'Message '.$i,
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    'recipient_id' => $user,
                    'sender_id' => rand(0,100) > 20 ? $this->collects[rand(0, count($this->collects) - 1)] : null,
                    'date_sent' => Carbon::now()->toDateTimeString(),
                    'read' => rand(0,1)
                ]);
            }
        }
    }

}